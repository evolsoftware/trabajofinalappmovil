package com.tec.krea.mercatec.Entities;

/**
 * Created by K-PC on 14/10/2018.
 */

public class Presupuesto_x_Producto_x_Puesto {
    public int ID;
    public int PresupuestoID;
    public int Producto_x_PuestoID;
    public Double Cantidad;
    public Double Precio;
    public Double Total;
    public String PuestoAcronimo;
    public String NombreProducto;
}
