package com.tec.krea.mercatec.SpinnerAdapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.tec.krea.mercatec.Entities.Tipo_Producto;

import java.util.List;

/**
 * Created by Jordi on 25/10/2018.
 */

public class TipoProductoSpinnerAdapter extends ArrayAdapter<Tipo_Producto> {
    private Context context;
    List<Tipo_Producto> data = null;

    public TipoProductoSpinnerAdapter(Context context, int resource, List<Tipo_Producto> data2)
    {
        super(context, resource, data2);
        this.context = context;
        this.data = data2;
    }

    @Nullable
    @Override
    public Tipo_Producto getItem(int position) {
        return data.get(position);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).ID;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(data.get(position).Nombre);
        return label;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.GRAY);
        label.setText(data.get(position).Nombre);
        label.setTextSize(20);
        return label;
    }
}
