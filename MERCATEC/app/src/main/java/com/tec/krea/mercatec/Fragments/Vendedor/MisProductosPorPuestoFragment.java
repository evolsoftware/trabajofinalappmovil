package com.tec.krea.mercatec.Fragments.Vendedor;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.tec.krea.mercatec.Entities.Conexion;
import com.tec.krea.mercatec.Entities.DetalleProducto;
import com.tec.krea.mercatec.Entities.Puesto;
import com.tec.krea.mercatec.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class MisProductosPorPuestoFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public MisProductosPorPuestoFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static MisProductosPorPuestoFragment newInstance(int columnCount) {
        MisProductosPorPuestoFragment fragment = new MisProductosPorPuestoFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_misproductosporpuesto_list, container, false);

        mListener.onClickProductosPorPuestoBackButton(false);

        final Conexion conexion=new Conexion();
        int ID_Puesto = mListener.getMisPuestosFragmentInteraction().ID;

        FloatingActionButton btnAgregarProducto = view.findViewById(R.id.btnAgregarProductoAPuesto);
        btnAgregarProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null){
                    mListener.onAddPoductoAPuestoPressed();
                }
            }
        });

        AndroidNetworking.initialize(getContext());
        AndroidNetworking.get(conexion.pathAPI+"/api/productosporpuestoporusuario")
                .addQueryParameter("id", String.valueOf(ID_Puesto))
                .setTag("productoporpuesto")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        ArrayList<DetalleProducto> detalleProductos = new ArrayList<DetalleProducto>();
                        try{
                            for (int i = 0; i < response.length(); i++)
                            {
                                JSONObject jsonObject = response.getJSONObject(i);
                                DetalleProducto objDetalleProducto = new DetalleProducto();
                                objDetalleProducto.ID = jsonObject.getInt("ID");
                                objDetalleProducto.Nombre = jsonObject.getString("Nombre");
                                objDetalleProducto.Nombre_Grupo_Alimenticio = jsonObject.getString("Nombre_Grupo_Alimenticio");
                                objDetalleProducto.Precio = jsonObject.getDouble("Precio");
                                objDetalleProducto.ImagePath = jsonObject.getString("ImagePath");
                                objDetalleProducto.Nombre_Tipo_Producto = jsonObject.getString("Nombre_Tipo_Producto");
                                detalleProductos.add(objDetalleProducto);
                            }

                            Context context = view.getContext();
                            RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.lista_productos_por_puesto);
                            if (mColumnCount <= 1) {
                                recyclerView.setLayoutManager(new LinearLayoutManager(context));
                            } else {
                                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
                            }
                            recyclerView.setAdapter(new MisProductosPorPuestoRecyclerViewAdapter(detalleProductos, mListener));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        Puesto getMisPuestosFragmentInteraction();
        void onListProductoPorPuestoFragmentInteraction(DetalleProducto item);
        void onAddPoductoAPuestoPressed();
        void onClickProductosPorPuestoBackButton(boolean enable);
    }
}
