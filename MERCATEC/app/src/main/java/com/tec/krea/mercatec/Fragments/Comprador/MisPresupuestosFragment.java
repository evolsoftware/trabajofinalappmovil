package com.tec.krea.mercatec.Fragments.Comprador;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.tec.krea.mercatec.Entities.Conexion;
import com.tec.krea.mercatec.Entities.Presupuesto;
import com.tec.krea.mercatec.R;
import com.tec.krea.mercatec.Fragments.Comprador.dummy.DummyContent;
import com.tec.krea.mercatec.Fragments.Comprador.dummy.DummyContent.DummyItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class MisPresupuestosFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public MisPresupuestosFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static MisPresupuestosFragment newInstance(int columnCount) {
        MisPresupuestosFragment fragment = new MisPresupuestosFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_mispresupuestos_list, container, false);

        final Conexion conexion=new Conexion();
        SharedPreferences prefs = getActivity().getSharedPreferences("Login", Context.MODE_PRIVATE);
        int storedUserID = prefs.getInt("UsuarioID", 0);
        AndroidNetworking.initialize(getContext());
        AndroidNetworking.get(conexion.pathAPI+"/api/presupuestosporusuario")
                .addQueryParameter("id",String.valueOf(storedUserID))
                .setTag("mispresupuestoparaseleccionar")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        ArrayList<Presupuesto> presupuestos = new ArrayList<Presupuesto>();
                        try {
                            for(int a=0;a<response.length();a++) {
                                JSONObject jsonObject = response.getJSONObject(a);
                                final Presupuesto objPresupuesto = new Presupuesto();
                                objPresupuesto.ID = jsonObject.getInt("ID");
                                objPresupuesto.Estado_uso = jsonObject.getBoolean("Estado_uso");
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                objPresupuesto.Fecha = dateFormat.parse(jsonObject.getString("Fecha"));
                                objPresupuesto.Nombre = jsonObject.getString("Nombre");
                                objPresupuesto.UsuarioID = jsonObject.getInt("UsuarioID");
                                objPresupuesto.Total = jsonObject.getDouble("Total");
                                presupuestos.add(objPresupuesto);
                            }

                            Context context = view.getContext();
                            RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.lista_mis_presupuestos);
                            if (mColumnCount <= 1) {
                                recyclerView.setLayoutManager(new LinearLayoutManager(context));
                            } else {
                                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
                            }
                            recyclerView.setAdapter(new MisPresupuestosRecyclerViewAdapter(presupuestos, mListener));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
        FloatingActionButton button = view.findViewById(R.id.btnAgregarMiPresupuesto);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.AgregarPresupuestoPresionado();
            }
        });
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListMisPresupuestosFragmentInteraction(Presupuesto item);
        void AgregarPresupuestoPresionado();
        void EliminarPresupuestoPresionado(Presupuesto item);
    }
}
