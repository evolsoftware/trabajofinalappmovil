package com.tec.krea.mercatec.Fragments.Vendedor.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tec.krea.mercatec.Entities.Conexion;
import com.tec.krea.mercatec.Entities.Puesto;
import com.tec.krea.mercatec.Fragments.Comprador.Dialogs.AgregarPresupuestoDialog;
import com.tec.krea.mercatec.R;

import org.json.JSONObject;

/**
 * Created by Jordi on 22/10/2018.
 */

public class AgregarPuestoDialog extends AppCompatDialogFragment{
    private EditText txtAcronimo;
    private AgregarPuestoDialogListener listener;

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_agregar_puesto,null);

        AndroidNetworking.initialize(getContext());

        builder.setView(view)
                .setTitle("Agregar Puesto")
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setPositiveButton("Guardar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        txtAcronimo = view.findViewById(R.id.txtAcronimoPuesto);
        return builder.create();
    }

    @Override
    public void onResume() {
        super.onResume();
        final Conexion conexion=new Conexion();
        final AlertDialog d = (AlertDialog) getDialog();
        if(d!=null){
            Button positiveButton = d.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Boolean wantToCloseDialog = false;
                    String Acronimo = txtAcronimo.getText().toString();
                    if(!Acronimo.equals("")){
                        Puesto objPuesto = new Puesto();
                        objPuesto.Acronimo = txtAcronimo.getText().toString();

                        SharedPreferences prefs = getActivity().getSharedPreferences("Login", Context.MODE_PRIVATE);
                        int storedUserID = prefs.getInt("UsuarioID", 0);
                        objPuesto.UsuarioID = storedUserID;

                        AndroidNetworking.post(conexion.pathAPI+"/api/puesto")
                                .addBodyParameter(objPuesto)
                                .setTag("puesto")
                                .setPriority(Priority.LOW)
                                .build()
                                .getAsJSONObject(new JSONObjectRequestListener() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        Log.println(Log.ASSERT, "Éxito", "Registro exitoso");
                                        if (listener != null)
                                        {
                                            listener.GuardarPuesto();
                                        }
                                    }
                                    @Override
                                    public void onError(ANError anError) {
                                        Log.println(Log.ERROR, "Error", "Registro fallido" +
                                                anError.getErrorBody() +
                                                " - " + anError.getErrorDetail() +
                                                " - " + anError.getResponse());
                                    }
                                });
                        wantToCloseDialog = true;
                    }

                    else
                    {
                        Toast.makeText(getContext(),"Por favor complete el campo", Toast.LENGTH_SHORT).show();
                    }
                    if(wantToCloseDialog){
                        d.dismiss();
                    }
                }
            });
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (AgregarPuestoDialogListener) context;
        } catch (ClassCastException e){
            throw  new ClassCastException(context.toString() +
                    "must implement AgregarPuestoDialogListener");
        }
    }

    public interface AgregarPuestoDialogListener{
        void GuardarPuesto();
    }
}
