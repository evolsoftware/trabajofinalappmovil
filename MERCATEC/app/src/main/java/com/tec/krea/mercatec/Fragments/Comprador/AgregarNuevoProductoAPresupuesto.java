package com.tec.krea.mercatec.Fragments.Comprador;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tec.krea.mercatec.Entities.Conexion;
import com.tec.krea.mercatec.Entities.Grupo_Alimenticio;
import com.tec.krea.mercatec.Entities.Presupuesto;
import com.tec.krea.mercatec.Entities.Presupuesto_x_Producto_x_Puesto;
import com.tec.krea.mercatec.Entities.Producto;
import com.tec.krea.mercatec.Entities.Producto_x_Puesto;
import com.tec.krea.mercatec.Entities.Tipo_Producto;
import com.tec.krea.mercatec.R;
import com.tec.krea.mercatec.SpinnerAdapters.GruposAlimenticiosSpinnerAdapter;
import com.tec.krea.mercatec.SpinnerAdapters.ProductosSpinnerAdapter;
import com.tec.krea.mercatec.SpinnerAdapters.ProductosXPuestoSpinnerAdapter;
import com.tec.krea.mercatec.SpinnerAdapters.TipoProductoSpinnerAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AgregarNuevoProductoAPresupuesto.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AgregarNuevoProductoAPresupuesto#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AgregarNuevoProductoAPresupuesto extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public AgregarNuevoProductoAPresupuesto() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AgregarNuevoProductoAPresupuesto.
     */
    // TODO: Rename and change types and number of parameters
    public static AgregarNuevoProductoAPresupuesto newInstance(String param1, String param2) {
        AgregarNuevoProductoAPresupuesto fragment = new AgregarNuevoProductoAPresupuesto();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_agregar_nuevo_producto_apresupuesto, container, false);
        mListener.backButtonAgregarNuevoProductoAPresupuesto(true);
        AndroidNetworking.initialize(getActivity().getApplicationContext());
        final Conexion conexion=new Conexion();
        final Spinner spnGruposAlimenticios = view.findViewById(R.id.spn_presupuesto_grupoalimenticio);
        AndroidNetworking.get(conexion.pathAPI+"/api/grupoalimenticio")
                .setTag("grupoAlimenticio")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try{
                            ArrayList<Grupo_Alimenticio> gruposAlimenticios = new ArrayList<Grupo_Alimenticio>();
                            Grupo_Alimenticio defautValue = new Grupo_Alimenticio();
                            defautValue.ID = 0;
                            defautValue.Nombre = "Seleccione un grupo alimenticio...";
                            gruposAlimenticios.add(defautValue);

                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = response.getJSONObject(i);
                                Grupo_Alimenticio objGrupoAlienticio = new Grupo_Alimenticio();
                                objGrupoAlienticio.ID = jsonObject.getInt("ID");
                                objGrupoAlienticio.Nombre = jsonObject.getString("Nombre");
                                gruposAlimenticios.add(objGrupoAlienticio);
                            }

                            GruposAlimenticiosSpinnerAdapter gruposAlimenticiosSpinnerAdapter = new GruposAlimenticiosSpinnerAdapter(view.getContext(), android.R.layout.simple_spinner_item, gruposAlimenticios);

                            spnGruposAlimenticios.setAdapter(gruposAlimenticiosSpinnerAdapter);
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

        final Spinner spnTiposProductos = view.findViewById(R.id.spn_presupuesto_tipoproducto);
        spnGruposAlimenticios.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, final View view, int position, long id) {
                AndroidNetworking.get(conexion.pathAPI+"/api/tipoproductosporgrupoalimenticio")
                        .addQueryParameter("id", Long.toString(id))
                        .setTag("tiposProductos")
                        .setPriority(Priority.LOW)
                        .build()
                        .getAsJSONArray(new JSONArrayRequestListener() {
                            @Override
                            public void onResponse(JSONArray response) {
                                try{
                                    ArrayList<Tipo_Producto> tiposProductos = new ArrayList<Tipo_Producto>();
                                    Tipo_Producto defaultValue = new Tipo_Producto();
                                    defaultValue.ID = 0;
                                    defaultValue.Nombre = "Seleccione un tipo de producto...";
                                    tiposProductos.add(defaultValue);

                                    for(int i = 0; i < response.length(); i++)
                                    {
                                        JSONObject jsonObject = response.getJSONObject(i);
                                        Tipo_Producto objTipoProducto = new Tipo_Producto();
                                        objTipoProducto.ID = jsonObject.getInt("ID");
                                        objTipoProducto.Nombre = jsonObject.getString("Nombre");
                                        tiposProductos.add(objTipoProducto);
                                    }

                                    TipoProductoSpinnerAdapter tipoProductoSpinnerAdapter = new TipoProductoSpinnerAdapter(view.getContext(), android.R.layout.simple_spinner_item, tiposProductos);

                                    spnTiposProductos.setAdapter(tipoProductoSpinnerAdapter);
                                }
                                catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onError(ANError anError) {

                            }
                        });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final Spinner spnProductos = view.findViewById(R.id.spn_presupuesto_producto);
        spnTiposProductos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, final View view, int position, long id) {
                AndroidNetworking.get(conexion.pathAPI+"/api/productosportipoproducto")
                        .addQueryParameter("id", Long.toString(id))
                        .setTag("productos")
                        .setPriority(Priority.LOW)
                        .build()
                        .getAsJSONArray(new JSONArrayRequestListener() {
                            @Override
                            public void onResponse(JSONArray response) {
                                try {
                                    ArrayList<Producto> misProductos = new ArrayList<Producto>();
                                    Producto defaultValue = new Producto();
                                    defaultValue.ID = 0;
                                    defaultValue.Nombre = "Seleccione un producto...";
                                    misProductos.add(defaultValue);

                                    for (int i = 0; i < response.length(); i++) {
                                        JSONObject jsonObject = response.getJSONObject(i);
                                        Producto objProducto = new Producto();
                                        objProducto.ID = jsonObject.getInt("ID");
                                        objProducto.Nombre = jsonObject.getString("Nombre");
                                        misProductos.add(objProducto);
                                    }

                                    ProductosSpinnerAdapter productosSpinnerAdapter = new ProductosSpinnerAdapter(view.getContext(), android.R.layout.simple_spinner_item, misProductos);

                                    spnProductos.setAdapter(productosSpinnerAdapter);
                                }
                                catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onError(ANError anError) {

                            }
                        });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final Spinner spnPresupuestoPrecioPuesto = view.findViewById(R.id.spn_presupuesto_precio_puesto);

        spnProductos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, final View view, int position, long id) {
                AndroidNetworking.get(conexion.pathAPI+"/api/preciosporproducto")
                        .addQueryParameter("id", Long.toString(id))
                        .setTag("preciosporproducto")
                        .setPriority(Priority.LOW)
                        .build()
                        .getAsJSONArray(new JSONArrayRequestListener() {
                            @Override
                            public void onResponse(JSONArray response) {
                                try {
                                    ArrayList<Producto_x_Puesto> producto_x_puestos = new ArrayList<Producto_x_Puesto>();

                                    for (int i = 0; i < response.length(); i++) {
                                        JSONObject jsonObject = response.getJSONObject(i);
                                        Producto_x_Puesto objProductoPuesto = new Producto_x_Puesto();
                                        objProductoPuesto.ID = jsonObject.getInt("ID");
                                        objProductoPuesto.PuestoAcronimo = jsonObject.getString("PuestoAcronimo");
                                        objProductoPuesto.Precio = jsonObject.getDouble("Precio");
                                        producto_x_puestos.add(objProductoPuesto);
                                    }

                                    ProductosXPuestoSpinnerAdapter productosXPuestoSpinnerAdapter = new ProductosXPuestoSpinnerAdapter(view.getContext(), android.R.layout.simple_spinner_item, producto_x_puestos);

                                    spnPresupuestoPrecioPuesto.setAdapter(productosXPuestoSpinnerAdapter);
                                }
                                catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onError(ANError anError) {

                            }
                        });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final EditText txtCantidad = view.findViewById(R.id.txt_cantidad_nuevo_producto_a_presupuesto);
        Button btnAgregar = view.findViewById(R.id.btnAgregarNuevoProductoAPresupuesto);
        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int Producto_x_PuestoID = (int)spnPresupuestoPrecioPuesto.getSelectedItemId();
                if(Producto_x_PuestoID > 0 && !txtCantidad.getText().toString().equals("")){
                    int PresupuestoID = mListener.getPresupuestoAgregarNuevoProductoAPresupuesto().ID;
                    final Presupuesto_x_Producto_x_Puesto obj = new Presupuesto_x_Producto_x_Puesto();
                    obj.PresupuestoID = PresupuestoID;
                    obj.Producto_x_PuestoID = Producto_x_PuestoID;
                    obj.Cantidad = Double.valueOf(txtCantidad.getText().toString());
                    AndroidNetworking.post(conexion.pathAPI+"/api/presupuestoporproductoporpuesto")
                            .addBodyParameter(obj)
                            .setTag("presupuestoporproductoporpuesto")
                            .setPriority(Priority.LOW)
                            .build()
                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.println(Log.ASSERT, "Éxito", "Agregar producto al presupuesto exitoso");
                                    Producto_x_Puesto pp = (Producto_x_Puesto)spnPresupuestoPrecioPuesto.getSelectedItem();
                                    Double Total = pp.Precio * Double.valueOf(txtCantidad.getText().toString());
                                    mListener.GuardarNuevoProductoAPresupuesto(Total);
                                }
                                @Override
                                public void onError(ANError anError) {
                                    Log.println(Log.ERROR, "Error", "Agregar producto al presupuesto fallido" +
                                            anError.getErrorBody() +
                                            " - " + anError.getErrorDetail() +
                                            " - " + anError.getResponse());
                                }
                            });
                }
                else{
                    Toast.makeText(getContext(),"Por favor complete todos los campos", Toast.LENGTH_SHORT).show();
                }
            }
        });
        Button btnCancelar = view.findViewById(R.id.btnCancelarAgregarNuevoProductoAPresupuesto);
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.CancelarNuevoProductoAPresupuesto();
            }
        });
        return view;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
        Presupuesto getPresupuestoAgregarNuevoProductoAPresupuesto();
        void GuardarNuevoProductoAPresupuesto(Double actualizarTotal);
        void CancelarNuevoProductoAPresupuesto();
        void backButtonAgregarNuevoProductoAPresupuesto(boolean enable);
    }
}
