package com.tec.krea.mercatec.Fragments.Vendedor;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.service.autofill.DateTransformation;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.tec.krea.mercatec.Activities.CompradorMainActivity;
import com.tec.krea.mercatec.Activities.LoginActivity;
import com.tec.krea.mercatec.Activities.VendedorMainActivity;
import com.tec.krea.mercatec.Entities.Conexion;
import com.tec.krea.mercatec.Entities.Usuario;
import com.tec.krea.mercatec.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.IllegalFormatCodePointException;
import java.util.Locale;

import okhttp3.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PerfilVendedorFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PerfilVendedorFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PerfilVendedorFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private EditText txtNombres;
    private EditText txtApellidos;
    private EditText txtCorreo;
    private EditText txtUsername;
    private EditText txtPassword;
    private EditText txtRepeatPassword;
    private TextView txtFecha_registro;
    private Button btnGuardar;
    private Button btnEliminarCuenta;
    public PerfilVendedorFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PerfilVendedorFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PerfilVendedorFragment newInstance(String param1, String param2) {
        PerfilVendedorFragment fragment = new PerfilVendedorFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view= inflater.inflate(R.layout.fragment_perfil_vendedor, container, false);

        final Conexion conexion=new Conexion();
        final Usuario objUsuario=new Usuario();
        txtNombres=view.findViewById(R.id.txt_epv_nombres);
        txtApellidos=view.findViewById(R.id.txt_epv_apellidos);
        txtCorreo=view.findViewById(R.id.txt_epv_correo);
        txtUsername=view.findViewById(R.id.txt_epv_username);
        txtPassword=view.findViewById(R.id.txt_epv_password);
        txtRepeatPassword=view.findViewById(R.id.txt_epv_repeat_password);
        txtFecha_registro=view.findViewById(R.id.txt_epv_fecha_registro);
        btnEliminarCuenta=view.findViewById(R.id.btnEliminarCuentaVendedor);
        btnGuardar=view.findViewById(R.id.btnEpvGuardar);

        //  Ubicar datos de usuario
        SharedPreferences prefs = getActivity().getSharedPreferences("Login", Context.MODE_PRIVATE);
        int storedUserID = prefs.getInt("UsuarioID", 0);

        AndroidNetworking.get(conexion.pathAPI+"/api/Usuario")
                .addQueryParameter("id", Integer.toString(storedUserID))
                .setTag("usuario")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        try {

                            String fecha=response.getString("Fecha_registro");
                            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                            Date date= null;
                            try {
                                date = sdf.parse(fecha);
                                objUsuario.Fecha_registro=date;
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(date);
                            String month="";
                            switch (cal.get(Calendar.MONTH)){
                                case 0:month="Enero";break;
                                case 1:month="Febrero";break;
                                case 2:month="Marzo";break;
                                case 3:month="Abril";break;
                                case 4:month="Mayo";break;
                                case 5:month="Junio";break;
                                case 6:month="Julio";break;
                                case 7:month="Agosto";break;
                                case 8:month="Septiembre";break;
                                case 9:month="Octubre";break;
                                case 10:month="Noviembre";break;
                                case 11:month="Diciembre";break;
                            }
                            String fecha_registro=String.valueOf(cal.get(Calendar.DAY_OF_MONTH)+
                                    " de "+month+" del "+String.valueOf(cal.get(Calendar.YEAR)));

                            objUsuario.ID=response.getInt("ID");
                            objUsuario.Nombre=response.getString("Nombre");
                            objUsuario.Apellidos=response.getString("Apellidos");
                            objUsuario.Correo=response.getString("Correo");
                            objUsuario.Username=response.getString("Username");
                            objUsuario.Password=response.getString("Password");

                            txtFecha_registro.setText(fecha_registro);
                            txtNombres.setText(objUsuario.Nombre);
                            txtApellidos.setText(objUsuario.Apellidos);
                            txtCorreo.setText(objUsuario.Correo);
                            txtUsername.setText(objUsuario.Username);
                            txtPassword.setText(objUsuario.Password);
                            txtRepeatPassword.setText(objUsuario.Password);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.println(Log.ERROR, "Error",
                                "Some error!!" + error.getErrorBody()
                                        + " - " + error.getErrorDetail()
                                        + " - " + error.getResponse());
                    }
                });

        //  Eventos del fragment
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  VALIDAR CONTRASEÑAS Y CHECKBOXES

                objUsuario.Nombre=txtNombres.getText().toString();
                objUsuario.Apellidos=txtApellidos.getText().toString();
                objUsuario.Correo=txtCorreo.getText().toString();
                objUsuario.Username=txtUsername.getText().toString();
                objUsuario.Password=txtPassword.getText().toString();
                String repeatPassword=txtRepeatPassword.getText().toString();

                if (objUsuario.Nombre.isEmpty()
                        ||objUsuario.Apellidos.isEmpty()
                        ||objUsuario.Correo.isEmpty()
                        ||objUsuario.Username.isEmpty()
                        ||objUsuario.Password.isEmpty()
                        ||repeatPassword.isEmpty()
                        ){
                    Toast.makeText(view.getContext(),
                            "Por favor complete todos los campos",
                            Toast.LENGTH_SHORT).show();
                }else{
                    if (objUsuario.Password.equals(repeatPassword)){
                        try {
                            //  PUT el objUsuario para actualizar datos
                            AndroidNetworking.put(conexion.pathAPI+"/api/Usuario/{id}")
                                    .addPathParameter("id", Integer.toString(objUsuario.ID))
                                    .addBodyParameter(objUsuario)
                                    .setTag("usuario")
                                    .setPriority(Priority.MEDIUM)
                                    .build()
                                    .getAsOkHttpResponse(new OkHttpResponseListener() {
                                        @Override
                                        public void onResponse(Response response) {
                                            Log.println(Log.INFO,
                                                    "INFO",
                                                    "Response Code "+Integer.toString(response.code()));
                                            Toast.makeText(view.getContext(),
                                                    "Se actualizo sus datos",
                                                    Toast.LENGTH_SHORT).show();
                                        }

                                        @Override
                                        public void onError(ANError anError) {
                                            anError.printStackTrace();
                                            Toast.makeText(view.getContext(),
                                                    "No se pudo actualizar sus datos",
                                                    Toast.LENGTH_SHORT).show();
                                        }
                                    });
                        } catch (Error e) {
                            e.printStackTrace();
                        }

                    }else{
                        Toast.makeText(view.getContext(),
                                "Las contraseñas no coinciden",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        btnEliminarCuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(view.getContext(),
                        "Servicio de eliminación de cuenta no disponible!",
                        Toast.LENGTH_SHORT).show();
            }
        });

        ImageButton btnCerrarSesion = view.findViewById(R.id.btnCerrarSesionVendedor);
        btnCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), LoginActivity.class);
                startActivity(intent);
            }
        });

        //Para el cambio de cuentas
        //prefs = getActivity().getSharedPreferences("Login", Context.MODE_PRIVATE);
        int numeroCuentas = prefs.getInt("cuentas", 0);

        LinearLayout linearLayout = view.findViewById(R.id.linearlayout_CambiarCuenta_Vendedor);
        if(numeroCuentas > 1){
            ImageButton btnCambiarCuenta = view.findViewById(R.id.btnCambiarCuentaVendedor);
            btnCambiarCuenta.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), CompradorMainActivity.class);
                    startActivity(intent);
                }
            });
        }
        else{
            linearLayout.setVisibility(LinearLayout.GONE);
        }
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
