package com.tec.krea.mercatec.Fragments.Comprador.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tec.krea.mercatec.Entities.Conexion;
import com.tec.krea.mercatec.R;

import org.json.JSONObject;

public class EliminarPresupuestoDialog extends AppCompatDialogFragment {
    private EliminarPresupuestoDialogListener listener;
    private int IDPresupuesto;

    public void setIDPresupuesto(int id){
        IDPresupuesto = id;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_eliminar_presupuesto,null);

        AndroidNetworking.initialize(getContext());
        builder.setView(view)
                .setTitle("Confirmar eliminación")
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        return builder.create();
    }

    @Override
    public void onResume() {
        super.onResume();
        final Conexion conexion=new Conexion();
        final AlertDialog d = (AlertDialog) getDialog();
        if(d!=null){
            Button positiveButton = d.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AndroidNetworking.delete(conexion.pathAPI+"/api/presupuesto")
                            .addQueryParameter("id",String.valueOf(IDPresupuesto))
                            .setTag("eliminarpresupuesto")
                            .setPriority(Priority.LOW)
                            .build()
                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Toast.makeText(getContext(),"Presupuesto eliminado", Toast.LENGTH_SHORT).show();
                                    listener.AceptarEliminarPresupuesto();
                                    d.dismiss();
                                }

                                @Override
                                public void onError(ANError anError) {
                                    Log.println(Log.ERROR, "Error", "Eliminación fallidida" +
                                            anError.getErrorBody() +
                                            " - " + anError.getErrorDetail() +
                                            " - " + anError.getResponse());
                                }
                            });
                }
            });
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            listener = (EliminarPresupuestoDialogListener) context;
        }catch (ClassCastException e){
            throw  new ClassCastException(context.toString() +
                    "must implement AgregarPresupuestoDialogListener");
        }
    }

    public interface EliminarPresupuestoDialogListener{
        void AceptarEliminarPresupuesto();
    }
}
