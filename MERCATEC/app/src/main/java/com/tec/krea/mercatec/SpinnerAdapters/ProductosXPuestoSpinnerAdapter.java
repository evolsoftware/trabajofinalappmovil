package com.tec.krea.mercatec.SpinnerAdapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.tec.krea.mercatec.Entities.Producto_x_Puesto;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by Jordi on 25/10/2018.
 */

public class ProductosXPuestoSpinnerAdapter extends ArrayAdapter<Producto_x_Puesto> {
    private Context context;
    List<Producto_x_Puesto> data = null;
     public ProductosXPuestoSpinnerAdapter(Context context, int resource, List<Producto_x_Puesto> data2){
         super(context, resource, data2);
         this.context = context;
         this.data = data2;
     }

    @Nullable
    @Override
    public Producto_x_Puesto getItem(int position) { return data.get(position); }

    @Override
    public int getCount(){ return data.size(); }

    @Override
    public long getItemId(int position) { return data.get(position).ID; }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        label.setText(data.get(position).PuestoAcronimo + " - S/" + decimalFormat.format(data.get(position).Precio));
        return label;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.GRAY);
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        label.setText(data.get(position).PuestoAcronimo + " - S/" + decimalFormat.format(data.get(position).Precio));
        label.setTextSize(20);
        return label;
    }
}
