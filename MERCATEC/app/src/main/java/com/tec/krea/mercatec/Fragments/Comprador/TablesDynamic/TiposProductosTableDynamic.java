package com.tec.krea.mercatec.Fragments.Comprador.TablesDynamic;

import android.content.Context;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.tec.krea.mercatec.Entities.Tipo_Producto;

import java.util.ArrayList;

/**
 * Created by alumnos on 10/30/18.
 */

public class TiposProductosTableDynamic {
    private TableLayout tableLayout;
    private Context context;
    private TableRow tableRow;
    private ImageView imageView;
    private TextView textView;
    private ArrayList<Tipo_Producto> data;
    int indexR;
    int indexC;
    public TiposProductosTableDynamic(TableLayout tableLayout, Context context){
            this.tableLayout = tableLayout;
            this.context = context;
    }

    private void newRow(){
        tableRow = new TableRow(context);
        tableRow.setClickable(false);
    }

    private void newCell(Tipo_Producto item){
        imageView = new ImageView(context);
        imageView.setLayoutParams(new TableRow.LayoutParams(20,20));
        textView = new TextView(context);
        textView.setGravity(Gravity.CENTER);
        textView.setText(item.Nombre);
        textView.setTextSize(10);
    }
    public void addData(ArrayList<Tipo_Producto>data){
        this.data = data;
        createDataTable();
    }
    private void createDataTable(){
        String info;
        newRow();
        for(indexC=0;indexC<data.size();indexC++){
            newCell(data.get(indexC));
        }
    }
    private TableRow getRow(int index){
        return (TableRow) tableLayout.getChildAt(index);
    }

    private TextView getCell(int rowIndex, int columIndex){
        tableRow = getRow(rowIndex);
        return (TextView) tableRow.getChildAt(columIndex + 1); // Es mas 1 porque hay un boton en la primera columna
    }

    private TableRow.LayoutParams newTableRowParams(){
        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT);
        params.setMargins(1,1,1,1);
        params.weight=1;
        return params;
    }
}
