package com.tec.krea.mercatec.Fragments.Comprador.TablesDynamic;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Layout;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.tec.krea.mercatec.Entities.Presupuesto_x_Producto_x_Puesto;
import com.tec.krea.mercatec.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class ProductosTableDynamic {
    private ProductosTableDynamicListener listener;
    private TableLayout tableLayout;
    private Context context;
    private String[]header;
    //private ArrayList<String[]>data;
    private TableRow tableRow;
    private TextView txtCell;
    private ImageButton deleteButton;
    private int indexC;
    private int indexR;
    private boolean multiColor = false;
    int firstColor;
    int secondColor;
    int textColor;
    int colorLine = 0;
    private ArrayList<Presupuesto_x_Producto_x_Puesto> data;
    public ProductosTableDynamic(TableLayout tableLayout, Context context){
        this.tableLayout = tableLayout;
        this.context = context;
    }
    public void addHeader(String[]header){
        this.header = header;
        createHeader();
    }
    public void addData(ArrayList<Presupuesto_x_Producto_x_Puesto>data){
        this.data = data;
        createDataTable();
    }
    private void newRow(final int index){
        tableRow = new TableRow(context);
        tableRow.setId(index);
        tableRow.setClickable(false);
    }

    private void newCell(){
        txtCell = new TextView(context);
        txtCell.setGravity(Gravity.CENTER);
        txtCell.setTextSize(20);
    }
    private void createDeleteButtonCell(int index, final int idProducto){
        deleteButton = new ImageButton(context);
        deleteButton.setImageResource(R.drawable.ic_delete_black_24dp);
        deleteButton.setId(index);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    listener.onClickDeleteItem(v.getId(), idProducto);
                }catch (Exception e){
                    int id = deleteButton.getId();
                    e.printStackTrace();
                }
            }
        });
    }
    private void createHeader(){
        indexC = 0;
        newRow(0);
        //Create Image
        ImageView deleteImage = new ImageView(context);
        deleteImage.setImageResource(R.drawable.ic_delete_black_24dp);
        tableRow.addView(deleteImage,newTableRowParams());
        //Create Text
        while(indexC<header.length){
            newCell();
            txtCell.setText(header[indexC++]);
            tableRow.addView(txtCell,newTableRowParams());
        }
        tableLayout.addView(tableRow);
    }
    private void createDataTable(){
        String info;
        for(indexR = 1; indexR <= data.size(); indexR++){
            newRow(indexR);
            createDeleteButtonCell(indexR, data.get(indexR - 1).ID);
            tableRow.addView(deleteButton,newTableRowParams());

            //ArrayList to String[]
            ArrayList<String> lista = new ArrayList<>();
            DecimalFormat decimalFormat = new DecimalFormat("#.00");
            lista.add(data.get(indexR-1).NombreProducto);
            lista.add(decimalFormat.format(data.get(indexR-1).Total));
            lista.add(decimalFormat.format(data.get(indexR-1).Precio));
            lista.add(decimalFormat.format(data.get(indexR-1).Cantidad));
            lista.add(data.get(indexR-1).PuestoAcronimo);

            String[] row = lista.toArray(new String[lista.size()]);
            for(indexC = 0; indexC < header.length; indexC++){
                newCell();
                info = (indexC<row.length)?row[indexC]:"";
                txtCell.setText(info);
                tableRow.addView(txtCell,newTableRowParams());
            }
            tableLayout.addView(tableRow);
        }
    }
    public void addItem(Presupuesto_x_Producto_x_Puesto item){
        String info;
        data.add(item);
        indexC=0;
        newRow(data.size());
        createDeleteButtonCell(data.size(), item.ID);
        tableRow.addView(deleteButton,newTableRowParams());
        //ArrayList to String[]
        ArrayList<String> lista = new ArrayList<>();
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        lista.add(item.NombreProducto);
        lista.add(decimalFormat.format(item.Total));
        lista.add(decimalFormat.format(item.Precio));
        lista.add(decimalFormat.format(item.Cantidad));
        lista.add(item.PuestoAcronimo);
        while (indexC<header.length){
            newCell();
            info = (indexC<lista.size())?lista.get(indexC++):"";
            txtCell.setText(info);
            tableRow.addView(txtCell,newTableRowParams());
        }
        tableLayout.addView(tableRow,data.size());
        reColoring();
    }
    public void deleteItem(int index){
        data.remove(index - 1);
        tableLayout.removeView(getRow(index));
        //Asign the numbers again
        for(int a=1;a<=data.size();a++){
            tableRow = getRow(a);
            ImageButton btn = (ImageButton) tableRow.getChildAt(0);
            btn.setId(a);
        }
        //Paint
        multiColor  = false;
        backgroundData(firstColor,secondColor);
        lineColor(colorLine);
    }
    public void backgroundHeader(int color){
        indexC = 0;
        while(indexC<header.length){
            txtCell = getCell(0,indexC++);
            txtCell.setBackgroundColor(color);
        }
    }

    public void backgroundData(int firstColor, int secondColor){
        for(indexR = 1; indexR <= data.size(); indexR++){
            multiColor = !multiColor;
            for(indexC = 0; indexC < header.length; indexC++){
                txtCell = getCell(indexR,indexC);
                txtCell.setBackgroundColor((multiColor)?firstColor:secondColor);
            }
        }
        this.firstColor = firstColor;
        this.secondColor = secondColor;
    }
    public void lineColor(int color){
        indexR = 0;
        while(indexR <= data.size()){
            getRow(indexR++).setBackgroundColor(color);
        }
        this.colorLine = color;
    }
    public void textColorData(int color){
        for(indexR = 1; indexR <= data.size(); indexR++){
            for(indexC = 0; indexC < header.length; indexC++){
                getCell(indexR,indexC).setTextColor(color);
            }
        }
        this.textColor = color;
    }
    public void textColorHeader(int color){
        indexC = 0;
        while(indexC <header.length){
            getCell(0,indexC++).setTextColor(color);
        }
    }
    public void reColoring(){
        indexC = 0;
        multiColor = !multiColor;
        while(indexC<header.length){
            txtCell = getCell(data.size(),indexC++);
            txtCell.setBackgroundColor((multiColor)?firstColor:secondColor);
            txtCell.setTextColor(textColor);
            //Line color
            getRow(data.size()).setBackgroundColor(colorLine);
        }

    }
    private TableRow getRow(int index){
        return (TableRow) tableLayout.getChildAt(index);
    }
    private TextView getCell(int rowIndex, int columIndex){
        tableRow = getRow(rowIndex);
        return (TextView) tableRow.getChildAt(columIndex + 1); // Es mas 1 porque hay un boton en la primera columna
    }
    private TableRow.LayoutParams newTableRowParams(){
        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT);
        params.setMargins(1,1,1,1);
        params.weight=1;
        return params;
    }
    public void setProductosTableDynamicListener(ProductosTableDynamicListener listener){
        this.listener = listener;
    }
    public interface ProductosTableDynamicListener{
        void onClickRowListener(int index);
        void onClickDeleteItem(int idButton, int idProducto);
    }
}
