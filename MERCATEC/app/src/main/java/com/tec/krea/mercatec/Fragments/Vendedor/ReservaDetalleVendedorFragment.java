package com.tec.krea.mercatec.Fragments.Vendedor;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.tec.krea.mercatec.Entities.Conexion;
import com.tec.krea.mercatec.Entities.Reserva;
import com.tec.krea.mercatec.Entities.ReservaDetalle;
import com.tec.krea.mercatec.R;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReservaDetalleVendedorFragment extends Fragment {


    public ReservaDetalleVendedorFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view=inflater.inflate(R.layout.fragment_reserva_detalle_vendedor, container, false);

        TextView estado=view.findViewById(R.id.txt_r_v_estado);
        TextView producto=view.findViewById(R.id.txt_r_v_producto);
        TextView fecha=view.findViewById(R.id.txt_r_v_fecha);
        TextView puesto=view.findViewById(R.id.txt_r_v_puesto);
        TextView volumen=view.findViewById(R.id.txt_r_v_volumen);
        TextView precio=view.findViewById(R.id.txt_r_v_precio);
        TextView ganancia=view.findViewById(R.id.txt_r_v_ganancia);
        Button btnAtender=view.findViewById(R.id.btn_atender_reserva);

        ReservaDetalle reservaDetalle = null;
        if (getArguments()!=null){
            reservaDetalle=(ReservaDetalle)getArguments().get("reserva");
            Log.println(Log.INFO, "INFO", reservaDetalle.NombreProducto+" "+reservaDetalle.AcronimoPuesto
            +" "+String.valueOf(reservaDetalle.Precio));

            producto.setText(reservaDetalle.NombreProducto);
            puesto.setText("Puesto: "+reservaDetalle.AcronimoPuesto);

            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date= null;
            try {
                date = sdf.parse(reservaDetalle.Fecha);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            String fecha_registro=String.valueOf(cal.get(Calendar.DAY_OF_MONTH)+
                    "/"+String.valueOf(cal.get(Calendar.MONTH)+1)+"/"+String.valueOf(cal.get(Calendar.YEAR)));
            fecha.setText("Fecha: "+fecha_registro);

            volumen.setText("Cantidad: "+String.valueOf(reservaDetalle.Cantidad));
            precio.setText("Precio: "+String.valueOf(reservaDetalle.Precio));
            ganancia.setText("Ganancia: S/."+String.valueOf(reservaDetalle.Ganancia));
            if (reservaDetalle.Estado==true){
                estado.setText("Estado: Reserva en espera...");
            }else{
                estado.setText("Estado: Reserva ya atendida");
            }
        }

        final ReservaDetalle finalReservaDetalle = reservaDetalle;
        final Conexion conexion=new Conexion();

        btnAtender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  atender reserva
                Reserva objReserva=new Reserva();
                objReserva.ID=finalReservaDetalle.ID;
                objReserva.Producto_x_PuestoID=finalReservaDetalle.Producto_x_PuestoID;
                objReserva.UsuarioID=finalReservaDetalle.UsuarioID;
                objReserva.Cantidad=finalReservaDetalle.Cantidad;

                DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                Date date= null;
                try {
                    date = sdf.parse(finalReservaDetalle.Fecha);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                objReserva.Fecha=date;
                objReserva.Estado=true;

                AndroidNetworking.put(conexion.pathAPI+"/api/Reserva/{id}")
                        .addPathParameter("id",Integer.toString(objReserva.ID))
                        .addBodyParameter(objReserva)
                        .setTag("reserva")
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsOkHttpResponse(new OkHttpResponseListener() {
                            @Override
                            public void onResponse(Response response) {
                                Log.println(Log.INFO,
                                        "INFO",
                                        "Response Code "+Integer.toString(response.code()));
                                Toast.makeText(view.getContext(),
                                        "Se registro la atencion de la reserva.",
                                        Toast.LENGTH_SHORT).show();
                                FragmentManager fragmentManager =
                                        getActivity().getSupportFragmentManager();
                                FragmentTransaction fragmentTransaction =
                                        fragmentManager.beginTransaction();
                                ReservaFragment reservasFragment = new ReservaFragment();
                                fragmentTransaction.replace(R.id.fragment_layout_vendedor, reservasFragment);
                                fragmentTransaction.commit();
                            }

                            @Override
                            public void onError(ANError anError) {
                                anError.printStackTrace();
                                Toast.makeText(view.getContext(),
                                        "No se pudo atender la reserva.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });

        return view;
    }

}
