package com.tec.krea.mercatec.Fragments.Vendedor;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tec.krea.mercatec.Entities.ReservaDetalle;
import com.tec.krea.mercatec.Fragments.Vendedor.ReservaFragment.OnListFragmentInteractionListener;
import com.tec.krea.mercatec.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link ReservaDetalle} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class ReservasRecyclerViewAdapter extends RecyclerView.Adapter<ReservasRecyclerViewAdapter.ViewHolder> {

    private final List<ReservaDetalle> mValues;
    private final OnListFragmentInteractionListener mListener;

    public ReservasRecyclerViewAdapter(List<ReservaDetalle> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_reserva, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        holder.mPuestoView.setText("Puesto: "+mValues.get(position).AcronimoPuesto);
        holder.mProductoView.setText("Producto: "+mValues.get(position).NombreProducto);
        holder.mCantidadView.setText("Volumen: "+ Double.toString(mValues.get(position).Cantidad)+"\t\t");
        holder.mPrecioView.setText("Precio: S/."+Double.toString(mValues.get(position).Precio));
        holder.mGananciaView.setText("Ganancia: S/."+Double.toString(mValues.get(position).Ganancia)+"\t\t");
        String fecha=mValues.get(position).Fecha;
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date date= null;
        try {
            date = sdf.parse(fecha);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        String fecha_registro=String.valueOf(cal.get(Calendar.DAY_OF_MONTH)+
                "/"+String.valueOf(cal.get(Calendar.MONTH)+1)+"/"+String.valueOf(cal.get(Calendar.YEAR)));
        holder.mFechaReservaView.setText("Fecha: "+ fecha_registro);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mPuestoView;
        public final TextView mProductoView;
        public final TextView mCantidadView;
        public final TextView mPrecioView;
        public final TextView mGananciaView;
        public final TextView mFechaReservaView;
        public ReservaDetalle mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mPuestoView=(TextView) view.findViewById(R.id.lbl_reserva_Puesto);
            mProductoView = (TextView) view.findViewById(R.id.lbl_reserva_Producto);
            mCantidadView = (TextView) view.findViewById(R.id.lbl_reserva_Cantidad);
            mPrecioView = (TextView) view.findViewById(R.id.lbl_reserva_Precio);
            mGananciaView = (TextView) view.findViewById(R.id.lbl_reserva_Ganancia);
            mFechaReservaView=(TextView)view.findViewById(R.id.lbl_reserva_Fecha_reserva);
        }

        @Override
        public String toString() {
            return super.toString() + "Puesto: " + mPuestoView.getText() + "\n"+
                    "Producto: "+mProductoView.getText()+"\n"+
                    "Cantidad: "+mCantidadView.getText()+"\n"+
                    "Precio: "+mPrecioView.getText()+"\n"+
                    "Ganancia: "+mGananciaView.getText()+"\n"+
                    "Fecha de registro: "+mFechaReservaView.getText();
        }
    }
}
