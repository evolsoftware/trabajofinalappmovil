package com.tec.krea.mercatec.Fragments.Comprador;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tec.krea.mercatec.Entities.Producto_x_Puesto;
import com.tec.krea.mercatec.Fragments.Comprador.PreciosPorProductoFragment.OnListFragmentInteractionListener;
import com.tec.krea.mercatec.Fragments.Comprador.dummy.DummyContent.DummyItem;
import com.tec.krea.mercatec.R;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MisPreciosPorProductoRecyclerViewAdapter extends RecyclerView.Adapter<MisPreciosPorProductoRecyclerViewAdapter.ViewHolder> {

    private final List<Producto_x_Puesto> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MisPreciosPorProductoRecyclerViewAdapter(List<Producto_x_Puesto> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_preciosporproducto, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mPuesto.setText("Puesto: " + mValues.get(position).PuestoAcronimo);
        holder.mPrecio.setText("Precio: " + String.valueOf(mValues.get(position).Precio));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListProductoxPuestoFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mPuesto;
        public final TextView mPrecio;
        public Producto_x_Puesto mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mPuesto = (TextView) view.findViewById(R.id.puesto_producto);
            mPrecio = (TextView) view.findViewById(R.id.precio_producto);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mPuesto.getText() + "'" + mPrecio.getText();
        }
    }
}
