package com.tec.krea.mercatec.Entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

import java.util.Date;

/**
 * Created by K-PC on 22/10/2018.
 */

public class ReservaDetalle implements Parcelable, Cloneable {
     public int ID;
     public int Producto_x_PuestoID;
     public int UsuarioID;
     public Double Cantidad;
     public String Fecha;
     public boolean Estado;
     public String AcronimoPuesto;
     public String NombreProducto;
     public Double Ganancia;
     public Double Precio;

     public ReservaDetalle(){

     }
     @Override
     public Object clone(){
          Parcel parcel = Parcel.obtain();
          this.writeToParcel(parcel, 0);
          byte[] bytes = parcel.marshall();

          Parcel parcel2 = Parcel.obtain();
          parcel2.unmarshall(bytes, 0, bytes.length);
          parcel2.setDataPosition(0);
          return ReservaDetalle.CREATOR.createFromParcel(parcel2);
     }

     protected ReservaDetalle(Parcel in) {
          ID = in.readInt();
          Producto_x_PuestoID = in.readInt();
          UsuarioID = in.readInt();
          if (in.readByte() == 0) {
               Cantidad = null;
          } else {
               Cantidad = in.readDouble();
          }
          Fecha = in.readString();
          Estado = in.readByte() != 0;
          AcronimoPuesto = in.readString();
          NombreProducto = in.readString();
          if (in.readByte() == 0) {
               Ganancia = null;
          } else {
               Ganancia = in.readDouble();
          }
          if (in.readByte() == 0) {
               Precio = null;
          } else {
               Precio = in.readDouble();
          }
     }

     @Override
     public void writeToParcel(Parcel dest, int flags) {
          dest.writeInt(ID);
          dest.writeInt(Producto_x_PuestoID);
          dest.writeInt(UsuarioID);
          if (Cantidad == null) {
               dest.writeByte((byte) 0);
          } else {
               dest.writeByte((byte) 1);
               dest.writeDouble(Cantidad);
          }
          dest.writeString(Fecha);
          dest.writeByte((byte) (Estado ? 1 : 0));
          dest.writeString(AcronimoPuesto);
          dest.writeString(NombreProducto);
          if (Ganancia == null) {
               dest.writeByte((byte) 0);
          } else {
               dest.writeByte((byte) 1);
               dest.writeDouble(Ganancia);
          }
          if (Precio == null) {
               dest.writeByte((byte) 0);
          } else {
               dest.writeByte((byte) 1);
               dest.writeDouble(Precio);
          }
     }

     @Override
     public int describeContents() {
          return 0;
     }

     public static final Creator<ReservaDetalle> CREATOR = new Creator<ReservaDetalle>() {
          @Override
          public ReservaDetalle createFromParcel(Parcel in) {
               return new ReservaDetalle(in);
          }

          @Override
          public ReservaDetalle[] newArray(int size) {
               return new ReservaDetalle[size];
          }
     };
}
