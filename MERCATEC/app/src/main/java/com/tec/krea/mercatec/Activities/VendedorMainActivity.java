package com.tec.krea.mercatec.Activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.tec.krea.mercatec.Entities.DetalleProducto;
import com.tec.krea.mercatec.Entities.Puesto;
import com.tec.krea.mercatec.Entities.ReservaDetalle;
import com.tec.krea.mercatec.Fragments.Comprador.ProductosPorPresupuestoFragment;
import com.tec.krea.mercatec.Fragments.Vendedor.AgregarProductoAPuestoFragment;
import com.tec.krea.mercatec.Fragments.Vendedor.Dialogs.AgregarPuestoDialog;
import com.tec.krea.mercatec.Fragments.Vendedor.MisProductosPorPuestoFragment;
import com.tec.krea.mercatec.Fragments.Vendedor.MisPuestosFragment;
import com.tec.krea.mercatec.Fragments.Vendedor.PerfilVendedorFragment;
import com.tec.krea.mercatec.Fragments.Vendedor.ReservaDetalleVendedorFragment;
import com.tec.krea.mercatec.R;
import com.tec.krea.mercatec.Fragments.Vendedor.ReservaFragment;

public class VendedorMainActivity extends AppCompatActivity implements
MisPuestosFragment.OnListFragmentInteractionListener,
MisProductosPorPuestoFragment.OnListFragmentInteractionListener,
AgregarProductoAPuestoFragment.OnFragmentInteractionListener,
ReservaFragment.OnListFragmentInteractionListener,
PerfilVendedorFragment.OnFragmentInteractionListener,
AgregarPuestoDialog.AgregarPuestoDialogListener{

    private TextView mTextMessage;
    Puesto objPuesto;
    ReservaDetalle objReserva;
    Fragment fragmentAnterior;
    boolean enable;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            //Disable back button
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);

            switch (item.getItemId()) {
                case R.id.navigation_vendedor_mis_puestos:
                    MisPuestosFragment misPuestosFragment = new MisPuestosFragment();
                    reemplazarFragment(misPuestosFragment);
                    return true;
                case R.id.navigation_vendedor_reservas:
                    ReservaFragment reservasFragment = new ReservaFragment();
                    reemplazarFragment(reservasFragment);
                    return true;
                case R.id.navigation_vendedor_perfil:
                    PerfilVendedorFragment perfilVendedorFragment = new PerfilVendedorFragment();
                    reemplazarFragment(perfilVendedorFragment);
                    return true;
            }
            return false;
        }
    };

    private void reemplazarFragment(Fragment myFragment){
        FragmentManager fragmentManager =
                getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_layout_vendedor, myFragment);
        fragmentTransaction.commit();
    }

    public void AsignarFragment(Fragment fragment){
        this.fragmentAnterior = fragment;
    }
    public void EnableBackButtonActionBar(){
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(fragmentAnterior != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(enable);
            getSupportActionBar().setDisplayHomeAsUpEnabled(enable);
            reemplazarFragment(fragmentAnterior);
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendedor_main);

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.vendedor_navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        MisPuestosFragment misPuestosFragment = new MisPuestosFragment();
        reemplazarFragment(misPuestosFragment);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onAddPuestoPressed() {
        AgregarPuestoDialog dialog = new AgregarPuestoDialog();
        dialog.setCancelable(false);
        dialog.show(getSupportFragmentManager(), "Agregar_Puesto");
    }

    @Override
    public void onListMisPuestosFragmentInteraction(Puesto item) {
        objPuesto = item;
        MisProductosPorPuestoFragment productosPorPuestoFragment = new MisProductosPorPuestoFragment();
        reemplazarFragment(productosPorPuestoFragment);
    }


    @Override
    public void GuardarPuesto() {
        MisPuestosFragment fragment = new MisPuestosFragment();
        reemplazarFragment(fragment);
        Toast.makeText(getApplicationContext(),"Nuevo puesto registrado", Toast.LENGTH_SHORT).show();
    }

    @Override
    public Puesto getMisPuestosFragmentInteraction() {
        return objPuesto;
    }

    @Override
    public void GuardarProductoPorPuesto() {
        MisProductosPorPuestoFragment productosPorPuestoFragment = new MisProductosPorPuestoFragment();
        reemplazarFragment(productosPorPuestoFragment);
    }

    @Override
    public void onClickAgregarProductoAPuestoBackButton(boolean enable) {
        fragmentAnterior = new MisProductosPorPuestoFragment();
        this.enable = enable;
        EnableBackButtonActionBar();
    }

    @Override
    public void onListProductoPorPuestoFragmentInteraction(DetalleProducto item) {

    }

    @Override
    public void onAddPoductoAPuestoPressed() {
        AgregarProductoAPuestoFragment fragment = new AgregarProductoAPuestoFragment();
        reemplazarFragment(fragment);
    }

    @Override
    public void onClickProductosPorPuestoBackButton(boolean enable) {
        fragmentAnterior = new MisPuestosFragment();
        this.enable = enable;
        EnableBackButtonActionBar();
    }

    @Override
    public void onListFragmentInteraction(ReservaDetalle reservaDetalle) {
        Log.println(Log.INFO, "INFO", reservaDetalle.AcronimoPuesto +" "+reservaDetalle.NombreProducto
        +" "+reservaDetalle.Fecha.toString());

        Bundle bundle=new Bundle();
        bundle.putParcelable("reserva", (ReservaDetalle)reservaDetalle.clone());
        ReservaDetalleVendedorFragment reservaDetalleVendedorFragment=new ReservaDetalleVendedorFragment();
        reservaDetalleVendedorFragment.setArguments(bundle);
        reemplazarFragment(reservaDetalleVendedorFragment);
    }
}
