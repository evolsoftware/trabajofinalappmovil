package com.tec.krea.mercatec.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.androidnetworking.AndroidNetworking;
import com.tec.krea.mercatec.R;

public class CuentasActivity extends AppCompatActivity {

    private Button btnComprador;
    private Button btnVendedor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuentas);

        btnComprador = findViewById(R.id.btnComprador);
        btnVendedor = findViewById(R.id.btnVendedor);

        btnComprador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CompradorMainActivity.class);
                startActivity(intent);
            }
        });

        btnVendedor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), VendedorMainActivity.class);
                startActivity(intent);
            }
        });
    }
}
