package com.tec.krea.mercatec.Fragments.Vendedor;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tec.krea.mercatec.Entities.DetalleProducto;
import com.tec.krea.mercatec.Entities.Producto_x_Puesto;
import com.tec.krea.mercatec.Fragments.Vendedor.MisProductosPorPuestoFragment.OnListFragmentInteractionListener;
import com.tec.krea.mercatec.Fragments.Vendedor.dummy.DummyContent.DummyItem;
import com.tec.krea.mercatec.R;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MisProductosPorPuestoRecyclerViewAdapter extends RecyclerView.Adapter<MisProductosPorPuestoRecyclerViewAdapter.ViewHolder> {

    private final List<DetalleProducto> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MisProductosPorPuestoRecyclerViewAdapter(List<DetalleProducto> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_misproductosporpuesto, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mProducto.setText(mValues.get(position).Nombre_Tipo_Producto + " " + mValues.get(position).Nombre);
        holder.mPrecio.setText("S/." + mValues.get(position).Precio);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListProductoPorPuestoFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mProducto;
        public final TextView mPrecio;
        public DetalleProducto mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mProducto = (TextView) view.findViewById(R.id.vendedor_producto_puesto);
            mPrecio = (TextView) view.findViewById(R.id.vendedor_precio_producto);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mProducto.getText() + "'" + mPrecio.getText();
        }
    }
}
