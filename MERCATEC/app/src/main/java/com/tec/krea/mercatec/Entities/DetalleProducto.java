package com.tec.krea.mercatec.Entities;

/**
 * Created by K-PC on 14/10/2018.
 */

public class DetalleProducto {
    public int ID;
    public String Nombre;
    public String ImagePath;
    public Double Precio;
    public String Nombre_Tipo_Producto;
    public String Nombre_Grupo_Alimenticio;
}
