package com.tec.krea.mercatec.Fragments.Comprador;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tec.krea.mercatec.Entities.Producto;
import com.tec.krea.mercatec.Fragments.Comprador.ProductosParaCompradorFragment.OnListFragmentInteractionListener;
import com.tec.krea.mercatec.Fragments.Comprador.dummy.DummyContent.DummyItem;
import com.tec.krea.mercatec.R;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MisProductosParaCompradorRecyclerViewAdapter extends RecyclerView.Adapter<MisProductosParaCompradorRecyclerViewAdapter.ViewHolder> {

    private final List<Producto> mValues;
    private final OnListFragmentInteractionListener mListener;
    private Context context;
    public MisProductosParaCompradorRecyclerViewAdapter(List<Producto> items, OnListFragmentInteractionListener listener, Context context) {
        mValues = items;
        mListener = listener;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_productosparacomprador, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mNombre.setText(mValues.get(position).Nombre);
        Glide.with(context)
                .load(mValues.get(position).ImagePath)
                .placeholder(R.drawable.ic_image_black_24dp)
                .centerCrop()
                .into(holder.mImagen);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListProductosFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mNombre;
        public final ImageView mImagen;
        public Producto mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNombre = (TextView) view.findViewById(R.id.producto_para_comprador_nombre);
            mImagen = view.findViewById(R.id.producto_para_comprador_imagen);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mNombre.getText() + "'";
        }
    }
}
