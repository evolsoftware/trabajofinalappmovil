package com.tec.krea.mercatec.Fragments.Comprador;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tec.krea.mercatec.Entities.Tipo_Producto;
import com.tec.krea.mercatec.R;
import com.tec.krea.mercatec.Fragments.Comprador.TipoProductoFragment.OnListFragmentInteractionListener;
import com.tec.krea.mercatec.Fragments.dummy.DummyContent.DummyItem;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MiTipoProductoRecyclerViewAdapter extends RecyclerView.Adapter<MiTipoProductoRecyclerViewAdapter.ViewHolder> {

    private final List<Tipo_Producto> mValues;
    private final GruposAlimenticiosFragment.OnListFragmentInteractionListener mListener;
    private Context context;

    public MiTipoProductoRecyclerViewAdapter(List<Tipo_Producto> items, GruposAlimenticiosFragment.OnListFragmentInteractionListener listener, Context context) {
        mValues = items;
        mListener = listener;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_tipoproducto, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mNombre.setText(mValues.get(position).Nombre);
        Glide.with(context)
                .load(mValues.get(position).ImagePath)
                .placeholder(R.drawable.ic_image_black_24dp)
               .centerCrop()
                .into(holder.mImagen);
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListGrupoAlimenticioFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mNombre;
        public final ImageView mImagen;
        public Tipo_Producto mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNombre = (TextView) view.findViewById(R.id.tipo_producto_nombre);
            mImagen = view.findViewById(R.id.tipo_producto_imagen);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }
}
