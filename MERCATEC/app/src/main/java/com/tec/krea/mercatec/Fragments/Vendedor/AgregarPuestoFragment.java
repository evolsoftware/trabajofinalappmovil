package com.tec.krea.mercatec.Fragments.Vendedor;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tec.krea.mercatec.Entities.Conexion;
import com.tec.krea.mercatec.Entities.Puesto;
import com.tec.krea.mercatec.R;

import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AgregarPuestoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AgregarPuestoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AgregarPuestoFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private EditText txtAcronimo;
    private Button btnAgregarPuesto;

    private OnFragmentInteractionListener mListener;

    public AgregarPuestoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AgregarPuestoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AgregarPuestoFragment newInstance(String param1, String param2) {
        AgregarPuestoFragment fragment = new AgregarPuestoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        AndroidNetworking.initialize(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_agregar_puesto, container, false);

        final Conexion conexion=new Conexion();
        btnAgregarPuesto = view.findViewById(R.id.btnRegistrarPuesto);
        txtAcronimo = view.findViewById(R.id.txtVendedorAgregarPuestoAcronimo);

        btnAgregarPuesto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String acr = txtAcronimo.getText().toString();

                if(!acr.equals(""))
                {
                    Puesto objPuesto = new Puesto();
                    objPuesto.Acronimo = txtAcronimo.getText().toString();

                    SharedPreferences prefs = getActivity().getSharedPreferences("Login", Context.MODE_PRIVATE);
                    int storedUserID = prefs.getInt("UsuarioID", 0);
                    objPuesto.UsuarioID = storedUserID;

                    AndroidNetworking.post(conexion.pathAPI+"/api/puesto")
                            .addBodyParameter(objPuesto)
                            .setTag("puesto")
                            .setPriority(Priority.LOW)
                            .build()
                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.println(Log.ASSERT, "Èxito", "Registro exitoso");
                                    if (mListener != null)
                                    {
                                        mListener.onRegistrarPuestoPressed();
                                    }
                                }

                                @Override
                                public void onError(ANError anError) {
                                    Log.println(Log.ERROR, "Error", "Registro fallido" +
                                            anError.getErrorBody() +
                                            " - " + anError.getErrorDetail() +
                                            " - " + anError.getResponse());
                                }
                            });
                }

                else
                {
                    Toast.makeText(getContext(),"Por favor complete el campo", Toast.LENGTH_SHORT).show();
                }

            }
        });

        return  view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
        void onRegistrarPuestoPressed();
    }
}
