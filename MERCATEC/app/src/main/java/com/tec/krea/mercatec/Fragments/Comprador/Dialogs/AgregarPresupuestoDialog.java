package com.tec.krea.mercatec.Fragments.Comprador.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tec.krea.mercatec.Entities.Conexion;
import com.tec.krea.mercatec.Entities.Presupuesto;
import com.tec.krea.mercatec.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class AgregarPresupuestoDialog extends AppCompatDialogFragment {
   private EditText txtNombre;

   private AgregarPresupuestoDialogListener listener;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_agregar_presupuesto,null);

        AndroidNetworking.initialize(getContext());
        //TextView title = new TextView(getActivity());
        //title.setText("Agregar Presupuesto");
        //title.setTextSize(20);
        //title.setGravity(Gravity.CENTER);
        builder.setView(view)
                .setTitle("Agregar Presupuesto")
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setPositiveButton("Guardar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        txtNombre = view.findViewById(R.id.txtNombrePresupuesto);
        return builder.create();
    }

    @Override
    public void onResume() {
        super.onResume();
        final Conexion conexion=new Conexion();
        final AlertDialog d = (AlertDialog) getDialog();
        if(d!=null){
            Button positiveButton = d.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Boolean wantToCloseDialog = false;
                    String Nombre = txtNombre.getText().toString();
                    if(!Nombre.equals("")){
                        //implementar aqui la API
                        SharedPreferences prefs = getActivity().getSharedPreferences("Login", Context.MODE_PRIVATE);
                        int storedUserID = prefs.getInt("UsuarioID", 0);
                        final Presupuesto objPresupesto = new Presupuesto();
                        objPresupesto.UsuarioID = storedUserID;
                        objPresupesto.Nombre = Nombre;
                        objPresupesto.Fecha = new Date();
                        objPresupesto.Estado_uso = true;
                        AndroidNetworking.post(conexion.pathAPI+"/api/presupuesto")
                                .addBodyParameter(objPresupesto)
                                .setTag("presupuesto")
                                .setPriority(Priority.LOW)
                                .build()
                                .getAsJSONObject(new JSONObjectRequestListener() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        try {
                                            objPresupesto.ID = response.getInt("ID");
                                            Log.println(Log.ASSERT, "Éxito", "Registro exitoso");
                                            listener.GuardarPresupuesto(objPresupesto);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    @Override
                                    public void onError(ANError anError) {
                                        Log.println(Log.ERROR, "Error", "Registro fallido" +
                                                anError.getErrorBody() +
                                                " - " + anError.getErrorDetail() +
                                                " - " + anError.getResponse());
                                    }
                                });
                        wantToCloseDialog = true;
                        //
                    }
                    else{
                        Toast.makeText(getContext(),"Ingresar el nombre del presupuesto", Toast.LENGTH_SHORT).show();
                    }
                    if(wantToCloseDialog){
                        d.dismiss();
                    }
                }
            });
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            listener = (AgregarPresupuestoDialogListener) context;
        }catch (ClassCastException e){
            throw  new ClassCastException(context.toString() +
            "must implement AgregarPresupuestoDialogListener");
        }
    }

    public interface AgregarPresupuestoDialogListener{
        void GuardarPresupuesto(Presupuesto objPresupuesto);
    }
}
