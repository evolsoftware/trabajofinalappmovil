package com.tec.krea.mercatec.Activities;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.tec.krea.mercatec.Entities.Grupo_Alimenticio;
import com.tec.krea.mercatec.Entities.Presupuesto;
import com.tec.krea.mercatec.Entities.Producto;
import com.tec.krea.mercatec.Entities.Producto_x_Puesto;
import com.tec.krea.mercatec.Entities.Reserva;
import com.tec.krea.mercatec.Entities.ReservaDetalle;
import com.tec.krea.mercatec.Entities.Tipo_Producto;
import com.tec.krea.mercatec.Fragments.Comprador.AgregarNuevoProductoAPresupuesto;
import com.tec.krea.mercatec.Fragments.Comprador.Dialogs.AgregarPresupuestoDialog;
import com.tec.krea.mercatec.Fragments.Comprador.Dialogs.EliminarPresupuestoDialog;
import com.tec.krea.mercatec.Fragments.Comprador.GruposAlimenticiosFragment;
import com.tec.krea.mercatec.Fragments.Comprador.MisPresupuestosFragment;
import com.tec.krea.mercatec.Fragments.Comprador.MisPresupuestosParaSeleccionarFragment;
import com.tec.krea.mercatec.Fragments.Comprador.MisReservasFragment;
import com.tec.krea.mercatec.Fragments.Comprador.PerfilCompradorFragment;
import com.tec.krea.mercatec.Fragments.Comprador.PreciosPorProductoFragment;
import com.tec.krea.mercatec.Fragments.Comprador.ProductosFragment;
import com.tec.krea.mercatec.Fragments.Comprador.ProductosParaCompradorFragment;
import com.tec.krea.mercatec.Fragments.Comprador.ProductosPorPresupuestoFragment;
import com.tec.krea.mercatec.Fragments.Comprador.ReservaDetalleComprador;
import com.tec.krea.mercatec.R;

public class CompradorMainActivity extends AppCompatActivity implements
ProductosFragment.OnFragmentInteractionListener,
MisPresupuestosFragment.OnListFragmentInteractionListener,
MisReservasFragment.OnListFragmentInteractionListener,
PerfilCompradorFragment.OnFragmentInteractionListener,
GruposAlimenticiosFragment.OnListFragmentInteractionListener,
ProductosParaCompradorFragment.OnListFragmentInteractionListener,
PreciosPorProductoFragment.OnListFragmentInteractionListener,
MisPresupuestosParaSeleccionarFragment.OnListFragmentInteractionListener,
AgregarPresupuestoDialog.AgregarPresupuestoDialogListener,
EliminarPresupuestoDialog.EliminarPresupuestoDialogListener,
ProductosPorPresupuestoFragment.OnFragmentInteractionListener,
AgregarNuevoProductoAPresupuesto.OnFragmentInteractionListener{

    private TextView mTextMessage;
    //Para el comprador en la seccion de productos
    Grupo_Alimenticio objGrupoAlimenticio;
    Tipo_Producto objTipoProducto;
    Producto objProducto;
    Producto_x_Puesto objProductoxPuesto;
    Presupuesto objPresupuesto;
    ////////////////////
    //Para el ActionBar
    Fragment fragment;
    boolean enable;
    //
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            //Disable back button
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);

            switch (item.getItemId()) {
                case R.id.navigation_comprador_productos:
                    GruposAlimenticiosFragment gruposAlimenticiosFragment = new GruposAlimenticiosFragment();
                    reemplazarFragment(gruposAlimenticiosFragment);
                    return true;
                case R.id.navigation_comprador_mis_reservas:
                    MisReservasFragment misReservasFragment = new MisReservasFragment();
                    reemplazarFragment(misReservasFragment);
                    return true;
                case R.id.navigation_comprador_mis_presupuestos:
                    MisPresupuestosFragment misPresupuestosFragment = new MisPresupuestosFragment();
                    reemplazarFragment(misPresupuestosFragment);
                    return true;
                case R.id.navigation_comprador_perfil:
                    PerfilCompradorFragment perfilCompradorFragment = new PerfilCompradorFragment();
                    reemplazarFragment(perfilCompradorFragment);
                    return true;
            }
            return false;
        }
    };

    private void reemplazarFragment(Fragment myFragment){
        FragmentManager fragmentManager =
                getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_layout_comprador, myFragment);
        fragmentTransaction.commit();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comprador_main);

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.comprador_navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        GruposAlimenticiosFragment gruposAlimenticiosFragment = new GruposAlimenticiosFragment();
        reemplazarFragment(gruposAlimenticiosFragment);
    }

    public void AsignarFragment(Fragment fragment){
        this.fragment = fragment;
    }
    public void EnableBackButtonActionBar(){
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(fragment != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(enable);
            getSupportActionBar().setDisplayHomeAsUpEnabled(enable);
            reemplazarFragment(fragment);
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public Presupuesto getPresupuestoAgregarNuevoProductoAPresupuesto() {
        return objPresupuesto;
    }

    @Override
    public void GuardarNuevoProductoAPresupuesto(Double actualizarTotal) {
        objPresupuesto.Total += actualizarTotal;
        ProductosPorPresupuestoFragment productosPorPresupuestoFragment = new ProductosPorPresupuestoFragment();
        reemplazarFragment(productosPorPresupuestoFragment);
    }

    @Override
    public void CancelarNuevoProductoAPresupuesto() {
        ProductosPorPresupuestoFragment productosPorPresupuestoFragment = new ProductosPorPresupuestoFragment();
        reemplazarFragment(productosPorPresupuestoFragment);
    }

    @Override
    public void backButtonAgregarNuevoProductoAPresupuesto(boolean enable) {
        //Enable back button
        AsignarFragment(new ProductosPorPresupuestoFragment());
        this.enable = enable;
        EnableBackButtonActionBar();
    }

    @Override
    public Presupuesto getPresupuesto() {
        return objPresupuesto;
    }

    @Override
    public void AgregarProductosPorPresupuestoListener(Presupuesto objPresupuesto) {
        this.objPresupuesto = objPresupuesto;
        AgregarNuevoProductoAPresupuesto agregarNuevoProductoAPresupuesto = new AgregarNuevoProductoAPresupuesto();
        reemplazarFragment(agregarNuevoProductoAPresupuesto);
    }

    @Override
    public void backButtonProductosPorPresupuestoListener(boolean enable) {
        //Enable back button
        AsignarFragment(new MisPresupuestosFragment());
        this.enable = enable;
        EnableBackButtonActionBar();
    }

    @Override
    public void onListGrupoAlimenticioFragmentInteraction(Tipo_Producto item) {
        objTipoProducto = item;
        ProductosParaCompradorFragment productosParaCompradorFragment = new ProductosParaCompradorFragment();
        reemplazarFragment(productosParaCompradorFragment);
    }

    @Override
    public void onListProductosFragmentInteraction(Producto item) {
        objProducto = item;
        PreciosPorProductoFragment preciosPorProductoFragment = new PreciosPorProductoFragment();
        reemplazarFragment(preciosPorProductoFragment);
    }

    @Override
    public Tipo_Producto getTipoProductoFragmentInteraction() {
        return objTipoProducto;
    }

    @Override
    public void backButtonProductosParaCompradorListener(boolean enable) {
        //Enable back button
        AsignarFragment(new GruposAlimenticiosFragment());
        this.enable = enable;
        EnableBackButtonActionBar();
    }

    @Override
    public Producto getProductoParaCompradorFragmentInteraction() {
        return objProducto;
    }

    @Override
    public void onListProductoxPuestoFragmentInteraction(Producto_x_Puesto item) {
        objProductoxPuesto = item;
    }

    @Override
    public void backButtonPreciosPorProductoListener(boolean enable) {
        //Enable back button
        AsignarFragment(new ProductosParaCompradorFragment());
        this.enable = enable;
        EnableBackButtonActionBar();
    }

    @Override
    public Producto_x_Puesto getProductoxPuestoFragmentInteraction() {
        return objProductoxPuesto;
    }

    @Override
    public void onListPresupuestosParaSeleccionarFragmentInteraction(Presupuesto presupuesto, Producto_x_Puesto producto_x_puesto) {
        objProductoxPuesto = producto_x_puesto;
        objPresupuesto = presupuesto;
    }

    @Override
    public void onListMisPresupuestosFragmentInteraction(Presupuesto item) {
        objPresupuesto = item;
        ProductosPorPresupuestoFragment productosPorPresupuestoFragment = new ProductosPorPresupuestoFragment();
        reemplazarFragment(productosPorPresupuestoFragment);
    }

    @Override
    public void AgregarPresupuestoPresionado() {
        AgregarPresupuestoDialog dialog = new AgregarPresupuestoDialog();
        dialog.setCancelable(false);
        dialog.show(getSupportFragmentManager(),"Agregar_Presupuesto_Tag");
    }

    @Override
    public void EliminarPresupuestoPresionado(Presupuesto item) {
        EliminarPresupuestoDialog dialog = new EliminarPresupuestoDialog();
        dialog.setIDPresupuesto(item.ID);
        dialog.setCancelable(false);
        dialog.show(getSupportFragmentManager(),"Eliminar_Presupuesto_Tag");
    }

    @Override
    public void GuardarPresupuesto(Presupuesto item) {
        objPresupuesto = item;
        ProductosPorPresupuestoFragment productosPorPresupuestoFragment = new ProductosPorPresupuestoFragment();
        reemplazarFragment(productosPorPresupuestoFragment);
        Toast.makeText(getApplicationContext(),"Presupuesto creado", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void AceptarEliminarPresupuesto() {
        MisPresupuestosFragment misPresupuestosFragment = new MisPresupuestosFragment();
        reemplazarFragment(misPresupuestosFragment);
    }

    @Override
    public void onListFragmentInteraction(ReservaDetalle reservaDetalle) {
        Bundle bundle=new Bundle();
        bundle.putParcelable("reserva", (ReservaDetalle)reservaDetalle.clone());
        ReservaDetalleComprador reservaDetalleComprador=new ReservaDetalleComprador();
        reservaDetalleComprador.setArguments(bundle);
        reemplazarFragment(reservaDetalleComprador);
    }
}
