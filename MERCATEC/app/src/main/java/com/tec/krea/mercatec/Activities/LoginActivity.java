package com.tec.krea.mercatec.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.tec.krea.mercatec.Entities.Cuenta;
import com.tec.krea.mercatec.Entities.Conexion;
import com.tec.krea.mercatec.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity {

    private EditText txtUsername;
    private EditText txtContraseña;
    private CheckBox cbRecuerdame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        final Conexion conexion=new Conexion();
        AndroidNetworking.initialize(getApplicationContext());

        txtUsername = findViewById(R.id.txtUsername);
        txtContraseña = findViewById(R.id.txtContraseña);
        cbRecuerdame = (CheckBox) findViewById(R.id.cbRecuerdame);

        TextView lblForgotPassword=findViewById(R.id.txtOlvidasteContraseña);
        TextView lblRegister=findViewById(R.id.lblRegister);

        SharedPreferences prefs = getSharedPreferences("Login", MODE_PRIVATE);
        String usernameGuardado = prefs.getString("Username", null);
        String contraseñaGuardado = prefs.getString("Contraseña", null);
        Boolean recuerdameGuardado = prefs.getBoolean("Recuerdame",false);

        txtUsername.setText(usernameGuardado);
        txtContraseña.setText(contraseñaGuardado);
        cbRecuerdame.setChecked(recuerdameGuardado);

        Button btnLogin = findViewById(R.id.btnIniciarSesion);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String u = txtUsername.getText().toString();
                String p = txtContraseña.getText().toString();
                if(!u.equals("") && !p.equals("")){
                    AndroidNetworking.get(conexion.pathAPI+"/api/login")
//                    AndroidNetworking.get("http://10.141.159.41/api/login")
                            .addQueryParameter("username", u)
                            .addQueryParameter("password", p)
                            .setTag("login")
                            .setPriority(Priority.LOW)
                            .build()
                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response) {

                                    try {

                                        int usuarioID = 0;
                                        usuarioID = response.getInt("ID");
                                        final SharedPreferences.Editor editor = getSharedPreferences("Login",MODE_PRIVATE).edit();
                                        editor.putInt("UsuarioID", usuarioID);
                                        //Recordar el username

                                        if(cbRecuerdame.isChecked()){
                                            editor.putString("Username", txtUsername.getText().toString());
                                            editor.putString("Contraseña", txtContraseña.getText().toString());
                                            editor.putBoolean("Recuerdame", true);
                                        }else{
                                            editor.putString("Username", "");
                                            editor.putString("Contraseña", "");
                                            editor.putBoolean("Recuerdame", false);
                                        }

                                        //Verificar las cuentas que tiene el usuario
                                        AndroidNetworking.get(conexion.pathAPI+"/api/cuentasporusuario")
//                                        AndroidNetworking.get("http://10.141.159.41/api/cuentasporusuario")
                                                .addQueryParameter("id", String.valueOf(usuarioID))
                                                .setTag("cuentasPorUsuario")
                                                .setPriority(Priority.LOW)
                                                .build()
                                                .getAsJSONArray(new JSONArrayRequestListener() {
                                                    @Override
                                                    public void onResponse(JSONArray response) {
                                                        ArrayList<Cuenta> cuentas = new ArrayList<Cuenta>();
                                                        try {
                                                            for(int a=0;a<response.length();a++) {
                                                                JSONObject jsonObject = response.getJSONObject(a);
                                                                Cuenta objCuenta = new Cuenta();
                                                                objCuenta.ID = jsonObject.getInt("ID");
                                                                objCuenta.UsuarioID = jsonObject.getInt("UsuarioID");
                                                                objCuenta.Tipo_cuenta = jsonObject.getString("Tipo_cuenta");
                                                                cuentas.add(objCuenta);
                                                            }
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }
                                                        if(cuentas.size() > 1){
                                                            editor.putInt("cuentas", 2);
                                                            Intent intent = new Intent(getApplicationContext(), CuentasActivity.class);
                                                            startActivity(intent);
                                                        }
                                                        else{
                                                            editor.putInt("cuentas", 1);
                                                            if(cuentas.get(0).Tipo_cuenta.equals("Comprador")){
                                                                Intent intent = new Intent(getApplicationContext(), CompradorMainActivity.class);
                                                                startActivity(intent);
                                                            }
                                                            else{
                                                                Intent intent = new Intent(getApplicationContext(), VendedorMainActivity.class);
                                                                startActivity(intent);
                                                            }
                                                        }
                                                        editor.apply();
                                                    }

                                                    @Override
                                                    public void onError(ANError anError) {

                                                    }
                                                });
                                        //

                                        /*
                                        //  TEST EN LOG usuario logueado
                                        Usuario user=new Usuario();
                                        user.ID=response.getInt("ID");
                                        user.Nombre=response.getString("Nombre");
                                        user.Apellidos=response.getString("Apellidos");
                                        Log.println(Log.ASSERT, "Assert", "WELCOME USUARIO: "+user.ID+" "+user.Nombre+" "+user.Apellidos);
                                        */
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        Toast.makeText(getApplicationContext(),
                                                "Error interno. Contactar con el soporte por favor.",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onError(ANError anError) {
                                    int codigoError = anError.getErrorCode();
                                    if(codigoError == 404) {
                                        Toast.makeText(getApplicationContext(),
                                                "Username o contraseña inválido",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                    else {
                                        Toast.makeText(getApplicationContext(),
                                                "Error interno. Error conexion.",
                                                Toast.LENGTH_SHORT).show();
                                    }

                                }
                            });
                }
                else{
                    Toast.makeText(getApplicationContext(),"Por favor completar todos los campos", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //Registro
        lblRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RegistroActivity.class);
                startActivity(intent);
            }
        });

        //Ovide la contraseña
        lblForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Servicio de recuperacion de cuenta no disponible!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
