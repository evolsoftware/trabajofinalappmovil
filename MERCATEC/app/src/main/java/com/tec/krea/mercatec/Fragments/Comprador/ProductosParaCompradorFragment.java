package com.tec.krea.mercatec.Fragments.Comprador;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.tec.krea.mercatec.Entities.Conexion;
import com.tec.krea.mercatec.Entities.Producto;
import com.tec.krea.mercatec.Entities.Tipo_Producto;
import com.tec.krea.mercatec.R;
import com.tec.krea.mercatec.Fragments.Comprador.dummy.DummyContent;
import com.tec.krea.mercatec.Fragments.Comprador.dummy.DummyContent.DummyItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class ProductosParaCompradorFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 3;
    private OnListFragmentInteractionListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ProductosParaCompradorFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static ProductosParaCompradorFragment newInstance(int columnCount) {
        ProductosParaCompradorFragment fragment = new ProductosParaCompradorFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_productosparacomprador_list, container, false);

        final Conexion conexion=new Conexion();
        mListener.backButtonProductosParaCompradorListener(false);

        int ID_TipoProducto = mListener.getTipoProductoFragmentInteraction().ID;
        AndroidNetworking.initialize(getContext());
        AndroidNetworking.get(conexion.pathAPI+"/api/productosportipoproducto")
                .addQueryParameter("id",String.valueOf(ID_TipoProducto))
                .setTag("productos")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        ArrayList<Producto> productos = new ArrayList<Producto>();
                        try {
                            for(int a=0;a<response.length();a++) {
                                JSONObject jsonObject = response.getJSONObject(a);
                                Producto objProducto = new Producto();
                                objProducto.ID = jsonObject.getInt("ID");
                                objProducto.Nombre = jsonObject.getString("Nombre");
                                objProducto.ImagePath = jsonObject.getString("ImagePath");
                                objProducto.Tipo_ProductoID = jsonObject.getInt("Tipo_ProductoID");
                                productos.add(objProducto);
                            }
                            // Set the adapter
                            //if (view instanceof RecyclerView) {
                            Context context = view.getContext();
                            RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.lista_productos_para_comprador);
                            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
                            recyclerView.setAdapter(new MisProductosParaCompradorRecyclerViewAdapter(productos, mListener, getContext()));
                            // }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListProductosFragmentInteraction(Producto item);
        Tipo_Producto getTipoProductoFragmentInteraction();
        void backButtonProductosParaCompradorListener(boolean enable);
    }
}
