package com.tec.krea.mercatec.Entities;

import java.util.Date;

/**
 * Created by K-PC on 14/10/2018.
 */

public class Usuario {
    public int ID;
    public String Nombre;
    public String Apellidos;
    public String Username;
    public String Password;
    public String Correo;
    public Date Fecha_registro;
}
