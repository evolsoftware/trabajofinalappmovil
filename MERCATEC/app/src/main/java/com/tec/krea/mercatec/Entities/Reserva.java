package com.tec.krea.mercatec.Entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by K-PC on 14/10/2018.
 */

public class Reserva{
    public int ID;
    public int Producto_x_PuestoID;
    public int UsuarioID;
    public Double Cantidad;
    public Date Fecha;
    public boolean Estado;

}
