package com.tec.krea.mercatec.Fragments.Comprador;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.tec.krea.mercatec.Entities.Presupuesto;
import com.tec.krea.mercatec.Fragments.Comprador.MisPresupuestosFragment.OnListFragmentInteractionListener;
import com.tec.krea.mercatec.Fragments.Comprador.dummy.DummyContent.DummyItem;
import com.tec.krea.mercatec.R;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MisPresupuestosRecyclerViewAdapter extends RecyclerView.Adapter<MisPresupuestosRecyclerViewAdapter.ViewHolder> {

    private final List<Presupuesto> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MisPresupuestosRecyclerViewAdapter(List<Presupuesto> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_mispresupuestos, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mNombre.setText(mValues.get(position).Nombre);

        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        holder.mMontoTotal.setText("Total: S/" + decimalFormat.format(mValues.get(position).Total));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListMisPresupuestosFragmentInteraction(holder.mItem);
                }
            }
        });
        holder.btnEliminarPresupuesto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.EliminarPresupuestoPresionado(holder.mItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mNombre;
        public final TextView mMontoTotal;
        public final ImageButton btnEliminarPresupuesto;
        public Presupuesto mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNombre = (TextView) view.findViewById(R.id.nombre_mis_presupuestos);
            mMontoTotal = (TextView) view.findViewById(R.id.total_mis_presupuestos);
            btnEliminarPresupuesto = view.findViewById(R.id.btnEliminarPresupuesto);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mNombre.getText() + "'" + "'" + mMontoTotal.getText() + "'";
        }
    }
}
