package com.tec.krea.mercatec.Entities;

import java.util.Date;

/**
 * Created by K-PC on 14/10/2018.
 */

public class Presupuesto {
    public int ID;
    public String Nombre;
    public int UsuarioID;
    public Date Fecha;
    public boolean Estado_uso;
    public Double Total;

}
