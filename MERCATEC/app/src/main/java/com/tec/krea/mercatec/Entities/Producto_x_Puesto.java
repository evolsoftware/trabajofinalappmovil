package com.tec.krea.mercatec.Entities;

/**
 * Created by K-PC on 14/10/2018.
 */

public class Producto_x_Puesto {
    public int ID;
    public int ProductoID;
    public int PuestoID;
    public String PuestoAcronimo;
    public Double Precio;
}
