package com.tec.krea.mercatec.Fragments.Comprador;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.tec.krea.mercatec.Entities.Conexion;
import com.tec.krea.mercatec.Entities.ReservaDetalle;
import com.tec.krea.mercatec.Fragments.Vendedor.ReservasRecyclerViewAdapter;
import com.tec.krea.mercatec.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class MisReservasFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public MisReservasFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static MisReservasFragment newInstance(int columnCount) {
        MisReservasFragment fragment = new MisReservasFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_misreservas_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            Conexion conexion=new Conexion();
            //  Ubicar datos de usuario
            SharedPreferences prefs = getActivity().getSharedPreferences("Login", Context.MODE_PRIVATE);
            int storedUserID = prefs.getInt("UsuarioID", 0);

            //  GET Reservas
            /*

            AndroidNetworking
                    .get(conexion.pathAPI+"/api/ReservasPorComprador/{id}")
                    .addPathParameter("id", Integer.toString(storedUserID))
                    .setTag("reservas")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {
                            final ArrayList<ReservaDetalle> reservas=new ArrayList<ReservaDetalle>();
                            for(int i=0;i<response.length();i++){
                                try {
                                    JSONObject jsonObject=response.getJSONObject(i);
                                    ReservaDetalle objReservaDetalle=new ReservaDetalle();
                                    objReservaDetalle.ID=jsonObject.getInt("ID");
                                    objReservaDetalle.Producto_x_PuestoID=jsonObject.getInt("Producto_x_PuestoID");
                                    objReservaDetalle.UsuarioID=jsonObject.getInt("UsuarioID");
                                    objReservaDetalle.Cantidad=jsonObject.getDouble("Cantidad");
                                    objReservaDetalle.Fecha=jsonObject.getString("Fecha");
                                    objReservaDetalle.Estado=jsonObject.getBoolean("Estado");
                                    objReservaDetalle.AcronimoPuesto=jsonObject.getString("PuestoAcronimo");
                                    objReservaDetalle.NombreProducto=jsonObject.getString("NombreProducto");
                                    objReservaDetalle.Precio=jsonObject.getDouble("Precio");
                                    objReservaDetalle.Ganancia=jsonObject.getDouble("Ganancia");

                                    reservas.add(objReservaDetalle);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                // Set the adapter
                                if (view instanceof RecyclerView) {
                                    Context context = view.getContext();
                                    RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.vendedor_list_reservas);
                                    //RecyclerView recyclerView=(RecyclerView)view;
                                    if (mColumnCount <= 1) {
                                        recyclerView.setLayoutManager(new LinearLayoutManager(context));
                                    } else {
                                        recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
                                    }
                                    recyclerView.setAdapter(new MyMisReservasRecyclerViewAdapter(reservas, mListener));
                                }

                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            anError.printStackTrace();
                        }
                    });*/
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(ReservaDetalle reservaDetalle);
    }
}
