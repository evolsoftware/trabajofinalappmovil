package com.tec.krea.mercatec.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tec.krea.mercatec.Entities.Conexion;
import com.tec.krea.mercatec.Entities.Cuenta;
import com.tec.krea.mercatec.Entities.Usuario;
import com.tec.krea.mercatec.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.IDN;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RegistroActivity extends AppCompatActivity {

    private EditText txtNombres;
    private EditText txtApellidos;
    private EditText txtCorreo;
    private EditText txtUsername;
    private EditText txtPassword;
    private EditText txtRepeatPassword;
    private CheckBox cbCuentaComprador;
    private CheckBox cbCuentaVendedor;
    private Button btnRegistrar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        final Conexion conexion=new Conexion();

        txtNombres=findViewById(R.id.txtNombres);
        txtApellidos=findViewById(R.id.txtApellidos);
        txtCorreo=findViewById(R.id.txtCorreo);
        txtUsername=findViewById(R.id.txtUsername);
        txtPassword=findViewById(R.id.txtRegistroContraseña);
        txtRepeatPassword=findViewById(R.id.txtRepetirContraseña);
        cbCuentaComprador=findViewById(R.id.cbComprador);
        cbCuentaVendedor=findViewById(R.id.cbVendedor);
        btnRegistrar=findViewById(R.id.btnRegistrar);

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  VALIDAR CONTRASEÑAS Y CHECKBOXES
                final Usuario objUsuario=new Usuario();
                objUsuario.Nombre=txtNombres.getText().toString();
                objUsuario.Apellidos=txtApellidos.getText().toString();
                objUsuario.Correo=txtCorreo.getText().toString();
                objUsuario.Username=txtUsername.getText().toString();
                objUsuario.Password=txtPassword.getText().toString();
                String repeatPassword=txtRepeatPassword.getText().toString();
                objUsuario.Fecha_registro=new Date();

                if (objUsuario.Nombre.isEmpty()
                        ||objUsuario.Apellidos.isEmpty()
                        ||objUsuario.Correo.isEmpty()
                        ||objUsuario.Username.isEmpty()
                        ||objUsuario.Password.isEmpty()
                        ||repeatPassword.isEmpty()
                        ||(!cbCuentaVendedor.isChecked()
                        &&!cbCuentaComprador.isChecked())
                        ){
                    Toast.makeText(getApplicationContext(),
                            "Por favor complete todos los campos",
                            Toast.LENGTH_SHORT).show();
                }else{
                    if (objUsuario.Password.equals(repeatPassword)){

                        AndroidNetworking.post(conexion.pathAPI+"/api/Usuario")
//                        AndroidNetworking.post("http://192.168.137.1/api/Usuario")
                                .addBodyParameter(objUsuario) // posting java object
                                .setTag("usuario")
                                .setPriority(Priority.MEDIUM)
                                .build()
                                .getAsJSONObject(new JSONObjectRequestListener() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        //  Crear las cuentas seleccionadas
                                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                                        String fecha_registro=sdf.format(objUsuario.Fecha_registro);

                                        AndroidNetworking.get(conexion.pathAPI+"/api/Usuario")
                                                .addQueryParameter("nombre", objUsuario.Nombre)
                                                .addQueryParameter("apellidos", objUsuario.Apellidos)
                                                .addQueryParameter("username", objUsuario.Username)
                                                .addQueryParameter("password", objUsuario.Password)
                                                .addQueryParameter("correo", objUsuario.Correo)
                                                .addQueryParameter("fecha_registro", fecha_registro)
                                                .setTag("login")
                                                .setPriority(Priority.LOW)
                                                .build()
                                                .getAsJSONObject(new JSONObjectRequestListener() {
                                                                     @Override
                                                                     public void onResponse(JSONObject response) {

                                                                         try {
                                                                             objUsuario.ID= response.getInt("ID");
                                                                             ArrayList<Cuenta> cuentas=new ArrayList<Cuenta>();

                                                                             if (cbCuentaVendedor.isChecked()){
                                                                                 Cuenta obj=new Cuenta();
                                                                                 obj.UsuarioID=objUsuario.ID;
                                                                                 obj.Tipo_cuenta="Vendedor";
                                                                                 cuentas.add(obj);
                                                                             }
                                                                             if (cbCuentaComprador.isChecked()){
                                                                                 Cuenta obj=new Cuenta();
                                                                                 obj.UsuarioID=objUsuario.ID;
                                                                                 obj.Tipo_cuenta="Comprador";
                                                                                 cuentas.add(obj);
                                                                             }
                                                                             for(int i=0;i<cuentas.size();i++){
                                                                                 AndroidNetworking.post(conexion.pathAPI+"/api/Cuenta")
                                                                                         .addBodyParameter(cuentas.get(i)) // posting java object
                                                                                         .setTag("cuenta")
                                                                                         .setPriority(Priority.MEDIUM)
                                                                                         .build()
                                                                                         .getAsJSONObject(new JSONObjectRequestListener() {
                                                                                             @Override
                                                                                             public void onResponse(JSONObject response) {
                                                                                                 // do anything with response
                                                                                                 Log.println(Log.ASSERT, "Success", "It worked!!!");
                                                                                             }

                                                                                             @Override
                                                                                             public void onError(ANError error) {
                                                                                                 // handle error
                                                                                                 Log.println(Log.ERROR, "Error",
                                                                                                         "Some error!!" + error.getErrorBody()
                                                                                                                 + " - " + error.getErrorDetail()
                                                                                                                 + " - " + error.getResponse());
                                                                                             }
                                                                                         });
                                                                             }
                                                                             Toast.makeText(getApplicationContext(),
                                                                                     "El usuario ha sido registrado",
                                                                                     Toast.LENGTH_LONG).show();

                                                                             Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                                                             startActivity(intent);
                                                                         } catch (JSONException e) {
                                                                             e.printStackTrace();
                                                                         }
                                                                     }
                                                                     @Override
                                                                     public void onError(ANError anError) {
                                                                         int codigoError = anError.getErrorCode();
                                                                         if(codigoError == 404) {
                                                                             Toast.makeText(getApplicationContext(),
                                                                                     "Username o contraseña inválido",
                                                                                     Toast.LENGTH_SHORT).show();
                                                                         }
                                                                         else {
                                                                             Toast.makeText(getApplicationContext(),
                                                                                     "Error interno. Error conexion.",
                                                                                     Toast.LENGTH_SHORT).show();
                                                                         }

                                                                     }
                                                });

                                        Log.println(Log.ASSERT, "Success", "It worked!!!");
                                    }

                                    @Override
                                    public void onError(ANError error) {
                                        // handle error
                                        Log.println(Log.ERROR, "Error",
                                                "Some error!!" + error.getErrorBody()
                                                        + " - " + error.getErrorDetail()
                                                        + " - " + error.getResponse());
                                    }
                                });
                    }else{
                        Toast.makeText(getApplicationContext(),
                                "Las contraseñas no coinciden",
                                Toast.LENGTH_SHORT).show();
                    }
                }

/*
                Toast.makeText(getApplicationContext(),
                        "Complete los campos",
                        Toast.LENGTH_SHORT).show();


                            */

            }
        });
    }
}
