package com.tec.krea.mercatec.Fragments.Comprador;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.bumptech.glide.Glide;
import com.tec.krea.mercatec.Entities.Conexion;
import com.tec.krea.mercatec.Entities.Grupo_Alimenticio;
import com.tec.krea.mercatec.Entities.Tipo_Producto;
import com.tec.krea.mercatec.Fragments.Comprador.GruposAlimenticiosFragment.OnListFragmentInteractionListener;
import com.tec.krea.mercatec.Fragments.Comprador.dummyGrupos.DummyContent.DummyItem;

import java.util.ArrayList;
import java.util.List;

import com.tec.krea.mercatec.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MisGruposAlimenticiosRecyclerViewAdapter extends RecyclerView.Adapter<MisGruposAlimenticiosRecyclerViewAdapter.ViewHolder> {

    private final List<Grupo_Alimenticio> mValues;
    private final OnListFragmentInteractionListener mListener;
    private Context context;

    public MisGruposAlimenticiosRecyclerViewAdapter(List<Grupo_Alimenticio> items, OnListFragmentInteractionListener listener, Context context) {
        mValues = items;
        mListener = listener;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_gruposalimenticios, parent, false);
        AndroidNetworking.initialize(context);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mNombre.setText(mValues.get(position).Nombre);
        final Conexion conexion=new Conexion();
        /*holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListGrupoAlimenticioFragmentInteraction(holder.mItem);
                }
            }
        });*/
        AndroidNetworking.get(conexion.pathAPI+"/api/tipoproductosporgrupoalimenticio")
                .addQueryParameter("id",String.valueOf(mValues.get(position).ID))
                .setTag("tiposproductos")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        ArrayList<Tipo_Producto> tipo_productos = new ArrayList<Tipo_Producto>();
                        try {
                            for(int a=0;a<response.length();a++) {
                                JSONObject jsonObject = response.getJSONObject(a);
                                Tipo_Producto objTipoProducto = new Tipo_Producto();
                                objTipoProducto.ID = jsonObject.getInt("ID");
                                objTipoProducto.Nombre = jsonObject.getString("Nombre");
                                objTipoProducto.ImagePath = jsonObject.getString("ImagePath");
                                objTipoProducto.Grupo_AlimenticioID = jsonObject.getInt("Grupo_AlimenticioID");
                                tipo_productos.add(objTipoProducto);
                            }
                            holder.mRecyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false));
                            holder.mRecyclerView.setAdapter(new MiTipoProductoRecyclerViewAdapter(tipo_productos,mListener,context));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mNombre;
        public Grupo_Alimenticio mItem;
        public RecyclerView mRecyclerView;
        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNombre = (TextView) view.findViewById(R.id.grupo_alimenticio_nombre);
            mRecyclerView = view.findViewById(R.id.lista_tipo_producto);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mNombre.getText() + "'";
        }
    }
}
