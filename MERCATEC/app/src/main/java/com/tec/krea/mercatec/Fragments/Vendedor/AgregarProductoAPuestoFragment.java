package com.tec.krea.mercatec.Fragments.Vendedor;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tec.krea.mercatec.Entities.Conexion;
import com.tec.krea.mercatec.Entities.Grupo_Alimenticio;
import com.tec.krea.mercatec.Entities.Producto;
import com.tec.krea.mercatec.Entities.Producto_x_Puesto;
import com.tec.krea.mercatec.Entities.Puesto;
import com.tec.krea.mercatec.Entities.Tipo_Producto;
import com.tec.krea.mercatec.R;
import com.tec.krea.mercatec.SpinnerAdapters.GruposAlimenticiosSpinnerAdapter;
import com.tec.krea.mercatec.SpinnerAdapters.ProductosSpinnerAdapter;
import com.tec.krea.mercatec.SpinnerAdapters.TipoProductoSpinnerAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AgregarProductoAPuestoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AgregarProductoAPuestoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AgregarProductoAPuestoFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public AgregarProductoAPuestoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AgregarProductoAPuestoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AgregarProductoAPuestoFragment newInstance(String param1, String param2) {
        AgregarProductoAPuestoFragment fragment = new AgregarProductoAPuestoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View myView =  inflater.inflate(R.layout.fragment_agregar_producto_a_puesto, container, false);

        mListener.onClickAgregarProductoAPuestoBackButton(true);

        final Conexion conexion=new Conexion();
        final int ID_Puesto = mListener.getMisPuestosFragmentInteraction().ID;

        AndroidNetworking.initialize(getActivity().getApplicationContext());

        final Spinner spnrGrupoAlimenticio = myView.findViewById(R.id.spnrGrupoAlimenticio);
        AndroidNetworking.get(conexion.pathAPI+"/api/grupoalimenticio")
                .setTag("grupoAlimenticio")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try{
                            ArrayList<Grupo_Alimenticio> gruposAlimenticios = new ArrayList<Grupo_Alimenticio>();
                            Grupo_Alimenticio defautValue = new Grupo_Alimenticio();
                            defautValue.ID = 0;
                            defautValue.Nombre = "Seleccione un grupo alimenticio...";
                            gruposAlimenticios.add(defautValue);

                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = response.getJSONObject(i);
                                Grupo_Alimenticio objGrupoAlienticio = new Grupo_Alimenticio();
                                objGrupoAlienticio.ID = jsonObject.getInt("ID");
                                objGrupoAlienticio.Nombre = jsonObject.getString("Nombre");
                                objGrupoAlienticio.ImagePath = jsonObject.getString("ImagePath");
                                gruposAlimenticios.add(objGrupoAlienticio);
                            }

                            GruposAlimenticiosSpinnerAdapter gruposAlimenticiosSpinnerAdapter = new GruposAlimenticiosSpinnerAdapter(myView.getContext(), android.R.layout.simple_spinner_item, gruposAlimenticios);

                            spnrGrupoAlimenticio.setAdapter(gruposAlimenticiosSpinnerAdapter);
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

        final Spinner spnrTiposProductos = myView.findViewById(R.id.spnrTipoProducto);
        spnrGrupoAlimenticio.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long grupoAlimenticioID) {
                AndroidNetworking.get(conexion.pathAPI+"/api/tipoproductosporgrupoalimenticio/{gaID}")
                        .addPathParameter("gaID", Long.toString(grupoAlimenticioID))
                        .setTag("tiposProductos")
                        .setPriority(Priority.LOW)
                        .build()
                        .getAsJSONArray(new JSONArrayRequestListener() {
                            @Override
                            public void onResponse(JSONArray response) {
                                try{
                                    ArrayList<Tipo_Producto> tiposProductos = new ArrayList<Tipo_Producto>();
                                    Tipo_Producto defaultValue = new Tipo_Producto();
                                    defaultValue.ID = 0;
                                    defaultValue.Nombre = "Seleccione un tipo de producto...";
                                    tiposProductos.add(defaultValue);

                                    for(int i = 0; i < response.length(); i++)
                                    {
                                        JSONObject jsonObject = response.getJSONObject(i);
                                        Tipo_Producto objTipoProducto = new Tipo_Producto();
                                        objTipoProducto.ID = jsonObject.getInt("ID");
                                        objTipoProducto.Nombre = jsonObject.getString("Nombre");
                                        objTipoProducto.ImagePath = jsonObject.getString("ImagePath");
                                        tiposProductos.add(objTipoProducto);
                                    }

                                    TipoProductoSpinnerAdapter tipoProductoSpinnerAdapter = new TipoProductoSpinnerAdapter(myView.getContext(), android.R.layout.simple_spinner_item, tiposProductos);

                                    spnrTiposProductos.setAdapter(tipoProductoSpinnerAdapter);
                                }
                                catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onError(ANError anError) {

                            }
                        });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        final Spinner spnrProductos = myView.findViewById(R.id.spnrProducto);
        spnrTiposProductos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long tipoProductoID) {
                AndroidNetworking.get(conexion.pathAPI+"/api/productosportipoproducto/{tpID}")
                        .addPathParameter("tpID", Long.toString(tipoProductoID))
                        .setTag("productos")
                        .setPriority(Priority.LOW)
                        .build()
                        .getAsJSONArray(new JSONArrayRequestListener() {
                            @Override
                            public void onResponse(JSONArray response) {
                                try {
                                    ArrayList<Producto> misProductos = new ArrayList<Producto>();
                                    Producto defaultValue = new Producto();
                                    defaultValue.ID = 0;
                                    defaultValue.Nombre = "Seleccione un producto...";
                                    misProductos.add(defaultValue);

                                    for (int i = 0; i < response.length(); i++) {
                                        JSONObject jsonObject = response.getJSONObject(i);
                                        Producto objProducto = new Producto();
                                        objProducto.ID = jsonObject.getInt("ID");
                                        objProducto.Nombre = jsonObject.getString("Nombre");
                                        objProducto.ImagePath = jsonObject.getString("ImagePath");
                                        misProductos.add(objProducto);
                                    }

                                    ProductosSpinnerAdapter productosSpinnerAdapter = new ProductosSpinnerAdapter(myView.getContext(), android.R.layout.simple_spinner_item, misProductos);

                                    spnrProductos.setAdapter(productosSpinnerAdapter);
                                }
                                catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onError(ANError anError) {

                            }
                        });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final EditText txtPrecio = myView.findViewById(R.id.txt_precio);
        Button btnRegistrar = myView.findViewById(R.id.btnRegistrarProductoEnPuesto);
        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int prodID = (int) spnrProductos.getSelectedItemId();
                String prec = txtPrecio.getText().toString();
                if(prodID != 0 && !prec.equals("")){
                    Producto_x_Puesto objProuctoPorPuesto = new Producto_x_Puesto();
                    objProuctoPorPuesto.Precio = Double.valueOf(txtPrecio.getText().toString());
                    objProuctoPorPuesto.ProductoID = (int) spnrProductos.getSelectedItemId();
                    objProuctoPorPuesto.PuestoID = ID_Puesto;
                    objProuctoPorPuesto.PuestoAcronimo = mListener.getMisPuestosFragmentInteraction().Acronimo;

                    AndroidNetworking.post(conexion.pathAPI+"/api/productoporpuesto")
                            .addBodyParameter(objProuctoPorPuesto)
                            .setTag("productoPorPuesto")
                            .setPriority(Priority.MEDIUM)
                            .build()
                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.println(Log.ASSERT, "Éxito", "Se registró correctamente el producto");
                                    mListener.GuardarProductoPorPuesto();
                                }

                                @Override
                                public void onError(ANError anError) {
                                    Log.println(Log.ERROR,"Error", "Ocurrió un error al registrar el producto " +
                                            anError.getErrorBody() + " - " + anError.getErrorDetail() + " - " + anError.getResponse());
                                }
                            });
                }
                else
                {
                    Toast.makeText(getContext(),"Por favor complete los campos", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return myView;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
        Puesto getMisPuestosFragmentInteraction();
        void GuardarProductoPorPuesto();
        void onClickAgregarProductoAPuestoBackButton(boolean enable);
    }
}
