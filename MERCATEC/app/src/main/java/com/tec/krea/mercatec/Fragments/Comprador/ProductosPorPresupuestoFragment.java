package com.tec.krea.mercatec.Fragments.Comprador;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tec.krea.mercatec.Entities.Conexion;
import com.tec.krea.mercatec.Entities.Presupuesto;
import com.tec.krea.mercatec.Entities.Presupuesto_x_Producto_x_Puesto;
import com.tec.krea.mercatec.Fragments.Comprador.TablesDynamic.ProductosTableDynamic;
import com.tec.krea.mercatec.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.security.ProtectionDomain;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProductosPorPresupuestoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProductosPorPresupuestoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductosPorPresupuestoFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private int contador=10;

    private OnFragmentInteractionListener mListener;

    public ProductosPorPresupuestoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProductosPorPresupuestoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProductosPorPresupuestoFragment newInstance(String param1, String param2) {
        ProductosPorPresupuestoFragment fragment = new ProductosPorPresupuestoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_productos_por_presupuesto, container, false);
        final Conexion conexion=new Conexion();
        mListener.backButtonProductosPorPresupuestoListener(false);

        final Presupuesto objPresupuesto = mListener.getPresupuesto();
        TextView txtNombre = view.findViewById(R.id.lblNombreDelPresupuesto);
        final TextView txtMontoTotal = view.findViewById(R.id.lblMontoTotal);
        txtNombre.setText(objPresupuesto.Nombre);
        if(objPresupuesto.Total == null){
            objPresupuesto.Total = 0.0;
        }
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        txtMontoTotal.setText("S/" + ((objPresupuesto.Total == 0.0)?"0.00":decimalFormat.format(objPresupuesto.Total)));

        final TableLayout tableLayout = view.findViewById(R.id.tableProductoPorPresupuesto);
        final ProductosTableDynamic productosTableDynamic = new ProductosTableDynamic(tableLayout, getContext());
        final String[] header = {"Producto","Total","Precio", "Cantidad", "Puesto"};
        final ArrayList<Presupuesto_x_Producto_x_Puesto> rows = new ArrayList();
        AndroidNetworking.get(conexion.pathAPI+"/api/productosporpresupuesto")
                .addQueryParameter("id", String.valueOf(objPresupuesto.ID))
                .setTag("productosporpresupuesto")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = response.getJSONObject(i);
                                Presupuesto_x_Producto_x_Puesto obj = new Presupuesto_x_Producto_x_Puesto();
                                obj.ID = jsonObject.getInt("ID");
                                obj.Producto_x_PuestoID = jsonObject.getInt("Producto_x_PuestoID");
                                obj.Cantidad = jsonObject.getDouble("Cantidad");
                                obj.Precio = jsonObject.getDouble("Precio");
                                obj.PuestoAcronimo = jsonObject.getString("PuestoAcronimo");
                                obj.NombreProducto = jsonObject.getString("NombreProducto");
                                obj.Total = obj.Precio * obj.Cantidad;
                                rows.add(obj);
                            }
                            productosTableDynamic.addHeader(header);
                            productosTableDynamic.addData(rows);
                            productosTableDynamic.backgroundHeader(Color.parseColor("#2196F3"));
                            productosTableDynamic.backgroundData(Color.parseColor("#BBDEFB"), Color.WHITE);
                            productosTableDynamic.lineColor(0);
                            productosTableDynamic.textColorData(Color.BLACK);
                            productosTableDynamic.textColorHeader(Color.BLACK);
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });

        productosTableDynamic.setProductosTableDynamicListener(new ProductosTableDynamic.ProductosTableDynamicListener() {
            @Override
            public void onClickRowListener(int index) {

            }

            @Override
            public void onClickDeleteItem(final int idButton, int idProducto) {
                AndroidNetworking.delete(conexion.pathAPI+"/api/presupuestoporproductoporpuesto")
                        .addQueryParameter("id",String.valueOf(idProducto))
                        .setTag("deletepresupuestoporproductoporpuesto")
                        .setPriority(Priority.LOW)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Double ActualizarTotal = rows.get(idButton - 1).Total;
                                objPresupuesto.Total -= ActualizarTotal;
                                DecimalFormat df = new DecimalFormat("#.00");
                                txtMontoTotal.setText("S/" + ((objPresupuesto.Total == 0.0)?"0.00":df.format(objPresupuesto.Total)));
                                productosTableDynamic.deleteItem(idButton);
                            }
                            @Override
                            public void onError(ANError anError) {

                            }
                        });
            }
        });
        FloatingActionButton btnAgregarProducto = view.findViewById(R.id.btnAgregarProductoAPresupuesto);
        btnAgregarProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.AgregarProductosPorPresupuestoListener(objPresupuesto);
            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
        Presupuesto getPresupuesto();
        void AgregarProductosPorPresupuestoListener(Presupuesto objPresupuesto);
        void backButtonProductosPorPresupuestoListener(boolean enable);
    }
}
