package com.tec.krea.mercatec.Fragments.Comprador;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.tec.krea.mercatec.Entities.Conexion;
import com.tec.krea.mercatec.Entities.ReservaDetalle;
import com.tec.krea.mercatec.Fragments.Vendedor.ReservaFragment;
import com.tec.krea.mercatec.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReservaDetalleComprador extends Fragment {


    public ReservaDetalleComprador() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view= inflater.inflate(R.layout.fragment_reserva_detalle_comprador, container, false);

        TextView estado=view.findViewById(R.id.txt_r_v_estado);
        TextView producto=view.findViewById(R.id.txt_r_v_producto);
        TextView fecha=view.findViewById(R.id.txt_r_v_fecha);
        TextView puesto=view.findViewById(R.id.txt_r_v_puesto);
        TextView volumen=view.findViewById(R.id.txt_r_v_volumen);
        TextView precio=view.findViewById(R.id.txt_r_v_precio);
        TextView ganancia=view.findViewById(R.id.txt_r_v_ganancia);
        Button btnEliminar=view.findViewById(R.id.btn_eliminar_reserva);

        ReservaDetalle reservaDetalle = null;
        if (getArguments()!=null){
            reservaDetalle=(ReservaDetalle)getArguments().get("reserva");
            Log.println(Log.INFO, "INFO", reservaDetalle.NombreProducto+" "+reservaDetalle.AcronimoPuesto
                    +" "+String.valueOf(reservaDetalle.Precio));

            producto.setText(reservaDetalle.NombreProducto);
            puesto.setText("Puesto: "+reservaDetalle.AcronimoPuesto);

            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date= null;
            try {
                date = sdf.parse(reservaDetalle.Fecha);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            String fecha_registro=String.valueOf(cal.get(Calendar.DAY_OF_MONTH)+
                    "/"+String.valueOf(cal.get(Calendar.MONTH)+1)+"/"+String.valueOf(cal.get(Calendar.YEAR)));
            fecha.setText("Fecha: "+fecha_registro);

            volumen.setText("Cantidad: "+String.valueOf(reservaDetalle.Cantidad));
            precio.setText("Precio: "+String.valueOf(reservaDetalle.Precio));
            ganancia.setText("Ganancia: S/."+String.valueOf(reservaDetalle.Ganancia));
            if (reservaDetalle.Estado==true){
                estado.setText("Estado: Reserva en espera...");
            }else{
                estado.setText("Estado: Reserva ya atendida");
            }
        }

        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(view.getContext(),
                        "No se puede eliminar la reserva.",
                        Toast.LENGTH_SHORT).show();
                FragmentManager fragmentManager =
                        getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction =
                        fragmentManager.beginTransaction();
                ReservaFragment reservasFragment = new ReservaFragment();
                fragmentTransaction.replace(R.id.fragment_layout_vendedor, reservasFragment);
                fragmentTransaction.commit();
            }
        });
        return view;
    }

}
