﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MercatecAPI.Models
{
    public class Presupuesto_x_Producto_x_PuestoApp
    {
        public int ID { get; set; }
        public int PresupuestoID { get; set; }
        public int Producto_x_PuestoID { get; set; }
        public Decimal Cantidad { get; set; }
        public Decimal Precio { get; set; }
        public String PuestoAcronimo { get; set; }
        public String NombreProducto { get; set; }
    }
}