﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MercatecAPI.Models
{
    public class PuestoApp
    {
        public int ID { get; set; }
        public int UsuarioID { get; set; }
        public String Acronimo { get; set; }
    }
}