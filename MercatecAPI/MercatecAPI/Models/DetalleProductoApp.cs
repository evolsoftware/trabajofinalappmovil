﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MercatecAPI.Models
{
    public class DetalleProductoApp
    {
        public int ID { get; set; }
        public String Nombre { get; set; }
        public String ImagePath { get; set; }
        public Decimal Precio { get; set; }
        public String Nombre_Tipo_Producto { get; set; }
        public String Nombre_Grupo_Alimenticio { get; set; }
    }
}