﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MercatecAPI.Models
{
    public class UsuarioApp
    {
        public int ID { get; set; }
        public String Nombre { get; set; }
        public String Apellidos { get; set; }
        public String Username { get; set; }
        public String Password { get; set; }
        public String Correo { get; set; }
        public DateTime Fecha_registro { get; set; }
    }
}