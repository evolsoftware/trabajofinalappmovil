﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MercatecAPI.Models
{
    public class Producto_x_PuestoApp
    {
        public int ID { get; set; }
        public int ProductoID { get; set; }
        public int PuestoID { get; set; }
        public String PuestoAcronimo { get; set; }
        public Decimal Precio { get; set; }
    }
}