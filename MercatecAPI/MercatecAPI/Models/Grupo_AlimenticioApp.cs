﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MercatecAPI.Models
{
    public class Grupo_AlimenticioApp
    {
        public int ID { get; set; }
        public String Nombre { get; set; }
        public String ImagePath { get; set; }
    }
}