﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MercatecAPI.Models
{
    public class Tipo_ProductoApp
    {
        public int ID { get; set; }
        public String Nombre { get; set; }
        public int Grupo_AlimenticioID { get; set; }
        public String ImagePath { get; set; }
    }
}