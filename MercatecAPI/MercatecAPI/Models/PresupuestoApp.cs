﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MercatecAPI.Models
{
    public class PresupuestoApp
    {
        public int ID { get; set; }
        public String Nombre { get; set; }
        public int UsuarioID { get; set; }
        public DateTime Fecha { get; set; }
        public bool Estado_uso { get; set; }
        public Decimal Total { get; set; }
    }
}