﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MercatecAPI.Models
{
    public class ReservaDetalleApp
    {
        public int ID { get; set; }
        public int Producto_x_PuestoID { get; set; }
        public int UsuarioID { get; set; }
        public Decimal Cantidad { get; set; }
        public DateTime Fecha { get; set; }
        public bool Estado { get; set; }

        public String PuestoAcronimo { get; set; }
        public String NombreProducto { get; set; }
        public Decimal Precio { get; set; }
        public Decimal Ganancia { get; set; }
    }
}