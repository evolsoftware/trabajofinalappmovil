﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using MercatecAPI.Models;

namespace MercatecAPI.Controllers
{
    public class ProductoController : ApiController
    {
        private MercatecEntitiesContext db = new MercatecEntitiesContext();

        // GET api/Producto
        public IEnumerable<ProductoApp> GetProductoes()
        {
            var query = from p in db.Productoes
                        orderby p.Nombre
                        select new ProductoApp
                        {
                            ID = p.ID,
                            Nombre = p.Nombre,
                            ImagePath = p.ImagePath,
                            Tipo_ProductoID = p.Tipo_ProductoID
                        };
            return query.ToList();
        }

        // GET api/Producto/5
        public ProductoApp GetProducto(int id)
        {
            var query = from p in db.Productoes
                        where p.ID == id
                        select new ProductoApp
                        {
                            ID = p.ID,
                            Nombre = p.Nombre,
                            ImagePath = p.ImagePath,
                            Tipo_ProductoID = p.Tipo_ProductoID
                        };
            return query.FirstOrDefault();
        }

        // PUT api/Producto/5
        public HttpResponseMessage PutProducto(int id, Producto producto)
        {
            if (ModelState.IsValid && id == producto.ID)
            {
                db.Entry(producto).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // POST api/Producto
        public HttpResponseMessage PostProducto(Producto producto)
        {
            if (ModelState.IsValid)
            {
                db.Productoes.Add(producto);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, producto);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = producto.ID }));
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // DELETE api/Producto/5
        public HttpResponseMessage DeleteProducto(int id)
        {
            Producto producto = db.Productoes.Find(id);
            if (producto == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Productoes.Remove(producto);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.OK, producto);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}