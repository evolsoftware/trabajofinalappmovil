﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using MercatecAPI.Models;

namespace MercatecAPI.Controllers
{
    public class PresupuestoPorProductoPorPuestoController : ApiController
    {
        private MercatecEntitiesContext db = new MercatecEntitiesContext();

        // GET api/PresupuestoPorProductoPorPuesto
        public IEnumerable<Presupuesto_x_Producto_x_PuestoApp> GetPresupuesto_x_Producto_x_Puesto()
        {
            var query = from ppp in db.Presupuesto_x_Producto_x_Puesto
                        select new Presupuesto_x_Producto_x_PuestoApp
                        {
                            ID = (int)ppp.ID,
                            PresupuestoID = (int)ppp.PresupuestoID,
                            Producto_x_PuestoID = (int)ppp.Producto_x_PuestoID,
                            Cantidad = (Decimal)ppp.Cantidad,
                            Precio = (Decimal)ppp.Producto_x_Puesto.Precio,
                            PuestoAcronimo = ppp.Producto_x_Puesto.Puesto.Acronimo,
                            NombreProducto = ppp.Producto_x_Puesto.Producto.Tipo_Producto.Nombre + " " + ppp.Producto_x_Puesto.Producto.Nombre
                        };
            return query.ToList();
        }

        // GET api/PresupuestoPorProductoPorPuesto/5
        public Presupuesto_x_Producto_x_PuestoApp GetPresupuesto_x_Producto_x_Puesto(int id)
        {
            var query = from ppp in db.Presupuesto_x_Producto_x_Puesto
                        where ppp.ID == id
                        select new Presupuesto_x_Producto_x_PuestoApp
                        {
                            ID = (int)ppp.ID,
                            PresupuestoID = (int)ppp.PresupuestoID,
                            Producto_x_PuestoID = (int)ppp.Producto_x_PuestoID,
                            Cantidad = (Decimal)ppp.Cantidad,
                            Precio = (Decimal)ppp.Producto_x_Puesto.Precio,
                            PuestoAcronimo = ppp.Producto_x_Puesto.Puesto.Acronimo,
                            NombreProducto = ppp.Producto_x_Puesto.Producto.Tipo_Producto.Nombre + " " + ppp.Producto_x_Puesto.Producto.Nombre
                        };
            return query.FirstOrDefault();
        }

        // PUT api/PresupuestoPorProductoPorPuesto/5
        public HttpResponseMessage PutPresupuesto_x_Producto_x_Puesto(int id, Presupuesto_x_Producto_x_Puesto presupuesto_x_producto_x_puesto)
        {
            if (ModelState.IsValid && id == presupuesto_x_producto_x_puesto.ID)
            {
                db.Entry(presupuesto_x_producto_x_puesto).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            
        }

        // POST api/PresupuestoPorProductoPorPuesto
        public HttpResponseMessage PostPresupuesto_x_Producto_x_Puesto(Presupuesto_x_Producto_x_Puesto presupuesto_x_producto_x_puesto)
        {
            if (ModelState.IsValid)
            {
                db.Presupuesto_x_Producto_x_Puesto.Add(presupuesto_x_producto_x_puesto);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, presupuesto_x_producto_x_puesto);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = presupuesto_x_producto_x_puesto.ID }));
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // DELETE api/PresupuestoPorProductoPorPuesto/5
        public HttpResponseMessage DeletePresupuesto_x_Producto_x_Puesto(int id)
        {
            Presupuesto_x_Producto_x_Puesto presupuesto_x_producto_x_puesto = db.Presupuesto_x_Producto_x_Puesto.Find(id);
            if (presupuesto_x_producto_x_puesto == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Presupuesto_x_Producto_x_Puesto.Remove(presupuesto_x_producto_x_puesto);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.OK, presupuesto_x_producto_x_puesto);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}