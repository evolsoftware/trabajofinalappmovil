﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using MercatecAPI.Models;

namespace MercatecAPI.Controllers
{
    public class ProductoPorPuestoController : ApiController
    {
        private MercatecEntitiesContext db = new MercatecEntitiesContext();

        // GET api/ProductoPorPuesto
        public IEnumerable<Producto_x_PuestoApp> GetProducto_x_Puesto()
        {
            var query = from pxp in db.Producto_x_Puesto
                         select new Producto_x_PuestoApp
                         {
                             ID = pxp.ID,
                             Precio = (Decimal)pxp.Precio,
                             ProductoID = (int)pxp.ProductoID,
                             PuestoAcronimo = pxp.Puesto.Acronimo,
                             PuestoID = (int)pxp.PuestoID
                         };
            return query.ToList();
        }

        // GET api/ProductoPorPuesto/5
        public Producto_x_PuestoApp GetProducto_x_Puesto(int id)
        {
            var query = from pxp in db.Producto_x_Puesto
                        where pxp.ID == id
                        select new Producto_x_PuestoApp
                        {
                            ID = pxp.ID,
                            Precio = (Decimal)pxp.Precio,
                            ProductoID = (int)pxp.ProductoID,
                            PuestoAcronimo = pxp.Puesto.Acronimo,
                            PuestoID = (int)pxp.PuestoID
                        };
            return query.FirstOrDefault();
        }

        // PUT api/ProductoPorPuesto/5
        public HttpResponseMessage PutProducto_x_Puesto(int id, Producto_x_Puesto producto_x_puesto)
        {
            if (ModelState.IsValid && id == producto_x_puesto.ID)
            {
                db.Entry(producto_x_puesto).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            
        }

        // POST api/ProductoPorPuesto
        public HttpResponseMessage PostProducto_x_Puesto(Producto_x_Puesto producto_x_puesto)
        {
            if (ModelState.IsValid)
            {
                db.Producto_x_Puesto.Add(producto_x_puesto);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, producto_x_puesto);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = producto_x_puesto.ID }));
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // DELETE api/ProductoPorPuesto/5
        public HttpResponseMessage DeleteProducto_x_Puesto(int id)
        {
            Producto_x_Puesto producto_x_puesto = db.Producto_x_Puesto.Find(id);
            if (producto_x_puesto == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Producto_x_Puesto.Remove(producto_x_puesto);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.OK, producto_x_puesto);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}