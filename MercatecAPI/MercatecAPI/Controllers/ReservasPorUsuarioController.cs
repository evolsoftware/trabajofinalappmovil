﻿using MercatecAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MercatecAPI.Controllers
{
    public class ReservasPorUsuarioController : ApiController
    {
        private MercatecEntitiesContext db = new MercatecEntitiesContext();

        // GET api/reservasporusuario
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/reservasporusuario/5
        public IEnumerable<ReservaApp> Get(int id)
        {
            var query = from r in db.Reservas
                        where r.UsuarioID == id && r.Estado==true
                        orderby r.Fecha descending
                        select new ReservaApp
                        {
                            ID = r.ID,
                            Cantidad = (Decimal)r.Cantidad,
                            Estado = (bool)r.Estado,
                            Fecha = (DateTime)r.Fecha,
                            UsuarioID = r.UsuarioID,
                            Producto_x_PuestoID = r.Producto_x_PuestoID
                        };
            return query.ToList();
        }

        // POST api/reservasporusuario
        public void Post([FromBody]string value)
        {
        }

        // PUT api/reservasporusuario/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/reservasporusuario/5
        public void Delete(int id)
        {
        }
    }
}
