﻿using MercatecAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MercatecAPI.Controllers
{
    public class LoginController : ApiController
    {
        private MercatecEntitiesContext db = new MercatecEntitiesContext();
        // GET api/login
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/login/5
        public UsuarioApp Get(string username, string password)
        {
            var query = from u in db.Usuarios
                        where u.Username == username &&
                        u.Password == password
                        select new UsuarioApp
                      {
                          ID=u.ID,
                          Nombre=u.Nombre,
                          Apellidos=u.Apellidos,
                          Username=u.Username,
                          Password=u.Password
                      };
            return query.FirstOrDefault();
        }

        // POST api/login
        public void Post([FromBody]string value)
        {
        }

        // PUT api/login/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/login/5
        public void Delete(int id)
        {
        }
    }
}
