﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using MercatecAPI.Models;

namespace MercatecAPI.Controllers
{
    public class CuentaController : ApiController
    {
        private MercatecEntitiesContext db = new MercatecEntitiesContext();

        // GET api/Cuenta
        public IEnumerable<CuentaApp> GetCuentas()
        {
            var query = from cs in db.Cuentas
                        select new CuentaApp
                        {
                            ID = cs.ID,
                            Tipo_cuenta = cs.Tipo_cuenta,
                            UsuarioID = (int)cs.UsuarioID
                        };
            return query.ToList();
        }

        // GET api/Cuenta/5
        public CuentaApp GetCuenta(int id)
        {
            var query = from cs in db.Cuentas
                        where cs.ID == id
                        select new CuentaApp
                        {
                            ID = cs.ID,
                            Tipo_cuenta = cs.Tipo_cuenta,
                            UsuarioID = (int)cs.UsuarioID
                        };
            return query.FirstOrDefault();
        }

        // PUT api/Cuenta/5
        public HttpResponseMessage PutCuenta(int id, Cuenta cuenta)
        {
            if (ModelState.IsValid && id == cuenta.ID)
            {
                db.Entry(cuenta).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            
        }

        // POST api/Cuenta
        public HttpResponseMessage PostCuenta(Cuenta cuenta)
        {
            if (ModelState.IsValid)
            {
                db.Cuentas.Add(cuenta);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, cuenta);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = cuenta.ID }));
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // DELETE api/Cuenta/5
        public HttpResponseMessage DeleteCuenta(int id)
        {
            Cuenta cuenta = db.Cuentas.Find(id);
            if (cuenta == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Cuentas.Remove(cuenta);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.OK, cuenta);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}