﻿using MercatecAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MercatecAPI.Controllers
{
    public class TipoProductosPorGrupoAlimenticioController : ApiController
    {
        private MercatecEntitiesContext db = new MercatecEntitiesContext();

        // GET api/tipoproductosporgrupoalimenticio
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/tipoproductosporgrupoalimenticio/5
        public IEnumerable<Tipo_ProductoApp> Get(int id)
        {
            var query = from tp in db.Tipo_Producto
                        where tp.Grupo_AlimenticioID == id
                        orderby tp.Nombre
                        select new Tipo_ProductoApp
                        {
                            ID = (int)tp.ID,
                            Grupo_AlimenticioID = (int)tp.Grupo_AlimenticioID,
                            Nombre = tp.Nombre,
                            ImagePath=tp.ImagePath
                        };
            return query.ToList();
        }

        // POST api/tipoproductosporgrupoalimenticio
        public void Post([FromBody]string value)
        {
        }

        // PUT api/tipoproductosporgrupoalimenticio/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/tipoproductosporgrupoalimenticio/5
        public void Delete(int id)
        {
        }
    }
}
