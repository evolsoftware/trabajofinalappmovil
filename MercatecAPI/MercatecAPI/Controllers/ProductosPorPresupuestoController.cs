﻿using MercatecAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MercatecAPI.Controllers
{
    public class ProductosPorPresupuestoController : ApiController
    {
        private MercatecEntitiesContext db = new MercatecEntitiesContext();
        // GET api/productosporpresupuesto
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/productosporpresupuesto/5
        public IEnumerable<Presupuesto_x_Producto_x_PuestoApp> Get(int id)
        {
            var query = from ppp in db.Presupuesto_x_Producto_x_Puesto
                        where ppp.PresupuestoID == id
                        select new Presupuesto_x_Producto_x_PuestoApp
                        {
                            ID = (int)ppp.ID,
                            PresupuestoID = (int)ppp.PresupuestoID,
                            Producto_x_PuestoID = (int)ppp.Producto_x_PuestoID,
                            Cantidad = (Decimal)ppp.Cantidad,
                            Precio = (Decimal)ppp.Producto_x_Puesto.Precio,
                            PuestoAcronimo = ppp.Producto_x_Puesto.Puesto.Acronimo,
                            NombreProducto = ppp.Producto_x_Puesto.Producto.Tipo_Producto.Nombre + " " + ppp.Producto_x_Puesto.Producto.Nombre
                        };
            return query.ToList();
        }

        // POST api/productosporpresupuesto
        public void Post([FromBody]string value)
        {

        }

        // PUT api/productosporpresupuesto/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/productosporpresupuesto/5
        public void Delete(int id)
        {
        }
    }
}
