﻿using MercatecAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MercatecAPI.Controllers
{
    public class PresupuestosPorUsuarioController : ApiController
    {
        private MercatecEntitiesContext db = new MercatecEntitiesContext();

        // GET api/presupuestosporusuario
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/presupuestosporusuario/5
        public IEnumerable<PresupuestoApp> Get(int id)
        {
            var query = from p in db.Presupuestoes
                        where p.UsuarioID == id
                        orderby p.Fecha descending
                        select new PresupuestoApp 
                        {
                            ID = p.ID,
                            Fecha = (DateTime)p.Fecha,
                            Estado_uso = (bool)p.Estado_uso,
                            Nombre = p.Nombre,
                            UsuarioID = (int)p.UsuarioID,
                            Total = 0
                        };
            List<PresupuestoApp> presupuestos = query.ToList();
            for(int a = 0; a < presupuestos.Count(); a++)
            {
                int idPresupuesto = presupuestos[a].ID;
                var s = from ppp in db.Presupuesto_x_Producto_x_Puesto
                        where ppp.PresupuestoID == idPresupuesto
                        select new Presupuesto_x_Producto_x_PuestoApp
                        {
                            Precio = (Decimal)ppp.Producto_x_Puesto.Precio,
                            Cantidad = (Decimal)ppp.Cantidad,
                        };
                List<Presupuesto_x_Producto_x_PuestoApp> l = s.ToList();
                var Total = l.Sum(x => x.Cantidad*x.Precio);
                presupuestos[a].Total = Total;
            }
            return presupuestos;
        }

        // POST api/presupuestosporusuario
        public void Post([FromBody]string value)
        {
        }

        // PUT api/presupuestosporusuario/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/presupuestosporusuario/5
        public void Delete(int id)
        {
        }
    }
}
