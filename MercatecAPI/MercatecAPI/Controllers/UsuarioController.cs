﻿using MercatecAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MercatecAPI.Controllers
{
    public class UsuarioController : ApiController
    {
        private MercatecEntitiesContext db = new MercatecEntitiesContext();

        // GET api/Usuario
        public IEnumerable<UsuarioApp> GetUsuarios()
        {
            var query = from u in db.Usuarios
                       select new UsuarioApp
                       {
                           ID = u.ID,
                           Nombre = u.Nombre,
                           Apellidos = u.Apellidos,
                           Username = u.Username,
                           Password = u.Password,
                           Correo = u.Correo,
                           Fecha_registro = u.Fecha_registro.Value

                       };
            return query.ToList();
        }
        //  POST api/Usuario
        public HttpResponseMessage PostUsuario(Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                db.Usuarios.Add(usuario);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, usuario);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = usuario.ID }));
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        // PUT api/Usuario/5
        public HttpResponseMessage PutUsuario(int id, Usuario usuario)
        {
            if (ModelState.IsValid && id == usuario.ID)
            {
                db.Entry(usuario).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
    
        //  DELETE api/Usuario/5
        public HttpResponseMessage DeleteUsuario(int id)
        {
            Usuario usuario = db.Usuarios.Find(id);
            if (usuario== null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Usuarios.Remove(usuario);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.OK, usuario);
        }
        //  GET api/Usuario/5
        public UsuarioApp GetUsuario(int id)
        {
            var query = from u in db.Usuarios
                        where u.ID == id
                        select new UsuarioApp
                        {
                            ID = u.ID,
                            Nombre = u.Nombre,
                            Apellidos = u.Apellidos,
                            Username = u.Username,
                            Password = u.Password,
                            Correo = u.Correo,
                            Fecha_registro = u.Fecha_registro.Value
                        };
           return query.FirstOrDefault();
        }
        public UsuarioApp GetUsuario(string username)
        {
            var query = from u in db.Usuarios
                        where u.Username==username
                        select new UsuarioApp
                        {
                            ID = u.ID,
                            Nombre = u.Nombre,
                            Apellidos = u.Apellidos,
                            Username = u.Username,
                            Password = u.Password,
                            Correo = u.Correo,
                            Fecha_registro = u.Fecha_registro.Value
                        };
            UsuarioApp usuario = query.FirstOrDefault();
            if (usuario == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            return usuario;
        }
        //  GET api/Usuario/
        public UsuarioApp GetUsuario(string nombre, string apellidos, string correo, string username, string password, DateTime fecha_registro)
        {
            var query = from u in db.Usuarios
                        where u.Nombre== nombre
                        && u.Apellidos== apellidos
                        && u.Correo== correo
                        && u.Username==username
                        && u.Password== password
                        && u.Fecha_registro== fecha_registro
                        select new UsuarioApp
                        {
                            ID = u.ID,
                            Nombre = u.Nombre,
                            Apellidos = u.Apellidos,
                            Username = u.Username,
                            Password = u.Password,
                            Correo = u.Correo,
                            Fecha_registro = u.Fecha_registro.Value
                        };
            UsuarioApp objUsuario = query.FirstOrDefault();
            if (objUsuario == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            return objUsuario;
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
