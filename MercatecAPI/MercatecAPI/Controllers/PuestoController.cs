﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using MercatecAPI.Models;

namespace MercatecAPI.Controllers
{
    public class PuestoController : ApiController
    {
        private MercatecEntitiesContext db = new MercatecEntitiesContext();

        // GET api/Puesto
        public IEnumerable<PuestoApp> GetPuestoes()
        {
            var query = from p in db.Puestoes
                        select new PuestoApp
                        {
                            ID = p.ID,
                            Acronimo = p.Acronimo,
                            UsuarioID = (int)p.UsuarioID
                        };
            return query.ToList();
        }

        // GET api/Puesto/5
        public PuestoApp GetPuesto(int id)
        {
            var query = from p in db.Puestoes
                        where p.ID == id
                        select new PuestoApp
                        {
                            ID = p.ID,
                            Acronimo = p.Acronimo,
                            UsuarioID = (int)p.UsuarioID
                        };
            return query.FirstOrDefault();
        }

        // PUT api/Puesto/5
        public HttpResponseMessage PutPuesto(int id, Puesto puesto)
        {
            if(ModelState.IsValid && id == puesto.ID)
            {
                db.Entry(puesto).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // POST api/Puesto
        public HttpResponseMessage PostPuesto(Puesto puesto)
        {
            if (ModelState.IsValid)
            {
                db.Puestoes.Add(puesto);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, puesto);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = puesto.ID }));
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // DELETE api/Puesto/5
        public HttpResponseMessage DeletePuesto(int id)
        {
            Puesto puesto = db.Puestoes.Find(id);
            if (puesto == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Puestoes.Remove(puesto);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.OK, puesto);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}