﻿using MercatecAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MercatecAPI.Controllers
{
    public class CuentasPorUsuarioController : ApiController
    {
        private MercatecEntitiesContext db = new MercatecEntitiesContext();

        // GET api/cuentasporusuario
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/cuentasporusuario/5
        public IEnumerable<CuentaApp> Get(int id)
        {
            var query = from cs in db.Cuentas
                        where cs.UsuarioID == id
                        select new CuentaApp
                        {
                            ID = (int)cs.ID,
                            Tipo_cuenta = cs.Tipo_cuenta,
                            UsuarioID = (int)cs.UsuarioID
                        };
            return query.ToList();
        }

        // POST api/cuentasporusuario
        public void Post([FromBody]string value)
        {
        }

        // PUT api/cuentasporusuario/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/cuentasporusuario/5
        public void Delete(int id)
        {
        }
    }
}
