﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using MercatecAPI.Models;

namespace MercatecAPI.Controllers
{
    public class PresupuestoController : ApiController
    {
        private MercatecEntitiesContext db = new MercatecEntitiesContext();

        // GET api/Presupuesto
        public IEnumerable<PresupuestoApp> GetPresupuestoes()
        {
            var query = from p in db.Presupuestoes
                        select new PresupuestoApp
                        {
                            ID = p.ID,
                            Nombre = p.Nombre,
                            Estado_uso = (bool)p.Estado_uso,
                            Fecha = (DateTime)p.Fecha,
                            UsuarioID = (int)p.UsuarioID
                        };
            return query.ToList();
        }

        // GET api/Presupuesto/5
        public PresupuestoApp GetPresupuesto(int id)
        {
            var query = from p in db.Presupuestoes
                        where p.ID == id
                        select new PresupuestoApp
                        {
                            ID = p.ID,
                            Nombre = p.Nombre,
                            Estado_uso = (bool)p.Estado_uso,
                            Fecha = (DateTime)p.Fecha,
                            UsuarioID = (int)p.UsuarioID
                        };
            return query.FirstOrDefault();
        }

        // PUT api/Presupuesto/5
        public HttpResponseMessage PutPresupuesto(int id, Presupuesto presupuesto)
        {
            if (ModelState.IsValid && id == presupuesto.ID)
            {
                db.Entry(presupuesto).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }

            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // POST api/Presupuesto
        public HttpResponseMessage PostPresupuesto(Presupuesto presupuesto)
        {
            if (ModelState.IsValid)
            {
                db.Presupuestoes.Add(presupuesto);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, presupuesto);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = presupuesto.ID }));
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // DELETE api/Presupuesto/5
        public HttpResponseMessage DeletePresupuesto(int id)
        {
            Presupuesto presupuesto = db.Presupuestoes.Find(id);
            if (presupuesto == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            //Eliminar campos de la tabla Presupuesto_x_Producto_Puesto
            var query = from ppp in db.Presupuesto_x_Producto_x_Puesto
                        where ppp.PresupuestoID == id
                        select ppp;
            List<Presupuesto_x_Producto_x_Puesto> lista = query.ToList();
            for(int a = 0; a < lista.Count; a++)
            {
                db.Presupuesto_x_Producto_x_Puesto.Remove(lista[a]);
            }
			//Eliminar el presupuesto
            db.Presupuestoes.Remove(presupuesto);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.OK, presupuesto);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}