﻿using MercatecAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MercatecAPI.Controllers
{
    public class PuestosPorUsuarioController : ApiController
    {
        private MercatecEntitiesContext db = new MercatecEntitiesContext();

        // GET api/puestosporusuario
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/puestosporusuario/5
        public IEnumerable<PuestoApp> Get(int id)
        {
            var query = from p in db.Puestoes
                        where p.UsuarioID == id
                        orderby p.Acronimo
                        select new PuestoApp
                        {
                            ID = p.ID,
                            Acronimo = p.Acronimo,
                            UsuarioID = (int)p.UsuarioID
                        };
            return query.ToList();
        }

        // POST api/puestosporusuario
        public void Post([FromBody]string value)
        {
        }

        // PUT api/puestosporusuario/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/puestosporusuario/5
        public void Delete(int id)
        {
        }
    }
}
