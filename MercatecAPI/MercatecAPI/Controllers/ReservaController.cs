﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using MercatecAPI.Models;

namespace MercatecAPI.Controllers
{
    public class ReservaController : ApiController
    {
        private MercatecEntitiesContext db = new MercatecEntitiesContext();

        // GET api/Reserva
        public IEnumerable<ReservaApp> GetReservas()
        {
            var query = from r in db.Reservas
                        select new ReservaApp
                        {
                            ID = r.ID,
                            Cantidad = (Decimal)r.Cantidad,
                            Estado = (bool)r.Estado,
                            Fecha = (DateTime)r.Fecha,
                            Producto_x_PuestoID = r.Producto_x_PuestoID,
                            UsuarioID = r.UsuarioID
                        };
            return query.ToList();
        }

        // GET api/Reserva/5
        public ReservaApp GetReserva(int id)
        {
            var query = from r in db.Reservas
                        where r.ID == id
                        select new ReservaApp
                        {
                            ID = r.ID,
                            Cantidad = (int)r.Cantidad,
                            Estado = (bool)r.Estado,
                            Fecha = (DateTime)r.Fecha,
                            Producto_x_PuestoID = r.Producto_x_PuestoID,
                            UsuarioID = r.UsuarioID
                        };
            return query.FirstOrDefault();
        }

        // PUT api/Reserva/5
        public HttpResponseMessage PutReserva(int id, Reserva reserva)
        {
            if (ModelState.IsValid && id == reserva.ID)
            {
                db.Entry(reserva).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            
        }

        // POST api/Reserva
        public HttpResponseMessage PostReserva(Reserva reserva)
        {
            if (ModelState.IsValid)
            {
                db.Reservas.Add(reserva);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, reserva);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = reserva.ID }));
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // DELETE api/Reserva/5
        public HttpResponseMessage DeleteReserva(int id)
        {
            Reserva reserva = db.Reservas.Find(id);
            if (reserva == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Reservas.Remove(reserva);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.OK, reserva);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}