﻿using MercatecAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MercatecAPI.Controllers
{
    public class ReservasPorCompradorController : ApiController
    {
        private MercatecEntitiesContext db = new MercatecEntitiesContext();
        // GET api/reservasporcomprador
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/reservasporcomprador/5
        public IEnumerable<ReservaDetalleApp> Get(int id)
        {
            var query = from r in db.Reservas
                        where r.UsuarioID == id
                        select new ReservaDetalleApp
                        {
                            ID = r.ID,
                            Producto_x_PuestoID = r.Producto_x_PuestoID,
                            UsuarioID = r.UsuarioID,
                            Cantidad = r.Cantidad.Value,
                            Fecha = r.Fecha.Value,
                            Estado = r.Estado.Value,
                            PuestoAcronimo = r.Producto_x_Puesto.Puesto.Acronimo,
                            NombreProducto = r.Producto_x_Puesto.Producto.Tipo_Producto.Grupo_Alimenticio.Nombre + " "
                            + r.Producto_x_Puesto.Producto.Tipo_Producto.Nombre + "  "
                            + r.Producto_x_Puesto.Producto.Nombre,
                            Precio = r.Producto_x_Puesto.Precio.Value,
                            Ganancia = r.Cantidad.Value * r.Producto_x_Puesto.Precio.Value
                        };
            
            return query.ToList();
        }

        // POST api/reservasporcomprador
        public void Post([FromBody]string value)
        {
        }

        // PUT api/reservasporcomprador/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/reservasporcomprador/5
        public void Delete(int id)
        {
        }
    }
}
