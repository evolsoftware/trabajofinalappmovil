﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using MercatecAPI.Models;

namespace MercatecAPI.Controllers
{
    public class TipoProductoController : ApiController
    {
        private MercatecEntitiesContext db = new MercatecEntitiesContext();

        // GET api/TipoProducto
        public IEnumerable<Tipo_ProductoApp> GetTipo_Producto()
        {
            var query = from tp in db.Tipo_Producto
                        orderby tp.Nombre
                        select new Tipo_ProductoApp
                        {
                            ID = tp.ID,
                            Grupo_AlimenticioID = tp.Grupo_AlimenticioID,
                            ImagePath = tp.ImagePath,
                            Nombre = tp.Nombre
                        };
            return query.ToList();
        }

        // GET api/TipoProducto/5
        public Tipo_ProductoApp GetTipo_Producto(int id)
        {
            var query = from tp in db.Tipo_Producto
                        where tp.ID == id
                       select new Tipo_ProductoApp
                       {
                           ID = tp.ID,
                           Grupo_AlimenticioID = tp.Grupo_AlimenticioID,
                           ImagePath = tp.ImagePath,
                           Nombre = tp.Nombre
                       };
            return query.FirstOrDefault();
        }

        // PUT api/TipoProducto/5
        public HttpResponseMessage PutTipo_Producto(int id, Tipo_Producto tipo_producto)
        {
            if (ModelState.IsValid && id == tipo_producto.ID)
            {
                db.Entry(tipo_producto).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // POST api/TipoProducto
        public HttpResponseMessage PostTipo_Producto(Tipo_Producto tipo_producto)
        {
            if (ModelState.IsValid)
            {
                db.Tipo_Producto.Add(tipo_producto);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, tipo_producto);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = tipo_producto.ID }));
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // DELETE api/TipoProducto/5
        public HttpResponseMessage DeleteTipo_Producto(int id)
        {
            Tipo_Producto tipo_producto = db.Tipo_Producto.Find(id);
            if (tipo_producto == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Tipo_Producto.Remove(tipo_producto);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.OK, tipo_producto);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}