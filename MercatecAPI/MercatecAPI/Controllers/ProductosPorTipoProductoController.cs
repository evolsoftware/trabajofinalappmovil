﻿using MercatecAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MercatecAPI.Controllers
{
    public class ProductosPorTipoProductoController : ApiController
    {
        private MercatecEntitiesContext db = new MercatecEntitiesContext();

        // GET api/productosportipoproducto
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/productosportipoproducto/5
        public IEnumerable<ProductoApp> Get(int id)
        {
            var query = from p in db.Productoes
                        where p.Tipo_ProductoID == id
                        orderby p.Nombre
                        select new ProductoApp
                        {
                            ID = (int)p.ID,
                            Tipo_ProductoID = (int)p.Tipo_ProductoID,
                            Nombre = p.Nombre,
                            ImagePath = p.ImagePath
                        };
            return query.ToList();
        }

        // POST api/productosportipoproducto
        public void Post([FromBody]string value)
        {
        }

        // PUT api/productosportipoproducto/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/productosportipoproducto/5
        public void Delete(int id)
        {
        }
    }
}
