﻿using MercatecAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MercatecAPI.Controllers
{
    public class ProductosPorPuestoPorUsuarioController : ApiController
    {
        private MercatecEntitiesContext db = new MercatecEntitiesContext();
        // GET api/productosporpuestoporusuario
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/productosporpuestoporusuario/5
        public IEnumerable<DetalleProductoApp> Get(int id)
        {
            var query = from p in db.Producto_x_Puesto
                        where p.PuestoID == id
                        orderby p.Producto.Nombre
                        select new DetalleProductoApp
                        {
                            ID = (int)p.ProductoID,
                            Nombre = p.Producto.Nombre,
                            ImagePath = p.Producto.ImagePath,
                            Precio = (Decimal)p.Precio,
                            Nombre_Tipo_Producto = p.Producto.Tipo_Producto.Nombre,
                            Nombre_Grupo_Alimenticio = p.Producto.Tipo_Producto.Grupo_Alimenticio.Nombre
                         };
            return query.ToList();
        }

        // POST api/productosporpuestoporusuario
        public void Post([FromBody]string value)
        {
        }

        // PUT api/productosporpuestoporusuario/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/productosporpuestoporusuario/5
        public void Delete(int id)
        {
        }
    }
}
