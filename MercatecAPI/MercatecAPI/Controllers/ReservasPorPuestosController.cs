﻿using MercatecAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MercatecAPI.Controllers
{
    public class ReservasPorPuestosController : ApiController
    {
        private MercatecEntitiesContext db = new MercatecEntitiesContext();

        // GET api/reservasporpuestos
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/reservasporpuestos/5
        public IEnumerable<ReservaApp> Get(int id)
        {
            var query = from r in db.Reservas
                        where r.Producto_x_Puesto.PuestoID == id
                        && r.Estado==true
                        select new ReservaApp
                        {
                            ID = (int)r.ID,
                            UsuarioID = (int)r.UsuarioID,
                            Producto_x_PuestoID = (int)r.Producto_x_PuestoID,
                            Cantidad = (Decimal)r.Cantidad,
                            Estado = (bool)r.Estado,
                            Fecha = (DateTime)r.Fecha
                        };
            return query.ToList();
        }

        // POST api/reservasporpuestos
        public void Post([FromBody]string value)
        {
        }

        // PUT api/reservasporpuestos/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/reservasporpuestos/5
        public void Delete(int id)
        {
        }
    }
}
