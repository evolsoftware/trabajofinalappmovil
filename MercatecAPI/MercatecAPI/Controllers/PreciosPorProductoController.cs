﻿using MercatecAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MercatecAPI.Controllers
{
    public class PreciosPorProductoController : ApiController
    {
        private MercatecEntitiesContext db = new MercatecEntitiesContext();
        // GET api/preciosporproducto
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/preciosporproducto/5
        public IEnumerable<Producto_x_PuestoApp> Get(int id)
        {
            var query = from pp in db.Producto_x_Puesto
                        where pp.ProductoID == id
                        select new Producto_x_PuestoApp
                        {
                            ID = (int)pp.ID,
                            ProductoID = (int)pp.ProductoID,
                            PuestoID=(int)pp.PuestoID,
                            PuestoAcronimo= pp.Puesto.Acronimo,
                            Precio = (Decimal)pp.Precio
                        };
            return query.ToList();
        }

        // POST api/preciosporproducto
        public void Post([FromBody]string value)
        {
        }

        // PUT api/preciosporproducto/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/preciosporproducto/5
        public void Delete(int id)
        {
        }
    }
}
