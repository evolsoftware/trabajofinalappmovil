﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using MercatecAPI.Models;

namespace MercatecAPI.Controllers
{
    public class GrupoAlimenticioController : ApiController
    {
        private MercatecEntitiesContext db = new MercatecEntitiesContext();

        // GET api/GrupoAlimenticio
        public IEnumerable<Grupo_AlimenticioApp> GetGrupo_Alimenticio()
        {
            var query = from ga in db.Grupo_Alimenticio
                        orderby ga.Nombre
                        select new Grupo_AlimenticioApp
                        {
                            ID = ga.ID,
                            Nombre = ga.Nombre,
                            ImagePath = ga.ImagePath
                        };
            return query.ToList();
        }

        // GET api/GrupoAlimenticio/5
        public Grupo_AlimenticioApp GetGrupo_Alimenticio(int id)
        {
            var query = from ga in db.Grupo_Alimenticio
                        where ga.ID == id
                        select new Grupo_AlimenticioApp                    
                        {
                            ID = ga.ID,
                            Nombre = ga.Nombre,
                            ImagePath = ga.ImagePath
                        };
            return query.FirstOrDefault();
        }

        // PUT api/GrupoAlimenticio/5
        public HttpResponseMessage PutGrupo_Alimenticio(int id, Grupo_Alimenticio grupo_alimenticio)
        {
            if (ModelState.IsValid && id == grupo_alimenticio.ID)
            {
                db.Entry(grupo_alimenticio).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // POST api/GrupoAlimenticio
        public HttpResponseMessage PostGrupo_Alimenticio(Grupo_Alimenticio grupo_alimenticio)
        {
            if (ModelState.IsValid)
            {
                db.Grupo_Alimenticio.Add(grupo_alimenticio);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, grupo_alimenticio);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = grupo_alimenticio.ID }));
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // DELETE api/GrupoAlimenticio/5
        public HttpResponseMessage DeleteGrupo_Alimenticio(int id)
        {
            Grupo_Alimenticio grupo_alimenticio = db.Grupo_Alimenticio.Find(id);
            if (grupo_alimenticio == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Grupo_Alimenticio.Remove(grupo_alimenticio);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.OK, grupo_alimenticio);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}