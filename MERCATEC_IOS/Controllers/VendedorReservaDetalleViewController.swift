//
//  VendedorReservaDetalleViewController.swift
//  Mercatec_IOS
//
//  Created by user146427 on 11/16/18.
//  Copyright © 2018 Alumnos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class VendedorReservaDetalleViewController: UIViewController {

    var objReserva:ReservaDetalle?=nil
    let dateFormatter=DateFormatter()
    @IBOutlet weak var txtNombreProducto: UILabel!
    @IBOutlet weak var txtPuesto: UILabel!
    @IBOutlet weak var txtFecha: UILabel!
    @IBOutlet weak var txtCantidad: UILabel!
    @IBOutlet weak var txtPrecio: UILabel!
    @IBOutlet weak var txtGanancia: UILabel!
    @IBOutlet weak var txtEstado: UILabel!
    let conexion=Conexion()
    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormatter.dateFormat="dd-MM-yyyy"
        // Do any additional setup after loading the view.
        if(self.objReserva != nil){
            txtNombreProducto.text=objReserva?.NombreProducto
            txtPuesto.text=objReserva?.AcronimoPuesto
            txtCantidad.text=String(objReserva!.Cantidad) + " uds."
            txtFecha.text=dateFormatter.string(from: objReserva!.Fecha)
            txtPrecio.text="S/. " + String(objReserva!.Precio)
            txtGanancia.text="S/. " + String(objReserva!.Ganancia)
            if(objReserva?.Estado == false){
                txtEstado.text="No Atendido"
            }
            else{
                txtEstado.text="Atendido"
            }
            print("Load data")
        }
        else{
            
            print("Clear data")
        }
    }
    
    @IBAction func btnAtenderReserva(_ sender: Any) {
        let obj=Reserva()
        obj.ID=(objReserva?.ID)!
        obj.Cantidad=objReserva!.Cantidad
        obj.Estado=true
        obj.Fecha=(objReserva?.Fecha)!
        obj.Producto_x_PuestoID=(objReserva?.Producto_x_PuestoID)!
        obj.UsuarioID=(objReserva?.UsuarioID)!
        dateFormatter.dateFormat="yyyy-MM-dd"
        let reservaP:Parameters=[
            "ID":obj.ID,
            "Producto_x_PuestoID":obj.Producto_x_PuestoID,
            "UsuarioID":obj.UsuarioID,
            "Cantidad":obj.Cantidad,
            "Fecha":dateFormatter.string(from: obj.Fecha),
            "Estado":obj.Estado
        ]
        Alamofire.request(conexion.pathAPI + "/api/Reserva/", method: .put, parameters: reservaP, encoding: JSONEncoding.default).response { response in
            print("Request: \(response.request)")
            print("Response: \(response.response)")
            print("Error: \(response.error)")
            
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)")
            }
            
            var message = ""
            var title = ""
            if(response.error == nil){
                title = "Exito"
                message = "Reservar atendidad!"
            }
            else{
                title = "Error"
                message = "Algo anda mal. Intentelo de nuevo!"
            }
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                NSLog("The \"OK\" alert occured.")
                
                if(title == "Exito"){
                    self.navigationController?.popViewController(animated: true)
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
