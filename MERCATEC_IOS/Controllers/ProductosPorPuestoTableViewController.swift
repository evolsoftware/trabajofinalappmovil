//
//  ProductosPorPuestoTableViewController.swift
//  Mercatec_IOS
//
//  Created by Alumnos on 11/13/18.
//  Copyright © 2018 Alumnos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ProductosPorPuestoTableViewController: UITableViewController {

    var misProductos: [DetalleProducto] = []
    var filaSeleccionada = 0
    var puestoID = 0
    var objPuesto : Puesto? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        puestoID = (self.objPuesto?.ID)!
        
        cargarProductosPorPuesto()
    }

    func cargarProductosPorPuesto(){
        Alamofire.request("http://vmdev1.nexolink.com:90/mercatecapi/api/productosporpuestoporusuario/"
            + String(puestoID)
            ).response{ response in
            print("Request: \(response.request)")
            print("Response: \(response.response)")
            print("Error: \(response.error)")
            
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)")
            }
                
            let productosJson = JSON(response.data)
                
            self.misProductos.removeAll()
            for(index, subJson):(String, JSON) in productosJson{
                let objDetalleProducto = DetalleProducto()
                objDetalleProducto.Nombre_Tipo_Producto = subJson["Nombre_Tipo_Producto"].stringValue
                objDetalleProducto.Nombre = subJson["Nombre"].stringValue
                objDetalleProducto.Precio = subJson["Precio"].doubleValue
                self.misProductos.append(objDetalleProducto)
            }
            
            self.tableView.reloadData()
        }
        print(puestoID)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return misProductos.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "productoPorPuestoCell", for: indexPath)

        // Configure the cell...
        cell.textLabel?.text = misProductos[indexPath.row].Nombre_Tipo_Producto + " " + misProductos[indexPath.row].Nombre
        cell.detailTextLabel?.text = "Precio: S/." + String(misProductos[indexPath.row].Precio)
        
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
