//
//  LoginViewController.swift
//  Mercatec_IOS
//
//  Created by Alumnos on 11/3/18.
//  Copyright © 2018 Alumnos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LoginViewController: UIViewController {

    @IBOutlet weak var txtUsuario: UITextField!
    @IBOutlet weak var swRecordar: UISwitch!
    @IBOutlet weak var txtContraseña: UITextField!
    let conexion=Conexion()
    override func viewDidLoad() {
        let usuario = UserDefaults.standard.string(forKey: "Username")
        let contrasenia = UserDefaults.standard.string(forKey: "Contrasenia")
        let recordar = UserDefaults.standard.bool(forKey: "Recordar")
        txtUsuario.text = usuario
        txtContraseña.text=contrasenia
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if(recordar==true && usuario != "" && contrasenia != ""){
            swRecordar.isOn=recordar
            btnIniciarSesion(self)
        }
    }
    
    @IBAction func btnIniciarSesion(_ sender: Any) {
        let username = self.txtUsuario.text
        let password = self.txtContraseña.text
        
        if ( username != "" && password != ""){
            
            Alamofire.request(conexion.pathAPI + "/api/login?username=" + username! + "&password=" + password!).responseJSON { response in
                print("Request: \(String(describing: response.request))")   // original url request
                print("Response: \(String(describing: response.response))") // http url response
                print("Result: \(response.result)")                         // response serialization result
                
                if let json = response.result.value as? NSNull{
                    
                    let alert = UIAlertController(title: "¡Error!", message: "Usuario o contraseña inválida", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                        NSLog("The \"OK\" alert occured.")
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                else{
                    let json = response.result.value
                    print("JSON: \(json)") // serialized json response
                    let userJson = JSON(json)
                    print(userJson)
                    let objUser=Usuario()
                    objUser.ID=userJson["ID"].intValue
                    objUser.Username=userJson["Username"].stringValue
                    print(objUser)
                    if(objUser.ID > 0 && objUser.Username != ""){
                        //Stored in phone
                        UserDefaults.standard.set(objUser.ID, forKey: "UsuarioID")
                        UserDefaults.standard.set(username, forKey: "Username")
                        if(self.swRecordar.isOn){
                            UserDefaults.standard.set(true, forKey: "Recordar")
                            UserDefaults.standard.set(password, forKey: "Contrasenia")
                        }else{
                            UserDefaults.standard.set(false,forKey: "recordar")
                            UserDefaults.standard.set("", forKey: "contrasenia")
                        }
                        
                        self.performSegue(withIdentifier: "cuentas", sender: self)
                    }else{
                        let alert = UIAlertController(title: "¡Error!", message: "Verfifique su conexión a internet.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                            NSLog("The \"OK\" alert occured.")
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    print("Data: \(utf8Text)") // original server data as UTF8 string
                }
            }
        }
        else{
            let alert = UIAlertController(title: "¡Error!", message: "Por favor complete todos los campos", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                NSLog("The \"OK\" alert occured.")
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    @IBAction func btnOlvidarContrasenia(_ sender: Any) {
        let alert = UIAlertController(title: "Mensaje", message: "Lo sentimos este servicio no esta disponible", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
