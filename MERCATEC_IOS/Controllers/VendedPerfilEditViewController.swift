//
//  VendedPerfilEditViewController.swift
//  Mercatec_IOS
//
//  Created by user146427 on 11/16/18.
//  Copyright © 2018 Alumnos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class VendedPerfilEditViewController: UIViewController {

    @IBOutlet weak var txtNombre: UITextField!
    @IBOutlet weak var txtApellidos: UITextField!
    @IBOutlet weak var txtContrasenia: UITextField!
    @IBOutlet weak var txtRepetirContrasenia: UITextField!
    @IBOutlet weak var txtCorreo: UITextField!
    var objUser=Usuario()
    let dateFormatter=DateFormatter()
    let conexion=Conexion()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dateFormatter.dateFormat="yyyy-MM-dd'T'HH:mm:ss"
        let userID = UserDefaults.standard.integer(forKey: "UsuarioID")
        // Do any additional setup after loading the view.
        Alamofire.request(conexion.pathAPI+"/api/Usuario?id=" + String(userID)).responseJSON { response in
            print("Request: \(String(describing: response.request))")   // original url request
            print("Response: \(String(describing: response.response))") // http url response
            print("Result: \(response.result)")                         // response serialization result
            
            
            if let json = response.result.value as? NSNull{
                
                let alert = UIAlertController(title: "¡Error!", message: "Usuario no no encontrado", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                    NSLog("The \"OK\" alert occured.")
                }))
                self.present(alert, animated: true, completion: nil)
            }
            else{
                let json = response.result.value
                print("JSON: \(json)") // serialized json response
                let userJson = JSON(json)
                self.objUser.ID=userID
                self.objUser.Apellidos=userJson["Apellidos"].stringValue
                self.objUser.Correo=userJson["Correo"].stringValue
                self.objUser.Nombre=userJson["Nombre"].stringValue
                self.objUser.Password=userJson["Password"].stringValue
                self.objUser.Username=userJson["Username"].stringValue
                self.objUser.FechaRegistro = self.dateFormatter.date(from: userJson["Fecha_registro"].stringValue)!

                self.txtRepetirContrasenia.text=userJson["Password"].stringValue
                self.txtContrasenia.text=userJson["Password"].stringValue
                self.txtNombre.text=userJson["Nombre"].stringValue
                self.txtApellidos.text=userJson["Apellidos"].stringValue
                self.txtCorreo.text=userJson["Correo"].stringValue
            }
            
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)") // original server data as UTF8 string
            }
            
        }
        
    }
    
    @IBAction func btnGuardarPerfil(_ sender: Any) {
        
        if((txtNombre.text?.isEmpty)! || (txtApellidos.text?.isEmpty)! || (txtContrasenia.text?.isEmpty)! || (txtRepetirContrasenia.text?.isEmpty)! || (txtCorreo.text?.isEmpty)!){
            let alert = UIAlertController(title: "Advertencia", message: "Complete todos los campos para continuar.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                NSLog("The \"OK\" alert occured.")
            }))
            self.present(alert, animated: true, completion: nil)         }else{
            let contrasenia=txtContrasenia.text
            let repetir_contrasenia=txtRepetirContrasenia.text
            if ((contrasenia?.elementsEqual(repetir_contrasenia!))!){
                dateFormatter.dateFormat="yyyy-MM-dd"
                let user:Parameters=[
                "ID":self.objUser.ID,
                "Nombre":self.objUser.Nombre,
                "Apellidos":self.objUser.Apellidos,
                "Fecha_registro":self.dateFormatter.string(from: self.objUser.FechaRegistro),
                "Correo":self.objUser.Correo,
                "Password":self.objUser.Password,
                "Username":self.objUser.Username
                ]
                let apiurl=self.conexion.pathAPI + "/api/Usuario"
                Alamofire.request(apiurl,
                                  method: .put,
                                  parameters: user,
                                  encoding: JSONEncoding.default)
                    .response { response in
                        print("Request: \(response.request)")
                        print("Response: \(response.response)")
                        print("Error: \(response.error)")
                        
                        if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                            print("Data: \(utf8Text)")
                        }
                        
                        var message = ""
                        var title = ""
                        if(response.error == nil){
                            title = "Exito"
                            message = "Usuario actualizado!"
                        }
                        else{
                            title = "Error"
                            message = "Ocurrio algo. Intentelo de nuevo!"
                        }
                        
                        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                            NSLog("The \"OK\" alert occured.")
                            
                            if(title == "Exito"){
                                self.navigationController?.popViewController(animated: true)
                            }
                        }))
                        self.present(alert, animated: true, completion: nil)
                }
                
            }else{
                let alert = UIAlertController(title: "Advertencia", message: "Las contraseñas no coinciden.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                    NSLog("The \"OK\" alert occured.")
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
