//
//  CompradorReservasTableViewController.swift
//  Mercatec_IOS
//
//  Created by user146427 on 11/22/18.
//  Copyright © 2018 Alumnos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class CompradorReservasTableViewController: UITableViewController {

    var reservas:[ReservaDetalle]=[]
    var selectedRow=0
    let dateformatter=DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self,
                                 action: #selector(self.loadReservas(_:)),
                                 for: .valueChanged)
        self.tableView.refreshControl = refreshControl
        self.tableView.addSubview(refreshControl)
        
        self.loadReservas(refreshControl)
    }

    // MARK: - Table view data source

    @objc func loadReservas(_ refreshControl: UIRefreshControl){
        let userID = UserDefaults.standard.integer(forKey: "UsuarioID")
        let conexion=Conexion()
        Alamofire.request(conexion.pathAPI + "/api/ReservasPorComprador/" + String(userID)).response { response in
            print("Request: \(response.request)")
            print("Response: \(response.response)")
            print("Error: \(response.error)")
            
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)")
            }
            
            let reservasJson = JSON(response.data)
            
            self.reservas.removeAll()
            for (index,subJson):(String, JSON) in reservasJson {
                // Do something you want
                let objRD = ReservaDetalle()
                objRD.ID=subJson["ID"].intValue
                objRD.AcronimoPuesto=subJson["PuestoAcronimo"].stringValue
                objRD.Cantidad=subJson["Cantidad"].doubleValue
                objRD.Estado=subJson["Estado"].boolValue
                objRD.Fecha=self.dateformatter.date(from: subJson["Fecha_reserva"].stringValue)!
                objRD.Ganancia=subJson["Ganancia"].doubleValue
                objRD.NombreProducto=subJson["NombreProducto"].stringValue
                objRD.Precio=subJson["Precio"].doubleValue
                objRD.Producto_x_PuestoID=subJson["Producto_x_PuestoID"].intValue
                objRD.UsuarioID=subJson["UsuarioID"].intValue
                self.reservas.append(objRD)
            }
            
            self.tableView.reloadData()
            refreshControl.endRefreshing()
        }
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return reservas.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reservaCell", for: indexPath)
        
        cell.textLabel?.text=reservas[indexPath.row].NombreProducto
        cell.detailTextLabel?.text="S/. " + String(reservas[indexPath.row].Precio)
        
        return cell
    }
    
    
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        self.selectedRow = indexPath.row
        print(self.selectedRow)
        self.performSegue(withIdentifier: "comprador_reserva_detalle", sender: self)
    }
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        if(segue.destination is CompradorReservaDetalleViewController){
            // Pass the selected object to the new view controller.
            let controller = segue.destination as? CompradorReservaDetalleViewController
            if(segue.identifier == "comprador_reserva_detalle"){
                controller?.objReserva = reservas[self.selectedRow]
            }
            else{
                controller?.objReserva = nil
            }
            print("Prepare Segue")
        }
    }

}
