            //
//  MisPresupuestosTableViewController.swift
//  Mercatec_IOS
//
//  Created by Alumnos on 11/23/18.
//  Copyright © 2018 Alumnos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
            
class MisPresupuestosTableViewController: UITableViewController {

    var misPresupuestos: [Presupuesto] = []
    var filaSeleccionada = 0
    
    let usuarioID = UserDefaults.standard.integer(forKey: "UsuarioID")
    let conexion=Conexion()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        cargarPresupuestos()
    }
    
    func cargarPresupuestos(){
        Alamofire.request(conexion.pathAPI+"/api/presupuestosporusuario/" +
            String(usuarioID)).response{ response in
                print("Request: \(response.request)")
                print("Response: \(response.response)")
                print("Error: \(response.error)")
                
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    print("Data: \(utf8Text)")
                }
                
                let presupuestosJson = JSON(response.data)
                
                self.misPresupuestos.removeAll()
                for(index, subJson):(String, JSON) in presupuestosJson{
                    let objPresupuesto = Presupuesto()
                    objPresupuesto.ID = subJson["ID"].intValue
                    objPresupuesto.Nombre = subJson["Nombre"].stringValue
                    objPresupuesto.Total = subJson["Total"].doubleValue
                    self.misPresupuestos.append(objPresupuesto)
                }
                self.tableView.reloadData()
        }
    }

    @IBAction func btnAgregarPresupuesto(_ sender: Any) {
        showInputDialog()
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return misPresupuestos.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "presupuestoCell", for: indexPath)

        // Configure the cell...
        cell.textLabel?.text = misPresupuestos[indexPath.row].Nombre
        cell.detailTextLabel?.text = String(misPresupuestos[indexPath.row].Total)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.filaSeleccionada = indexPath.row
        self.performSegue(withIdentifier: "detallesPresupuesto", sender: self)
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            
            self.misPresupuestos.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        if(segue.destination is ProductosPorPresupuestoTableViewController){
            // Pass the selected object to the new view controller.
            let controller = segue.destination as? ProductosPorPresupuestoTableViewController
            if(segue.identifier == "detallesPresupuesto"){
                controller?.objPresuspuesto = misPresupuestos[self.filaSeleccionada]
            }
            else{
                controller?.objPresuspuesto = nil
            }
        }
    }
    
    
    func showInputDialog() {
        //Creating UIAlertController and
        //Setting title and message for the alert dialog
        let alertController = UIAlertController(title: "Agregar presupuesto", message: "", preferredStyle: .alert)
        
        //the confirm action taking the inputs
        let confirmAction = UIAlertAction(title: "Guardar", style: .default) { (_) in
            //getting the input values from user
            let nombre = alertController.textFields?[0].text
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            
            if(nombre != ""){
                //Llamado API
                var objPresupuesto = Presupuesto()
                objPresupuesto.Nombre = nombre ?? ""
                objPresupuesto.UsuarioID = self.usuarioID
                let presupuestoParameters: Parameters = [
                    "UsuarioID": objPresupuesto.UsuarioID,
                    "Nombre": objPresupuesto.Nombre,
                    "Fecha": formatter.string(from: Date()),
                    "Estado_uso": true,
                    ]
                
                Alamofire.request("http://vmdev1.nexolink.com:90/mercatecapi/api/presupuesto",
                                  method: .post,
                                  parameters: presupuestoParameters,
                                  encoding: JSONEncoding.default)
                    .responseJSON{ response in
                        print("Request: \(response.request)")
                        print("Response: \(response.response)")
                        print("Error: \(response.error)")
                        
                        if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                            print("Data: \(utf8Text)")
                        }
                        
                        var message = ""
                        var title = ""
                        if(response.error == nil){
                            title = "Registro exitoso"
                            message = "Se ha registrado correctamente el presupuesto"
                            objPresupuesto.ID = JSON(response.result.value)["ID"].intValue
                            self.misPresupuestos.append(objPresupuesto)
                            self.tableView.reloadData()
                        }
                        else{
                            title = "Error"
                            message = "No se pudo registrar el presupuesto"
                        }
                        
                        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
                        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                            print("OK")
                        }
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                }
                
                
            }else{
                let alertController = UIAlertController(title: "Error", message: "Por favor complete el campo", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    self.showInputDialog()
                    print("OK")
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
        
        //the cancel action doing nothing
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel) { (_) in }
        
        //adding textfields to our dialog box
        alertController.addTextField { (textField) in
            textField.placeholder = "Nombre"
        }
        
        //adding the action to dialogbox
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        //finally presenting the dialog box
        self.present(alertController, animated: true, completion: nil)
        
    }

}
