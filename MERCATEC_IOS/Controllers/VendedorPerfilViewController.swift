//
//  VendedorPerfilViewController.swift
//  Mercatec_IOS
//
//  Created by user146427 on 11/16/18.
//  Copyright © 2018 Alumnos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class VendedorPerfilViewController: UIViewController {

    @IBOutlet weak var txtFechaRegistro: UILabel!
    @IBOutlet weak var txtNombres: UILabel!
    @IBOutlet weak var txtApellidos: UILabel!
    @IBOutlet weak var txtCorreo: UILabel!
    let dateFormatter=DateFormatter()
    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormatter.dateFormat="yyyy-MM-dd'T'HH:mm:sss"
        // Do any additional setup after loading the view.
        let conexion=Conexion()
        let userID = UserDefaults.standard.integer(forKey: "UsuarioID")
            Alamofire.request(conexion.pathAPI+"/api/Usuario?id=" + String(userID)).responseJSON { response in
                print("Request: \(String(describing: response.request))")   // original url request
                print("Response: \(String(describing: response.response))") // http url response
                print("Result: \(response.result)")                         // response serialization result
                
                
                if let json = response.result.value as? NSNull{
                    
                    let alert = UIAlertController(title: "¡Error!", message: "Usuario no no encontrado", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                        NSLog("The \"OK\" alert occured.")
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                else{
                    let json = response.result.value
                    print("JSON: \(json)") // serialized json response
                    let userJson = JSON(json)
                   
                    let  fecha=self.dateFormatter.date(from: userJson["Fecha_registro"].stringValue)!
                    self.dateFormatter.dateFormat="dd-MM-yyyy"
                    self.txtFechaRegistro.text=self.dateFormatter.string(from: fecha)
                    self.txtCorreo.text=userJson["Correo"].stringValue
                    self.txtNombres.text=userJson["Nombre"].stringValue
                    self.txtApellidos.text=userJson["Apellidos"].stringValue
                }
                
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    print("Data: \(utf8Text)") // original server data as UTF8 string
                }
                
            }
            
        
    }
    
    @IBAction func btnSalir(_ sender: Any) {
        UserDefaults.standard.set(false,forKey: "Recordar")
        UserDefaults.standard.set("", forKey: "Contrasenia")
        self.performSegue(withIdentifier: "a_login", sender: self)    }
    
    
    @IBAction func btnCambiarCuenta(_ sender: Any) {
        self.performSegue(withIdentifier: "a_cuentas", sender: self)
    }

    @IBAction func btnEliminarCuentaVendedor(_ sender: Any) {
        let alert = UIAlertController(title: "Mensaje", message: "Mil disculpas este servicio no esá disponible.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
