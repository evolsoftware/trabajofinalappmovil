//
//  CuentasViewController.swift
//  Mercatec_IOS
//
//  Created by user146427 on 11/23/18.
//  Copyright © 2018 Alumnos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class CuentasViewController: UIViewController {

    let conexion=Conexion()
    override func viewDidLoad() {
        let userID = UserDefaults.standard.integer(forKey: "UsuarioID")
        print(userID)
        Alamofire.request(conexion.pathAPI + "/api/CuentasPorUsuario/" + String(userID)).response { response in
            print("Request: \(response.request)")
            print("Response: \(response.response)")
            print("Error: \(response.error)")
            
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)")
            }
            
            let reservasJson = JSON(response.data)
            var mis_cuentas:[Cuenta]=[]
            for (index,subJson):(String, JSON) in reservasJson {
                // Do something you want
                var objCuenta=Cuenta()
                objCuenta.ID=subJson["ID"].intValue
                objCuenta.Tipo_cuenta=subJson["Tipo_cuenta"].stringValue
                objCuenta.UsuarioID=subJson["UsuarioID"].intValue
                mis_cuentas.append(objCuenta)
            }
            let n_cuentas=mis_cuentas.count
            print(mis_cuentas)
            if(n_cuentas>0){
                if (n_cuentas==1){
                    if((mis_cuentas.first?.Tipo_cuenta.contains("Comprador"))!){
                        self.performSegue(withIdentifier: "a_compradores", sender: self)
                    }else if((mis_cuentas.first?.Tipo_cuenta.contains("Vendedor"))!){
                        self.performSegue(withIdentifier: "a_vendedores", sender: self)
                    }
                }else{
                    
                }
            }else{
                self.performSegue(withIdentifier: "cuenta_a_login", sender: self)
            }
        }
        super.viewDidLoad()
    }
    
    @IBAction func btnSalir(_ sender: Any) {
        UserDefaults.standard.set("", forKey: "Contrasenia")
        UserDefaults.standard.set(false, forKey: "Recordar")
        self.performSegue(withIdentifier: "cuenta_a_login", sender: self)
    }
    
    @IBAction func btnAgregarCun(_ sender: Any) {
        let alert = UIAlertController(title: "Mensaje", message: "No se puede acceder a este servicio", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
