//
//  ProductosPorPresupuestoTableViewController.swift
//  Mercatec_IOS
//
//  Created by Alumnos on 11/24/18.
//  Copyright © 2018 Alumnos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ProductosPorPresupuestoTableViewController: UITableViewController {
    
    var misProductos: [Presupuesto_x_Producto_x_Puesto] = []
    var filaSeleccionada = 0
    var presupuestoID = 0
    var objPresuspuesto: Presupuesto? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        presupuestoID = (self.objPresuspuesto?.ID)!
        cargarProductosPorPresupuesto()
    }
    
    func cargarProductosPorPresupuesto(){
        let conexion=Conexion()
        Alamofire.request(conexion.pathAPI + "/api/productosporpresupuesto/" + String(presupuestoID)).response{ response in
            print("Request: \(response.request)")
            print("Response: \(response.response)")
            print("Error: \(response.error)")
            
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)")
            }
            
            let productosJson = JSON(response.data)
            self.misProductos.removeAll()
            for(index, subJson):(String, JSON) in productosJson{
                let objProductoXPresupuesto = Presupuesto_x_Producto_x_Puesto()
                objProductoXPresupuesto.ID = subJson["ID"].intValue
                objProductoXPresupuesto.Cantidad = subJson["Cantidad"].doubleValue
                objProductoXPresupuesto.Precio = subJson["Precio"].doubleValue
                objProductoXPresupuesto.Producto_x_PuestoID = subJson["Producto_x_PuestoID"].intValue
                objProductoXPresupuesto.Total = objProductoXPresupuesto.Precio * objProductoXPresupuesto.Cantidad
                objProductoXPresupuesto.NombreProducto = subJson["NombreProducto"].stringValue
                objProductoXPresupuesto.PuestoAcronimo = subJson["PuestoAcronimo"].stringValue
                
                self.misProductos.append(objProductoXPresupuesto)
            }
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return misProductos.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "productosPorPresupuestoCell", for: indexPath)

        // Configure the cell...
        cell.textLabel?.text = misProductos[indexPath.row].NombreProducto
        cell.detailTextLabel?.text = "Precio: S/." + String(misProductos[indexPath.row].Precio)
        return cell
    }

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
