//
//  RegistrarUsuarioViewController.swift
//  Mercatec_IOS
//
//  Created by user146427 on 11/15/18.
//  Copyright © 2018 Alumnos. All rights reserved.
//
import UIKit
import Alamofire
import SwiftyJSON
class RegistrarUsuarioViewController: UIViewController {

    @IBOutlet weak var txtNombres: UITextField!
    @IBOutlet weak var txtApellidos: UITextField!
    @IBOutlet weak var txtCorreo: UITextField!
    @IBOutlet weak var txtUsuario: UITextField!
    @IBOutlet weak var txtContrasenia: UITextField!
    @IBOutlet weak var txtRepetirContrasenia: UITextField!
    @IBOutlet weak var rbCComprador: UISwitch!
    @IBOutlet weak var rbCVendedor: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnRegistrar(_ sender: UIButton) {
        let c:String=txtContrasenia.text!
        let rc:String=txtRepetirContrasenia.text!
        if(c.isEmpty==false && rc.isEmpty==false){
            if(c.elementsEqual(rc)){
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                
                let objUsuario=Usuario()
                objUsuario.Nombre=txtNombres.text!
                objUsuario.Apellidos=txtApellidos.text!
                objUsuario.Username=txtUsuario.text!
                objUsuario.Password=txtContrasenia.text!
                objUsuario.Correo=txtCorreo.text!
                objUsuario.FechaRegistro=Date()
                
                let userParameters:Parameters=[
                    "Nombre":objUsuario.Nombre,
                    "Apellidos":objUsuario.Apellidos,
                    "Username":objUsuario.Username,
                    "Password":objUsuario.Password,
                    "Correo":objUsuario.Correo,
                    "FechaRegistro":formatter.string(from: objUsuario.FechaRegistro)
                    ]
                let conexion=Conexion()
                Alamofire.request(conexion.pathAPI+"/api/Usuario",
                                  method: .post,
                                  parameters: userParameters,
                                  encoding: JSONEncoding.default)
                    .response { response in
                        print("Request: \(response.request)")
                        print("Response: \(response.response)")
                        print("Error: \(response.error)")
                        
                        if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                            print("Data: \(utf8Text)")
                        }
                }
                Alamofire.request(conexion.pathAPI+"/api/Usuario?nombre=" + objUsuario.Nombre
                    + "&apellidos=" + objUsuario.Apellidos
                    + "&username=" + objUsuario.Username
                    + "&password=" + objUsuario.Password
                    + "&correo=" + objUsuario.Correo
                    + "&fecha_registro" + formatter.string(from: objUsuario.FechaRegistro)
                    ).responseJSON { response in
                    print("Request: \(String(describing: response.request))")   // original url request
                    print("Response: \(String(describing: response.response))") // http url response
                    print("Result: \(response.result)")                         // response serialization result
                    
                    if let json = response.result.value {
                        print("JSON: \(json)") // serialized json response
                        let userJson = JSON(json)
                        let id = userJson["ID"].intValue
                        objUsuario.ID=id
                        
                        if(id>0){
                            if(self.rbCComprador.isOn==true){
                                let comprador:Parameters=[
                                    "UsuarioID":id,
                                    "Tipo_cuenta":"Comprador"
                                ]
                                Alamofire.request(conexion.pathAPI+"/api/Cuenta",
                                                  method: .post,
                                                  parameters: comprador,
                                                  encoding: JSONEncoding.default)
                                    .response { response in
                                        print("Request: \(response.request)")
                                        print("Response: \(response.response)")
                                        print("Error: \(response.error)")
                                        
                                        if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                                            print("Data: \(utf8Text)")
                                        }
                                }
                            }
                            if(self.rbCVendedor.isOn==true){
                                let vendedor:Parameters=[
                                    "UsuarioID":id,
                                    "Tipo_cuenta":"Vendedor"
                                ]
                                Alamofire.request(conexion.pathAPI+"/api/Cuenta",
                                                  method: .post,
                                                  parameters: vendedor,
                                                  encoding: JSONEncoding.default)
                                    .response { response in
                                        print("Request: \(response.request)")
                                        print("Response: \(response.response)")
                                        print("Error: \(response.error)")
                                        
                                        if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                                            print("Data: \(utf8Text)")
                                        }
                                }
                            }
                            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                                print("Data: \(utf8Text)")
                            }
                            
                            let alert = UIAlertController(title: "Exito", message: "Usuario guardado.", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                                NSLog("The \"OK\" alert occured.")
                                self.navigationController?.popViewController(animated: true)
                            }))
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            //Stored in phone
                            UserDefaults.standard.set(id, forKey: "UsuarioID")
                            UserDefaults.standard.set(objUsuario.Username, forKey: "Username")
                            
                            self.performSegue(withIdentifier: "registro-cuenta", sender: self)
                        }
                    }
                    else{
                        let alert = UIAlertController(title: "Fail!", message: "No se puede registrar.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                            NSLog("The \"OK\" alert occured.")
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                        print("Data: \(utf8Text)") // original server data as UTF8 string
                    }
                }
            }
 
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
