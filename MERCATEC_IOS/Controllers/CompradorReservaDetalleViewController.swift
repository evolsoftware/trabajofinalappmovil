//
//  CompradorReservaDetalleViewController.swift
//  Mercatec_IOS
//
//  Created by user146427 on 11/22/18.
//  Copyright © 2018 Alumnos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class CompradorReservaDetalleViewController: UIViewController {

    var objReserva:ReservaDetalle?=nil
    let dateFormatter=DateFormatter()
    let conexion=Conexion()
    @IBOutlet weak var txtNombreProducto: UILabel!
    @IBOutlet weak var txtPuesto: UILabel!
    @IBOutlet weak var txtFecha: UILabel!
    @IBOutlet weak var txtCantidad: UILabel!
    @IBOutlet weak var txtPrecio: UILabel!
    @IBOutlet weak var txtGastoTotal: UILabel!
    @IBOutlet weak var txtEstado: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormatter.dateFormat="dd-MM-yyyy"
        // Do any additional setup after loading the view.
        if(self.objReserva != nil){
            txtNombreProducto.text=objReserva?.NombreProducto
            txtPuesto.text=objReserva?.AcronimoPuesto
            txtCantidad.text=String(objReserva!.Cantidad) + " uds."
            txtFecha.text=dateFormatter.string(from: objReserva!.Fecha)
            txtPrecio.text="S/. " + String(objReserva!.Precio)
            txtGastoTotal.text="S/. " + String(objReserva!.Ganancia)
            if(objReserva?.Estado == true){
                txtEstado.text="No Atendido"
            }
            else{
                txtEstado.text="Atendido"
            }
            print("Load data")
        }
        else{
            
            print("Clear data")
        }
    }
    

    @IBAction func btnEliminarReserva(_ sender: Any) {
        
        let urlapi:String=conexion.pathAPI + "/api/Reserva/" + String(objReserva!.ID)
        Alamofire.request(urlapi, method: .delete).responseJSON { response in
            guard response.result.error == nil else {
                // got an error in getting the data, need to handle it
                print("error calling DELETE on /todos/1")
                if let error = response.result.error {
                    print("Error: \(error)")
                }
                return
            }
            print("DELETE ok")
            
            let alert = UIAlertController(title: "Eliminado!", message: "Se ha eliminado su reserva", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                NSLog("The \"OK\" alert occured.")
            }))
            self.present(alert, animated: true, completion: nil)
            
            }
        self.performSegue(withIdentifier: "comprador_reserva_volver", sender: self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
