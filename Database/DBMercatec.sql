USE [master]
GO
/****** Object:  Database [Mercatec]    Script Date: 24/11/2018 03:06:40 p. m. ******/
CREATE DATABASE [Mercatec]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Mercatec', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Mercatec.mdf' , SIZE = 4288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Mercatec_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Mercatec_log.ldf' , SIZE = 1072KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Mercatec] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Mercatec].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Mercatec] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Mercatec] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Mercatec] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Mercatec] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Mercatec] SET ARITHABORT OFF 
GO
ALTER DATABASE [Mercatec] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Mercatec] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Mercatec] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Mercatec] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Mercatec] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Mercatec] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Mercatec] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Mercatec] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Mercatec] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Mercatec] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Mercatec] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Mercatec] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Mercatec] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Mercatec] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Mercatec] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Mercatec] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Mercatec] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Mercatec] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Mercatec] SET  MULTI_USER 
GO
ALTER DATABASE [Mercatec] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Mercatec] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Mercatec] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Mercatec] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Mercatec] SET DELAYED_DURABILITY = DISABLED 
GO
USE [Mercatec]
GO
/****** Object:  Table [dbo].[Cuenta]    Script Date: 24/11/2018 03:06:40 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cuenta](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UsuarioID] [int] NULL,
	[Tipo_cuenta] [varchar](50) NULL,
 CONSTRAINT [PK_Cuenta] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Grupo_Alimenticio]    Script Date: 24/11/2018 03:06:40 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Grupo_Alimenticio](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[ImagePath] [varchar](150) NULL,
 CONSTRAINT [PK_Grupo_Alimenticio] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Presupuesto]    Script Date: 24/11/2018 03:06:40 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Presupuesto](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](100) NULL,
	[UsuarioID] [int] NULL,
	[Fecha] [date] NULL,
	[Estado_uso] [bit] NULL,
 CONSTRAINT [PK_Presupuesto] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Presupuesto_x_Producto_x_Puesto]    Script Date: 24/11/2018 03:06:40 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Presupuesto_x_Producto_x_Puesto](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PresupuestoID] [int] NULL,
	[Producto_x_PuestoID] [int] NULL,
	[Cantidad] [decimal](5, 2) NULL,
 CONSTRAINT [PK_Presupuesto_x_Producto_x_Puesto] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Producto]    Script Date: 24/11/2018 03:06:40 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Producto](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Tipo_ProductoID] [int] NOT NULL,
	[ImagePath] [varchar](150) NULL,
 CONSTRAINT [PK_Variedad] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Producto_x_Puesto]    Script Date: 24/11/2018 03:06:40 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Producto_x_Puesto](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProductoID] [int] NULL,
	[PuestoID] [int] NULL,
	[Precio] [decimal](10, 2) NULL,
 CONSTRAINT [PK_Producto_x_Puesto] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Puesto]    Script Date: 24/11/2018 03:06:40 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Puesto](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UsuarioID] [int] NULL,
	[Acronimo] [varchar](50) NULL,
 CONSTRAINT [PK_Puesto] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Reserva]    Script Date: 24/11/2018 03:06:40 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reserva](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UsuarioID] [int] NOT NULL,
	[Producto_x_PuestoID] [int] NOT NULL,
	[Cantidad] [decimal](5, 2) NULL,
	[Fecha] [date] NULL,
	[Estado] [bit] NULL,
 CONSTRAINT [PK_Reserva] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tipo_Producto]    Script Date: 24/11/2018 03:06:40 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tipo_Producto](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NULL,
	[Grupo_AlimenticioID] [int] NOT NULL,
	[ImagePath] [varchar](150) NULL,
 CONSTRAINT [PK_Tipo_Producto] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 24/11/2018 03:06:40 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuario](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NULL,
	[Apellidos] [varchar](50) NULL,
	[Username] [varchar](50) NULL,
	[Password] [varchar](50) NULL,
	[Correo] [varchar](50) NULL,
	[Fecha_registro] [date] NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Cuenta] ON 

INSERT [dbo].[Cuenta] ([ID], [UsuarioID], [Tipo_cuenta]) VALUES (1, 1, N'Comprador')
INSERT [dbo].[Cuenta] ([ID], [UsuarioID], [Tipo_cuenta]) VALUES (2, 3, N'Vendedor')
INSERT [dbo].[Cuenta] ([ID], [UsuarioID], [Tipo_cuenta]) VALUES (3, 2, N'Comprador')
INSERT [dbo].[Cuenta] ([ID], [UsuarioID], [Tipo_cuenta]) VALUES (4, 2, N'Vendedor')
SET IDENTITY_INSERT [dbo].[Cuenta] OFF
SET IDENTITY_INSERT [dbo].[Grupo_Alimenticio] ON 

INSERT [dbo].[Grupo_Alimenticio] ([ID], [Nombre], [ImagePath]) VALUES (1, N'Tubérculos', NULL)
INSERT [dbo].[Grupo_Alimenticio] ([ID], [Nombre], [ImagePath]) VALUES (2, N'Frutas', NULL)
INSERT [dbo].[Grupo_Alimenticio] ([ID], [Nombre], [ImagePath]) VALUES (3, N'Verduras', NULL)
INSERT [dbo].[Grupo_Alimenticio] ([ID], [Nombre], [ImagePath]) VALUES (4, N'Bulbos', NULL)
SET IDENTITY_INSERT [dbo].[Grupo_Alimenticio] OFF
SET IDENTITY_INSERT [dbo].[Presupuesto] ON 

INSERT [dbo].[Presupuesto] ([ID], [Nombre], [UsuarioID], [Fecha], [Estado_uso]) VALUES (20, N'Presupuesto1', 2, CAST(N'2018-10-25' AS Date), 1)
INSERT [dbo].[Presupuesto] ([ID], [Nombre], [UsuarioID], [Fecha], [Estado_uso]) VALUES (43, N'presupuesto', 1, CAST(N'2018-10-28' AS Date), 1)
INSERT [dbo].[Presupuesto] ([ID], [Nombre], [UsuarioID], [Fecha], [Estado_uso]) VALUES (44, N'hik', 1, CAST(N'2018-10-28' AS Date), 1)
INSERT [dbo].[Presupuesto] ([ID], [Nombre], [UsuarioID], [Fecha], [Estado_uso]) VALUES (45, N'hjkkbs', 1, CAST(N'2018-10-28' AS Date), 1)
INSERT [dbo].[Presupuesto] ([ID], [Nombre], [UsuarioID], [Fecha], [Estado_uso]) VALUES (46, N'fjj', 1, CAST(N'2018-10-30' AS Date), 1)
INSERT [dbo].[Presupuesto] ([ID], [Nombre], [UsuarioID], [Fecha], [Estado_uso]) VALUES (47, N'hjk', 2, CAST(N'2018-10-31' AS Date), 1)
SET IDENTITY_INSERT [dbo].[Presupuesto] OFF
SET IDENTITY_INSERT [dbo].[Presupuesto_x_Producto_x_Puesto] ON 

INSERT [dbo].[Presupuesto_x_Producto_x_Puesto] ([ID], [PresupuestoID], [Producto_x_PuestoID], [Cantidad]) VALUES (14, 43, 14, CAST(5.00 AS Decimal(5, 2)))
INSERT [dbo].[Presupuesto_x_Producto_x_Puesto] ([ID], [PresupuestoID], [Producto_x_PuestoID], [Cantidad]) VALUES (16, 43, 15, CAST(8.00 AS Decimal(5, 2)))
INSERT [dbo].[Presupuesto_x_Producto_x_Puesto] ([ID], [PresupuestoID], [Producto_x_PuestoID], [Cantidad]) VALUES (21, 44, 1, CAST(5.00 AS Decimal(5, 2)))
INSERT [dbo].[Presupuesto_x_Producto_x_Puesto] ([ID], [PresupuestoID], [Producto_x_PuestoID], [Cantidad]) VALUES (22, 44, 3, CAST(1.00 AS Decimal(5, 2)))
INSERT [dbo].[Presupuesto_x_Producto_x_Puesto] ([ID], [PresupuestoID], [Producto_x_PuestoID], [Cantidad]) VALUES (23, 43, 1, CAST(5.00 AS Decimal(5, 2)))
INSERT [dbo].[Presupuesto_x_Producto_x_Puesto] ([ID], [PresupuestoID], [Producto_x_PuestoID], [Cantidad]) VALUES (24, 47, 21, CAST(18.00 AS Decimal(5, 2)))
SET IDENTITY_INSERT [dbo].[Presupuesto_x_Producto_x_Puesto] OFF
SET IDENTITY_INSERT [dbo].[Producto] ON 

INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (1, N'Amarilla', 1, N'http://www.costalverde.com.pe/wp-content/uploads/2017/09/papa-amarilla.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (2, N'Blanca', 1, N'http://www.mercadothefrutas.com/21-large_default/papa-blanca-lavada.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (3, N'Huayro', 1, N'https://c1.staticflickr.com/5/4111/5054600908_a259f640a1_b.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (4, N'Huamantanga', 1, N'https://c1.staticflickr.com/5/4151/5054789913_fd81ca713f_b.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (5, N'Negra Andina', 1, N'http://3.bp.blogspot.com/-Yu26ntbYePE/T7_tXMw8TGI/AAAAAAAAEL0/ZfjPLjyuFm4/s320/Papa+Negra.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (6, N'Peruanita', 1, N'https://www.ecofertas.com/content/images/thumbs/0000997_papa-peruanita_550.png')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (7, N'Yungay', 1, N'http://www.costalverde.com.pe/wp-content/uploads/2017/09/papa-yungay-.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (8, N'Canchan', 1, N'http://www.costalverde.com.pe/wp-content/uploads/2017/09/papa-canchan.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (9, N'Amarilla', 2, N'https://unabiologaenlacocina.files.wordpress.com/2014/02/dev2402_3.jpg?w=584')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (10, N'Blanca', 2, N'http://ve.emedemujer.com/wp-content/uploads/sites/2/2017/02/yuca-amarga.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (11, N'Largo', 4, N'http://www.generaccion.com/secciones/biodiversidad/imagenes/grandes/277.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (12, N'Redondo', 4, N'https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Ulluco.jpg/220px-Ulluco.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (13, N'Amarilla', 5, N'https://cdne.diariocorreo.pe/thumbs/uploads/articles/images/conoce-las-multiples-propiedad-png_604x0.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (14, N'Roja', 5, N'https://cdne.diariocorreo.pe/thumbs/uploads/articles/images/conoce-las-multiples-propiedad-png_604x0.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (15, N'Amarillo', 6, N'https://swissbrothers.com/rshop/775-large_default/camote-amarillo-fresco-400gr.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (16, N'Morado', 6, N'http://www.savitari.com/wp-content/uploads/2011/09/camote-morado.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (17, N'Blanco', 6, N'http://3.bp.blogspot.com/-_Q6X5FzsQVM/TlWGqe1sgpI/AAAAAAAAA8Y/I4I8kQggQEw/s1600/camote+blanco.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (18, N'Criolla', 7, N'https://cdne.diariocorreo.pe/thumbs/uploads/articles/images/tuberculos-como-camote-y-zanahoria-se-venden-23517-jpg_604x0.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (19, N'Serrana', 7, N'http://www.campusagricola.com/wp-content/uploads/2018/08/zanahorias.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (20, N'Cabeza Blanca Nacional', 8, N'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTXxFw_dmbNoWHJ_P1_mXziHpvk5gmKm_VS2bVS7rseYF7BoJ1S')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (21, N'Cabeza Roja', 8, N'https://www.comenaranjas.com/images/stories/virtuemart/product/cebolla-roja.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (22, N'China', 8, N'https://i2.wp.com/www.lashierbasderosa.com/wp-content/uploads/2018/03/cebollachina-660x500.jpg?fit=660%2C500')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (23, N'Italiana', 8, N'https://http2.mlstatic.com/30-semillas-cebolla-italiana-tropea-lunga-torpedo-tropeana-D_NQ_NP_454215-MLA25140701434_112016-F.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (24, N'Morado', 9, N'http://cadenaser00.epimg.net/emisora/imagenes/2018/08/30/radio_azul/1535626518_098200_1535626590_noticia_normal.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (25, N'Serrano', 9, N'https://spcdn.co/wp-content/uploads/2018/04/beneficios-del-ajo.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (26, N'Chino', 9, N'https://static.vix.com/es/sites/default/files/styles/large/public/imj/otramedicina/P/Propiedades-del-ajo-chino-1.jpg?itok=snL8VhwL')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (27, N'Criollo', 10, N'https://www.enbuenasmanos.com/wp-content/uploads/bfi_thumb/7008155-354ds29ufpp1hy8oiixe6i.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (28, N'Serrano', 10, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (29, N'Roja', 12, N'http://www.elhuertodelabuelo.es/14/fresa-roja.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (30, N'Huaral', 12, N'https://www.tour-gastronomico.com/multimanager/public/imagen/noticia/gestiona/JW95PW381N4783671G18.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (31, N'Isla', 13, N'http://agromania.pe/wp-content/uploads/2010/12/platano.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (32, N'Seda', 13, N'https://vivanda.vteximg.com.br/arquivos/ids/167121-1000-1000/772631.jpg?v=636137838159130000')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (33, N'Borgoña Negra', 14, N'http://vinodemesa.blogdiario.com/media/cache/resolve/media/files/01/558/529/2017/12/maduraci-in-de-la-uva.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (34, N'Italia', 14, N'https://www.campusdelvino.com/media/k2/items/cache/39a27618f1dc54b80987c6706135e6b7_XL.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (35, N'Azul', 15, N'https://http2.mlstatic.com/mango-kent-limonero-4-estaciones-mandarina-criolla-cerezo-D_NQ_NP_771098-MLA25684854657_062017-F.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (36, N'Ciruelo', 15, N'http://bp1.blogger.com/_eGV0s4nYLsM/RqEDSEUCDqI/AAAAAAAAAqo/wiXX11jbYCM/w1200-h630-p-k-no-nu/mangosciruelos.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (37, N'Papaya', 15, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (38, N'Mexicano', 15, N'https://dmplast.mx/wp-content/uploads/mango-ataulfo2.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (39, N'Israel', 16, N'https://vivanda.vteximg.com.br/arquivos/ids/165119-1000-1000/63616.jpg?v=636137791197300000')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (40, N'Chilena Roja', 16, N'http://agraria.pe/uploads/images/2017/05/MANZANA.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (41, N'Chilena Verde', 16, N'http://www.disgralec.com/384-large_default/manzana-chilena-verde-5-u.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (42, N'Winter', 16, N'http://www.waldhus.com/upload/c/17/c17e71ff1656728c3cd292a519edc724.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (43, N'Golden', 16, N'http://www.fruteriaelvergel.com/WebRoot/StoreES2/Shops/64945569/57C0/6872/EDE7/B6FC/8FBA/C0A8/2BBA/A1FF/manzana_golden_extra.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (44, N'Costa', 17, N'http://www.innovacion.cr/sites/default/files/styles/large/public/Maracuy%25C3%25A1_stand.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (45, N'Selva', 17, N'https://portal.andina.pe/EDPFotografia3/thumbnail/2018/01/25/000478338M.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (46, N'Huarochiri', 18, N'https://sportadictos.com/files/2016/09/Razones-comer-tuna.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (47, N'Huancayo', 18, N'https://www.elesquiu.com/u/fotografias/m/2011/5/7/f960x0-22275_22293_0.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (48, N'Criolla', 19, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (49, N'Morocha', 19, N'https://img.elcomercio.pe/files/article_content_ec_fotos/uploads/2017/03/21/58d1b4d0b14bd.jpeg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (50, N'Huando', 19, N'https://i1.wp.com/huaralenlinea.com/wp-content/uploads/2014/06/naranja-huando-huaral-en-linea-jpg.jpg?resize=450%2C340')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (51, N'Hawayana', 20, N'https://i.pinimg.com/originals/68/da/86/68da862197bd1675f3a052331aaac606.jpg')
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (52, N'Golden', 20, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (53, N'Criolla Selva', 21, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (54, N'Fuerte Selva', 21, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (55, N'Villacampa', 21, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (56, N'Naval', 21, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (57, N'Criolla', 22, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (58, N'Selva', 22, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (59, N'Verde Serrana', 24, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (60, N'Seca Corriente', 24, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (61, N'Seca Importada', 24, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (62, N'Criolla', 27, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (63, N'Serrana', 27, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (64, N'Verde Americana', 29, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (65, N'Verde Azul', 29, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (66, N'Verde Blanco Criolla', 29, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (67, N'Verde Blanca Serrana', 29, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (68, N'Criolla', 30, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (69, N'Serrana', 30, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (70, N'Criolla', 31, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (71, N'Serrana', 31, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (72, N'Americana', 32, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (73, N'Criolla Seda', 32, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (74, N'Serrana Seda', 32, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (75, N'Romana', 32, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (76, N'Durazno', 11, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (77, N'Mandarina', 23, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (78, N'Acelga', 25, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (79, N'Albahaca', 26, NULL)
INSERT [dbo].[Producto] ([ID], [Nombre], [Tipo_ProductoID], [ImagePath]) VALUES (80, N'Apio', 28, NULL)
SET IDENTITY_INSERT [dbo].[Producto] OFF
SET IDENTITY_INSERT [dbo].[Producto_x_Puesto] ON 

INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (1, 1, 2, CAST(2.30 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (2, 1, 1, CAST(2.40 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (3, 2, 1, CAST(2.30 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (4, 3, 2, CAST(2.70 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (5, 3, 4, CAST(2.80 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (6, 3, 5, CAST(2.80 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (7, 3, 1, CAST(2.80 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (8, 4, 5, CAST(2.80 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (9, 4, 1, CAST(2.90 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (10, 5, 2, CAST(3.40 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (11, 5, 5, CAST(3.90 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (12, 6, 4, CAST(3.30 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (13, 6, 1, CAST(3.30 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (14, 6, 2, CAST(3.40 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (15, 8, 1, CAST(3.50 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (16, 7, 4, CAST(3.40 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (17, 9, 4, CAST(2.60 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (18, 9, 2, CAST(2.70 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (19, 10, 4, CAST(3.00 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (20, 10, 1, CAST(3.40 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (21, 15, 1, CAST(3.80 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (22, 15, 4, CAST(4.00 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (23, 16, 5, CAST(4.20 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (24, 16, 2, CAST(4.20 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (25, 18, 1, CAST(2.20 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (26, 18, 4, CAST(2.00 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (27, 19, 4, CAST(2.50 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (28, 19, 1, CAST(2.50 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (29, 10, 6, CAST(2.00 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (30, 11, 5, CAST(2.50 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (31, 76, 7, CAST(1.50 AS Decimal(10, 2)))
INSERT [dbo].[Producto_x_Puesto] ([ID], [ProductoID], [PuestoID], [Precio]) VALUES (32, 19, 1, CAST(3.00 AS Decimal(10, 2)))
SET IDENTITY_INSERT [dbo].[Producto_x_Puesto] OFF
SET IDENTITY_INSERT [dbo].[Puesto] ON 

INSERT [dbo].[Puesto] ([ID], [UsuarioID], [Acronimo]) VALUES (1, 3, N'A-001')
INSERT [dbo].[Puesto] ([ID], [UsuarioID], [Acronimo]) VALUES (2, 3, N'A-002')
INSERT [dbo].[Puesto] ([ID], [UsuarioID], [Acronimo]) VALUES (3, 3, N'A-003')
INSERT [dbo].[Puesto] ([ID], [UsuarioID], [Acronimo]) VALUES (4, 2, N'B-001')
INSERT [dbo].[Puesto] ([ID], [UsuarioID], [Acronimo]) VALUES (5, 2, N'B-002')
INSERT [dbo].[Puesto] ([ID], [UsuarioID], [Acronimo]) VALUES (6, 2, N'B-007')
INSERT [dbo].[Puesto] ([ID], [UsuarioID], [Acronimo]) VALUES (7, 2, N'C-006')
INSERT [dbo].[Puesto] ([ID], [UsuarioID], [Acronimo]) VALUES (8, 2, N'D-004')
INSERT [dbo].[Puesto] ([ID], [UsuarioID], [Acronimo]) VALUES (9, 2, N'D-005')
INSERT [dbo].[Puesto] ([ID], [UsuarioID], [Acronimo]) VALUES (10, 2, N'D-005')
INSERT [dbo].[Puesto] ([ID], [UsuarioID], [Acronimo]) VALUES (11, 3, N'A-004')
INSERT [dbo].[Puesto] ([ID], [UsuarioID], [Acronimo]) VALUES (12, 3, N'A-005')
INSERT [dbo].[Puesto] ([ID], [UsuarioID], [Acronimo]) VALUES (13, 3, N'A-006')
INSERT [dbo].[Puesto] ([ID], [UsuarioID], [Acronimo]) VALUES (14, 3, N'Z-001')
INSERT [dbo].[Puesto] ([ID], [UsuarioID], [Acronimo]) VALUES (15, 3, N'B-009')
INSERT [dbo].[Puesto] ([ID], [UsuarioID], [Acronimo]) VALUES (16, 2, N'D-007')
INSERT [dbo].[Puesto] ([ID], [UsuarioID], [Acronimo]) VALUES (17, 2, N'D-008')
INSERT [dbo].[Puesto] ([ID], [UsuarioID], [Acronimo]) VALUES (18, 3, N'Z-002')
SET IDENTITY_INSERT [dbo].[Puesto] OFF
SET IDENTITY_INSERT [dbo].[Reserva] ON 

INSERT [dbo].[Reserva] ([ID], [UsuarioID], [Producto_x_PuestoID], [Cantidad], [Fecha], [Estado]) VALUES (1, 1, 1, CAST(10.00 AS Decimal(5, 2)), CAST(N'2018-10-11' AS Date), 1)
INSERT [dbo].[Reserva] ([ID], [UsuarioID], [Producto_x_PuestoID], [Cantidad], [Fecha], [Estado]) VALUES (2, 1, 2, CAST(20.00 AS Decimal(5, 2)), CAST(N'2018-10-11' AS Date), 1)
INSERT [dbo].[Reserva] ([ID], [UsuarioID], [Producto_x_PuestoID], [Cantidad], [Fecha], [Estado]) VALUES (3, 2, 4, CAST(10.00 AS Decimal(5, 2)), CAST(N'2018-10-11' AS Date), 1)
INSERT [dbo].[Reserva] ([ID], [UsuarioID], [Producto_x_PuestoID], [Cantidad], [Fecha], [Estado]) VALUES (4, 1, 10, CAST(5.00 AS Decimal(5, 2)), CAST(N'2018-10-11' AS Date), 1)
SET IDENTITY_INSERT [dbo].[Reserva] OFF
SET IDENTITY_INSERT [dbo].[Tipo_Producto] ON 

INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (1, N'Papa', 1, N'https://lampadia.com/assets/uploads_images/images/Papa-001.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (2, N'Yuca', 1, N'http://peru.info/DesktopModules/BB_LA_Superfood_Home/Images/Yuca/yuca-detalle.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (4, N'Olluco', 1, N'https://www.dimebeneficios.com/wp-content/uploads/2017/09/beneficios-del-olluco.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (5, N'Oca', 1, N'https://www.tuberculos.org/wp-content/uploads/2018/05/oca.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (6, N'Camote', 1, N'http://eatfresh.org/sites/default/files/styles/645x417large/public/food/media/sweet-potatoes_0.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (7, N'Zanahoria', 1, N'https://www.conasi.eu/blog/wp-content/uploads/2018/07/zanahorias-y-tomar-el-sol.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (8, N'Cebolla', 4, N'https://cdn2.cocinadelirante.com/sites/default/files/styles/gallerie/public/images/2017/10/cebollas.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (9, N'Ajo', 4, N'https://hdstatic.net/gridfs/holadoctor/625x470_52263cdcb93795c501414701_0_33-1378238766062.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (10, N'Nabo', 4, N'https://www.queroviverbem.com.br/wp-content/uploads/2018/03/beneficios-do-nabo.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (11, N'Durazno', 2, N'http://cdnprod.foodnetworklatam.com/wp-content/uploads/2015/09/1442886366-shutterstock_190839806-1.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (12, N'Fresa', 2, N'https://www.natursan.net/wp-content/fresas-propiedades.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (13, N'Platano', 2, N'https://assets.metrolatam.com/ec/2017/02/03/beneficios-platano-1200x600.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (14, N'Uva', 2, N'http://www.adnradio.cl/images_remote/370/3708883_n_vir3.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (15, N'Mango', 2, N'https://www.muyinteresante.com.mx/wp-content/uploads/2018/07/Mango-770x513.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (16, N'Manzana', 2, N'http://www.redagricola.com/cl/assets/uploads/2017/10/xdiapo8-792x591-c-default.jpg.pagespeed.ic.qnxIxJil_z.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (17, N'Maracuya', 2, N'http://cde.peru.com/ima/0/1/4/2/1/1421029/611x458/frutas.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (18, N'Tuna', 2, N'https://http2.mlstatic.com/nopal-de-tuna-blanca-en-penca-D_NQ_NP_451801-MLM20413137759_092015-F.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (19, N'Naranja', 2, N'https://multimedia.larepublica.pe/720x405/larepublica/imagen/2017/08/11/noticia-la-naranja-en-tus-comidas.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (20, N'Piña', 2, N'https://www.lechepuleva.es/documents/13930/203222/pi%C3%B1a_g.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (21, N'Palta', 2, N'https://cde.publimetro.e3.pe/ima/0/0/1/4/7/147111.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (22, N'Papaya', 2, N'http://as01.epimg.net/buenavida/imagenes/2017/04/16/portada/1492362108_801597_1492362279_noticia_normal.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (23, N'Mandarina', 2, N'https://cdn2.salud180.com/sites/default/files/styles/medium/public/field/image/2017/11/mandraina.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (24, N'Haba', 3, N'https://sc01.alicdn.com/kf/HTB1eZsvXnZRMeJjSspkq6xGpXXaX/DPL-300-Desgranadora-de-haba-sorgo-soya.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (25, N'Acelga', 3, N'https://biotrendies.com/wp-content/uploads/2015/06/Acelga.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (26, N'Albahaca', 3, N'https://static.tuasaude.com/media/article/pf/2v/manjericao_3994_l.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (27, N'Alcachofa', 3, N'https://mejorconsalud.com/wp-content/uploads/2014/01/Alcachofa.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (28, N'Apio', 3, N'http://benicio.cl/1444-large_default/apio.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (29, N'Arveja', 3, N'http://www.supermercadoslatorre.com/wp-content/uploads/2015/09/Arveja-498x261.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (30, N'Coliflor', 3, N'https://biotrendies.com/wp-content/uploads/2015/07/colifor.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (31, N'Espinaca', 3, N'https://comefruta.es/wp-content/uploads/espinacas.jpg')
INSERT [dbo].[Tipo_Producto] ([ID], [Nombre], [Grupo_AlimenticioID], [ImagePath]) VALUES (32, N'Lechuga', 3, N'https://static1.squarespace.com/static/552cc4f2e4b08f5bedcf026a/t/5562e245e4b022cec2207406/1432543818672/lechuga+batvia.jpg')
SET IDENTITY_INSERT [dbo].[Tipo_Producto] OFF
SET IDENTITY_INSERT [dbo].[Usuario] ON 

INSERT [dbo].[Usuario] ([ID], [Nombre], [Apellidos], [Username], [Password], [Correo], [Fecha_registro]) VALUES (1, N'Martha', N'Mendoza Quispe', N'martha', N'123', N'martha@gmail.com', CAST(N'2018-10-10' AS Date))
INSERT [dbo].[Usuario] ([ID], [Nombre], [Apellidos], [Username], [Password], [Correo], [Fecha_registro]) VALUES (2, N'Marco', N'Perez Sanchez', N'marco', N'123', N'marco@gmail.com', CAST(N'2018-10-10' AS Date))
INSERT [dbo].[Usuario] ([ID], [Nombre], [Apellidos], [Username], [Password], [Correo], [Fecha_registro]) VALUES (3, N'Maria', N'Martinez Lopez', N'maria', N'123', N'maria@gmail.com', CAST(N'2018-10-10' AS Date))
SET IDENTITY_INSERT [dbo].[Usuario] OFF
ALTER TABLE [dbo].[Cuenta]  WITH CHECK ADD  CONSTRAINT [FK_Cuenta_Usuario] FOREIGN KEY([UsuarioID])
REFERENCES [dbo].[Usuario] ([ID])
GO
ALTER TABLE [dbo].[Cuenta] CHECK CONSTRAINT [FK_Cuenta_Usuario]
GO
ALTER TABLE [dbo].[Presupuesto]  WITH CHECK ADD  CONSTRAINT [FK_Presupuesto_Usuario] FOREIGN KEY([UsuarioID])
REFERENCES [dbo].[Usuario] ([ID])
GO
ALTER TABLE [dbo].[Presupuesto] CHECK CONSTRAINT [FK_Presupuesto_Usuario]
GO
ALTER TABLE [dbo].[Presupuesto_x_Producto_x_Puesto]  WITH CHECK ADD  CONSTRAINT [FK_Presupuesto_x_Producto_x_Puesto_Presupuesto] FOREIGN KEY([PresupuestoID])
REFERENCES [dbo].[Presupuesto] ([ID])
GO
ALTER TABLE [dbo].[Presupuesto_x_Producto_x_Puesto] CHECK CONSTRAINT [FK_Presupuesto_x_Producto_x_Puesto_Presupuesto]
GO
ALTER TABLE [dbo].[Presupuesto_x_Producto_x_Puesto]  WITH CHECK ADD  CONSTRAINT [FK_Presupuesto_x_Producto_x_Puesto_Producto_x_Puesto] FOREIGN KEY([Producto_x_PuestoID])
REFERENCES [dbo].[Producto_x_Puesto] ([ID])
GO
ALTER TABLE [dbo].[Presupuesto_x_Producto_x_Puesto] CHECK CONSTRAINT [FK_Presupuesto_x_Producto_x_Puesto_Producto_x_Puesto]
GO
ALTER TABLE [dbo].[Producto]  WITH CHECK ADD  CONSTRAINT [FK_Variedad_Tipo_Producto] FOREIGN KEY([Tipo_ProductoID])
REFERENCES [dbo].[Tipo_Producto] ([ID])
GO
ALTER TABLE [dbo].[Producto] CHECK CONSTRAINT [FK_Variedad_Tipo_Producto]
GO
ALTER TABLE [dbo].[Producto_x_Puesto]  WITH CHECK ADD  CONSTRAINT [FK_Producto_x_Puesto_Producto] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Producto] ([ID])
GO
ALTER TABLE [dbo].[Producto_x_Puesto] CHECK CONSTRAINT [FK_Producto_x_Puesto_Producto]
GO
ALTER TABLE [dbo].[Producto_x_Puesto]  WITH CHECK ADD  CONSTRAINT [FK_Producto_x_Puesto_Puesto] FOREIGN KEY([PuestoID])
REFERENCES [dbo].[Puesto] ([ID])
GO
ALTER TABLE [dbo].[Producto_x_Puesto] CHECK CONSTRAINT [FK_Producto_x_Puesto_Puesto]
GO
ALTER TABLE [dbo].[Puesto]  WITH CHECK ADD  CONSTRAINT [FK_Puesto_Usuario] FOREIGN KEY([UsuarioID])
REFERENCES [dbo].[Usuario] ([ID])
GO
ALTER TABLE [dbo].[Puesto] CHECK CONSTRAINT [FK_Puesto_Usuario]
GO
ALTER TABLE [dbo].[Reserva]  WITH CHECK ADD  CONSTRAINT [FK_Reserva_Producto_x_Puesto] FOREIGN KEY([Producto_x_PuestoID])
REFERENCES [dbo].[Producto_x_Puesto] ([ID])
GO
ALTER TABLE [dbo].[Reserva] CHECK CONSTRAINT [FK_Reserva_Producto_x_Puesto]
GO
ALTER TABLE [dbo].[Reserva]  WITH CHECK ADD  CONSTRAINT [FK_Reserva_Usuario] FOREIGN KEY([UsuarioID])
REFERENCES [dbo].[Usuario] ([ID])
GO
ALTER TABLE [dbo].[Reserva] CHECK CONSTRAINT [FK_Reserva_Usuario]
GO
ALTER TABLE [dbo].[Tipo_Producto]  WITH CHECK ADD  CONSTRAINT [FK_Tipo_Producto_Grupo_Alimenticio] FOREIGN KEY([Grupo_AlimenticioID])
REFERENCES [dbo].[Grupo_Alimenticio] ([ID])
GO
ALTER TABLE [dbo].[Tipo_Producto] CHECK CONSTRAINT [FK_Tipo_Producto_Grupo_Alimenticio]
GO
USE [master]
GO
ALTER DATABASE [Mercatec] SET  READ_WRITE 
GO
